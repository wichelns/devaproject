create database if not exists `devaproject-dev` default charset = utf8mb4 default collate = utf8mb4_unicode_ci;

# 系统文档表
create table if not exists sys_doc
(
    doc_id       smallint unsigned primary key auto_increment comment '文档ID',
    type         smallint  not null comment '文档类型 10-用户协议 20-隐私协议 30-需求用户手册 40-开发者手册 50-业务配置 60-系统配置',
    content      text comment '内容',
    status       tinyint   not null default '0' comment '状态 1-激活 0-未激活',
    extra        varchar(2048) comment '扩展字段',
    deleted      tinyint            default '0' comment '删除位 0-未删除 null-已删除',
    version      smallint  not null default '0' comment '版本号',
    created_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间'
) engine innodb comment '系统文档';

insert into `sys_doc`
values (1, 60, '{"id-buckets":[{"b107":107},{"b109":109}],"login":{"jwtSecret":"hnust@lzj2025","tokenTimeout":604800}}',
        1,
        '{"invalidIdBuckets":[]}', 0, 1, '2024-02-15 10:23:33', '2024-02-16 08:09:16');

# 需求用户表
create table if not exists requirement_user
(
    req_user_id     bigint unsigned primary key comment '需求用户ID',
    username        varchar(32)  not null comment '用户名',
    avatar          varchar(255) not null comment '头像',
    phone           char(11)     not null comment '电话',
    password        char(32)     not null comment '账号密码',
    status          smallint     not null comment '1-正常 99-冻结',
    contact         varchar(32)  not null comment '微信/QQ号',
    profile         varchar(128) comment '个性签名',
    last_login_time datetime comment '上次登录时间',
    last_login_ip   varchar(64) comment '上次登录IP',
    deleted         tinyint               default '0' comment '删除位 0-未删除 null-已删除',
    version         smallint     not null default '0' comment '版本号',
    created_time    timestamp    not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time    timestamp    not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    unique key phone_idx (phone, deleted)
) engine innodb comment '需求用户';


# 开发者用户
create table if not exists developer_user
(
    dev_user_id           bigint unsigned primary key comment '开发者用户ID',
    username              varchar(32)  not null comment '用户名',
    avatar                varchar(255) not null comment '头像',
    phone                 char(11)     not null comment '电话',
    password              char(32)     not null comment '账号密码',
    status                smallint     not null comment '1-正常 99-冻结',
    contact               varchar(32)  not null comment '微信/QQ号',
    profile               varchar(128) comment '个性签名',
    recv_project_count    smallint     not null default '0' comment '接取项目数',
    deliver_project_count smallint     not null default '0' comment '交付项目数',
    stars                 smallint     not null default '0' comment '获取星星数',
    last_login_time       datetime comment '上次登录时间',
    last_login_ip         varchar(64) comment '上次登录IP',
    deleted               tinyint               default '0' comment '删除位 0-未删除 null-已删除',
    version               smallint     not null default '0' comment '版本号',
    created_time          timestamp    not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time          timestamp    not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    unique key phone_idx (phone, deleted)
) engine innodb comment '开发者用户';

insert into `developer_user`
values (3590324254, 'n70umt_106', '', '18476596106', 'e10adc3949ba59abbe56e057f20f883e', 1, 'QQ:1871245748', '', 0, 0,
        0, '2024-02-16 19:34:25', '127.0.0.1', 0, 0, '2024-02-16 09:15:33', '2024-02-16 11:34:25');

# 管理员
create table if not exists sys_admin
(
    `admin_id`        int unsigned primary key comment '管理员ID',
    `phone`           char(11)    not null comment '电话',
    `password`        char(32)    not null comment '密码',
    `name`            varchar(32) not null default '' comment '昵称',
    `last_login_time` datetime comment '最后登陆时间',
    `status`          smallint    not null default '99' comment '状态 1-激活 99-冻结',
    `deleted`         tinyint              default '0' comment '删除位 0-未删除 null-已删除',
    `version`         smallint    not null default '0' comment '版本号',
    `created_time`    timestamp   not null default CURRENT_TIMESTAMP comment '创建时间',
    `updated_time`    timestamp   null     default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    unique key phone_idx (phone, deleted)
) engine innodb comment '系统管理员';


create table if not exists project_requirement
(
    project_id   int unsigned primary key auto_increment comment '项目ID 从10w开始',
    title        varchar(32)     not null comment '标题',
    description  varchar(128)  comment '简介',
    content      varchar(2048)   not null comment '内容',
    req_user_id  bigint unsigned not null comment '需求用户ID',
    reward       double unsigned not null default '0' comment '报酬',
    view_count   int unsigned    not null default '0' comment '浏览量',
    tag_list     varchar(128)    not null comment '标签列表',
    status       smallint        not null default '10' comment '状态',
    deliver_time datetime comment '交付时间',
    deleted      tinyint                  default '0' comment '删除位 0-未删除 null-已删除',
    version      smallint        not null default '0' comment '版本号',
    created_time timestamp       not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time timestamp       null     default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    key (req_user_id)
) engine innodb comment '项目需求';

alter table project_requirement
    auto_increment = 100000;

create table if not exists personal_project
(
    personal_proj_id bigint unsigned primary key comment '个人项目ID',
    project_id       int unsigned    not null comment '项目ID',
    dev_user_id      bigint unsigned not null comment '开发者ID',
    team_proj_id     bigint unsigned comment '团队项目ID',
    join_intro       varchar(128) comment '参选介绍',
    status           smallint        not null default '10' comment '状态',
    deliver_time     datetime comment '交付时间',
    comment          varchar(128) comment '评语',
    stars            tinyint         not null default '0' comment '星星数',
    deleted          tinyint                  default '0' comment '删除位 0-未删除 null-已删除',
    version          smallint        not null default '0' comment '版本号',
    created_time     timestamp       not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time     timestamp       null     default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    key user_proj_idx (dev_user_id, project_id),
    key proj_idx (project_id)
) engine innodb comment '个人项目';


create table if not exists team_project
(
    team_proj_id bigint unsigned primary key comment '团队项目ID',
    project_id   int unsigned    not null comment '项目ID',
    team_id      int unsigned    not null comment '团队ID',
    leader_id    bigint unsigned not null comment '队长ID',
    extra        varchar(1024)            default '{}' comment '额外信息，Json格式',
    join_intro   varchar(128) comment '参选介绍',
    status       smallint        not null default '10' comment '状态',
    deliver_time datetime comment '交付时间',
    comment      varchar(128) comment '评语',
    stars        tinyint         not null default '0' comment '星星数',
    deleted      tinyint                  default '0' comment '删除位 0-未删除 null-已删除',
    version      smallint        not null default '0' comment '版本号',
    created_time timestamp       not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time timestamp       null     default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    key team_proj_idx (team_id, project_id),
    key proj_idx (project_id)
) engine innodb comment '团队项目';


create table if not exists credit_record
(
    record_id    bigint unsigned primary key comment '记录ID',
    credit_item  tinyint         not null comment '信誉项 1-接取 5-交付 10-星星数',
    delta        tinyint         not null comment '增量',
    project_id   int unsigned    not null comment '项目ID',
    source_id    bigint unsigned not null comment '来源ID',
    source_type  tinyint         not null comment '来源类型 10-个人项目 20-团队项目',
    target_id    bigint unsigned not null comment '目标ID',
    target_type  tinyint         not null comment '目标类型 10-个人 20-团队',
    deleted      tinyint                  default '0' comment '删除位 0-未删除 null-已删除',
    version      smallint        not null default '0' comment '版本号',
    created_time timestamp       not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time timestamp       null     default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    unique key proj_tg_credit_idx (project_id, target_id, target_type, credit_item, deleted)
) engine innodb comment '信誉记录';


create table if not exists team
(
    team_id               int unsigned primary key auto_increment comment '团队ID',
    leader_id             bigint unsigned  not null comment '队长ID',
    leaderName            varchar(32) comment '队长名称',
    avatar                varchar(255)              default '' comment '头像',
    team_name             varchar(32)      not null comment '团队名称',
    profile               varchar(128) comment '简介',
    cur_member_count      tinyint unsigned not null default '1' comment '当前人数',
    max_member_count      tinyint unsigned not null default '20' comment '最大人数',
    recv_project_count    smallint         not null default '0' comment '接取项目数',
    deliver_project_count smallint         not null default '0' comment '交付项目数',
    stars                 smallint         not null default '0' comment '获取星星数',
    deleted               tinyint                   default '0' comment '删除位 0-未删除 null-已删除',
    version               smallint         not null default '0' comment '版本号',
    created_time          timestamp        not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time          timestamp        null     default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    key leader_idx (leader_id)
) engine innodb comment '团队';

alter table team
    change leaderName leader_name varchar(32) comment '队长名称';

alter table team
    auto_increment = 100000;

create table if not exists team_member
(
    member_id    bigint unsigned primary key comment '队员ID',
    dev_user_id  bigint unsigned not null comment '开发人员ID',
    team_id      int unsigned    not null comment '团队ID',
    identity     tinyint         not null comment '成员身份 1-队长 5-普通成员',
    resp_tag_id  varchar(8) comment '职责标签ID groupId-tagId',
    remark       varchar(16) comment '成员备注',
    deleted      tinyint                  default '0' comment '删除位 0-未删除 null-已删除',
    version      smallint        not null default '0' comment '版本号',
    created_time timestamp       not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time timestamp       null     default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    unique key team_user_idx (team_id, dev_user_id, deleted),
    key user_idx (dev_user_id)
) engine innodb comment '团队成员';


create table if not exists request
(
    req_id       bigint unsigned primary key comment '请求ID',
    source_id    bigint unsigned not null comment '来源ID',
    source_type  tinyint         not null comment '来源类型 10-个人 20-团队',
    target_id    bigint unsigned not null comment '目标ID',
    target_type  tinyint         not null comment '目标类型 10-个人 20-团队',
    operation    tinyint         not null comment '操作 1-入队邀请 5-入队申请 10-好友申请',
    extra        varchar(1024) comment '额外信息',
    status       tinyint         not null default '10' comment '状态 10-未读 20-已读未操作 30-已确认 40-已拒绝',
    deleted      tinyint                  default '0' comment '删除位 0-未删除 null-已删除',
    version      smallint        not null default '0' comment '版本号',
    created_time timestamp       not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time timestamp       null     default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    key source_idx (source_id),
    key target_idx (target_id)
) engine innodb comment '请求';


create table if not exists resp_tag_lib
(
    id            int unsigned primary key auto_increment,
    dev_user_id   bigint unsigned  not null comment '开发者用户ID',
    group_id      tinyint unsigned not null comment '组ID',
    group_name    varchar(16)               default 'g',
    resp_tag_list varchar(512) comment '标签列表JSON数组格式：[{"tagId":1,"tagName":"xxx"},...]',
    deleted       tinyint                   default '0' comment '删除位 0-未删除 null-已删除',
    version       smallint         not null default '0' comment '版本号',
    created_time  timestamp        not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time  timestamp        null     default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    unique key user_group_idx (dev_user_id, group_id, deleted)
) engine innodb comment '职责标签库';


create table if not exists announce
(
    announce_id   int unsigned primary key auto_increment comment '通知ID',
    title         varchar(32)      not null comment '标题',
    content       varchar(1024)    not null comment '内容',
    ref_link      varchar(512) comment '跳转链接',
    announce_type tinyint unsigned not null comment '通知类型 1-运营 5-系统 10-公告板',
    receiver_id   bigint unsigned comment '接收者ID',
    receiver_type tinyint unsigned comment '接收者类型',
    status        tinyint          not null default '0' comment '状态 1-已读 0-未读',
    deleted       tinyint                   default '0' comment '删除位 0-未删除 null-已删除',
    version       smallint         not null default '0' comment '版本号',
    created_time  timestamp        not null default CURRENT_TIMESTAMP comment '创建时间',
    updated_time  timestamp        null     default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '更新时间',
    key user_idx (receiver_id)
) engine innodb comment '通知';