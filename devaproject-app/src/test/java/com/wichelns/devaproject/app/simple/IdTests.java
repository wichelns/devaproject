package com.wichelns.devaproject.app.simple;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import org.junit.jupiter.api.Test;

/**
 * @author zhijia
 * @date 2024/2/12 11:52
 */
public class IdTests {

	@Test
	void id0() {
		System.out.println("fastUUID:" + IdUtil.fastUUID());// 6f95e1df-9895-4925-80f4-bd12b3a3decb
		System.out.println("fastSimpleUUID:" + IdUtil.fastSimpleUUID());// 57baecd2dfb5487394cbdb9bf7efd57c
		System.out.println("snow-0-0:" + IdUtil.getSnowflake(0, 0).nextId());// 1756889952311836672
		System.out.println("snow-0-0:" + IdUtil.getSnowflake(0, 1).nextId());//
	}

	@Test
	void snow0() {
		DateTime dateTime = DateTime.of(1288834974657L);
		System.out.println(DateUtil.formatDateTime(dateTime));
		// IdUtil.getSnowflake().getDataCenterId()
	}

	@Test
	void t1() {
		long leading = 107;
		long begin = leading << 10;
		System.out.println("begin=" + begin + ", nextId=" + (begin + RandomUtil.randomInt(1, 10)));
	}

}
