package com.wichelns.devaproject.app.simple;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSONObject;
import com.wichelns.devaproject.app.module.project.convert.DeveloperProjConvert;
import com.wichelns.devaproject.app.module.project.entity.TeamProject;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;

/**
 * @author zhijia
 * @date 2024/2/14 12:20
 */
public class JsonTests {

	String json = "{\"k3\":{\"k4\":6,\"k1\":[{\"b1\":127},{\"b2\":166}]},\"k2\":10000}";

	@Test
	void t1() {
		System.out.println(json);

		JSONObject jsonObject = JSONObject.parseObject(json);
		System.out.println(jsonObject.getByPath("k3.k1"));

		// 替换
		// jsonObject.

		System.out.println(jsonObject.toJSONString());
		System.out.println(jsonObject.get("$.k3.k1"));
	}

	@Test
	void t2() {
		// hutool
		JSON jsonObject = JSONUtil.parse(json);
		System.out.println(jsonObject.toJSONString(0));
		System.out.println(JSONUtil.getByPath(jsonObject, "k3.k1"));
		// 替换
		jsonObject.putByPath("k3.k1", Collections.emptyList());
		System.out.println(jsonObject.toJSONString(0));
		System.out.println(json);
	}

	@Test
	void t3() {
		String str = "{\"k1\":[{\"b1\":127},{\"b2\":166}]}";
		JSON jsonObj = JSONUtil.parse(str);
		Object t = jsonObj.getByPath("k1");
		System.out.println(t.getClass());//JSONArray

		JSONArray jsonArray = (JSONArray) t;
		HashMap<Object, Object> map = new HashMap<>(jsonArray.size());
		jsonArray.forEach(e -> map.putAll((cn.hutool.json.JSONObject) e));
		System.out.println(map);
	}

	@Test
	void t4() {// ok
		String s = "{\"k2\":[{\"b1\":101},{\"b2\":103}]}";
		JSON parse = JSONUtil.parse(s);
		JSONArray jsonArray = (JSONArray) parse.getByPath("k2");
		jsonArray.add(Collections.singletonMap("b3", 106));

		parse.putByPath("k2", jsonArray);
		System.out.println(parse.toJSONString(0));
	}

	@Test
	void t5() {
		String s = "{\"k1\":[]}";
		JSON parse = JSONUtil.parse(s);
		JSONArray k1 = (JSONArray) parse.getByPath("k1");
		System.out.println(k1.isEmpty());
	}

	@Test
	void t6() {
		String jsonStr = JSONUtil.toJsonStr(DeveloperProjConvert.INSTANCE.domain2DTO(new TeamProject()));
		System.out.println(jsonStr);
	}

}
