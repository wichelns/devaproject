package com.wichelns.devaproject.app.spring;

import com.wichelns.devaproject.app.DevaprojectApp;
import com.wichelns.devaproject.app.dao.user.mapper.DeveloperUserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Collections;

/**
 * @author zhijia
 * @date 2024/2/27 0:43
 */
@SpringBootTest(classes = DevaprojectApp.class)
public class MPTests {
	@Resource
	DeveloperUserMapper developerUserMapper;

	@Test
	void testColl() {
		System.out.println(developerUserMapper.incDeliverCountById(Collections.singletonList(1L)));
	}

}
