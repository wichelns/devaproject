package com.wichelns.devaproject.app.spring;

import com.wichelns.devaproject.app.DevaprojectApp;
import com.wichelns.devaproject.app.core.context.SysUserContext;
import com.wichelns.devaproject.app.dao.user.repo.DeveloperUserRepo;
import com.wichelns.devaproject.app.module.user.entity.DeveloperUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

/**
 * @author zhijia
 * @date 2024/2/15 18:28
 */
@SpringBootTest(classes = DevaprojectApp.class)
public class ContextTests {

	@Test
	void testGenId() {// ok
		Long nextUserId = SysUserContext.holder().nextUserId();
		System.out.println(nextUserId);
	}

	@Test
	void testDao(@Autowired DeveloperUserRepo developerUserRepo) {
		boolean update = developerUserRepo.lambdaUpdate().set(DeveloperUser::getDeliverProjectCount, 1)
				.eq(DeveloperUser::getDevUserId, 10700000008L).update();
		System.out.println(update);
		developerUserRepo.list().forEach(System.out::println);
	}

	@Test
	void checkMsgConverter(@Autowired ApplicationContext context) {
		HttpMessageConverters converters = context.getBean(HttpMessageConverters.class);
		converters.getConverters().forEach(System.out::println);
	}

}
