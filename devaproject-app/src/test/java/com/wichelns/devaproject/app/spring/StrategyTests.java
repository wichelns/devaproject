package com.wichelns.devaproject.app.spring;

import com.wichelns.devaproject.app.DevaprojectApp;
import com.wichelns.devaproject.app.module.message.request.enums.OperationEnum;
import com.wichelns.devaproject.app.module.message.request.strategy.ReqQueryStrategyFactory;
import com.wichelns.devaproject.app.module.message.request.strategy.ReqQueryStrategy;
import com.wichelns.devaproject.common.enums.BaseStatus;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/28 23:54
 */
@SpringBootTest(classes = DevaprojectApp.class)
public class StrategyTests {
	@Resource
	ReqQueryStrategyFactory reqQueryStrategyFactory;

	@Test
	void testDP() {
		ReqQueryStrategy reqQueryStrategy = reqQueryStrategyFactory.getStrategy(OperationEnum.INVITATION_JOIN_TEAM.getCode(), BaseStatus.Y.getCode());
		System.out.println(reqQueryStrategy);
	}

}
