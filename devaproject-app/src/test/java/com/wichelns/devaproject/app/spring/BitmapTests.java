package com.wichelns.devaproject.app.spring;

import com.wichelns.devaproject.app.DevaprojectApp;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/3/4 12:39
 */
@SpringBootTest(classes = DevaprojectApp.class)
public class BitmapTests {
	@Resource
	RedisTemplate<String, String> redisTemplate;

	@Test
	void testAll() {
		String key = "bitmap";
		redisTemplate.opsForValue().setBit(key, 2, true);
		redisTemplate.opsForValue().setBit(key, 4, true);
		redisTemplate.opsForValue().setBit(key, 5, true);
		System.out.println(redisTemplate.opsForValue().getBit(key, 2));
		System.out.println(redisTemplate.opsForValue().getBit(key, 5));
		System.out.println(redisTemplate.opsForValue().getBit(key, 1));
		Long count = redisTemplate.execute((RedisCallback<Long>) connection -> connection.bitCount(key.getBytes()));
		System.out.println(count);
	}

	@Test
	void testSingle() {
		String bit109 = "bit109";
		// redisTemplate.opsForValue().setBit(bit109, 1, true);
		System.out.println(redisTemplate.opsForValue().getBit(bit109, 10));// 只有set了，才会扩展位
	}

}
