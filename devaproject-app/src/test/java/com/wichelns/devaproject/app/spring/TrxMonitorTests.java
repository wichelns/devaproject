package com.wichelns.devaproject.app.spring;

import com.wichelns.devaproject.app.DevaprojectApp;
import com.wichelns.devaproject.app.dao.user.repo.CreditRecordRepo;
import com.wichelns.devaproject.app.module.user.entity.CreditRecord;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/3/5 16:04
 */
@SpringBootTest(classes = DevaprojectApp.class)
public class TrxMonitorTests {
	@Resource
	private CreditRecordRepo creditRecordRepo;

	@Test
	@Transactional
	public void test() {
		creditRecordRepo.lambdaUpdate().set(CreditRecord::getVersion, 1)
				.eq(CreditRecord::getRecordId, 1).update();
	}

}
