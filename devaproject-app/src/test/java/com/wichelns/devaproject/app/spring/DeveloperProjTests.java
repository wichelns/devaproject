package com.wichelns.devaproject.app.spring;

import com.wichelns.devaproject.app.DevaprojectApp;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 开发者项目管理测试
 *
 * @author zhijia
 * @date 2024/2/23 13:42
 */
@SpringBootTest(classes = DevaprojectApp.class)
public class DeveloperProjTests {

	@Test
	void personJoin() {

	}

	@Test
	void teamJoin() {

	}

	@Test
	void personQuit() {

	}

	@Test
	void teamQuit() {

	}

}
