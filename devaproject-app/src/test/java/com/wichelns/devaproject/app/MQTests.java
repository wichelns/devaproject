package com.wichelns.devaproject.app;

import cn.hutool.json.JSONUtil;
import com.wichelns.devaproject.app.module.project.convert.DeveloperProjConvert;
import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.module.project.entity.TeamProject;
import com.wichelns.devaproject.app.mq.dto.MessageDTO;
import com.wichelns.devaproject.app.mq.producer.Producer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static com.wichelns.devaproject.app.constant.MQConstant.KEY_PROJECT_TEAM_JOIN;

/**
 * @author zhijia
 * @date 2024/2/23 16:39
 */
@SpringBootTest(classes = DevaprojectApp.class)
public class MQTests {
	@Resource
	private Producer producer;

	@Test
	void testPubsub() {
		// 注意：本MQ单元测试会有问题，启动整个应用就没问题
		MessageDTO messageDto = new MessageDTO(KEY_PROJECT_TEAM_JOIN, DeveloperProjConvert.INSTANCE.domain2DTO(new TeamProject()));
		producer.produce(messageDto);
	}


	@Test
	void testSerialize() {
		// redisTemplate.opsForValue().set("k12", JSONUtil.toJsonStr(DeveloperProjConvert.INSTANCE.domain2DTO(new TeamProject())));
		// System.out.println(redisTemplate.opsForValue().get("k12"));
		String jsonStr = JSONUtil.toJsonStr(DeveloperProjConvert.INSTANCE.domain2DTO(new TeamProject()));
		DeveloperProjDTO developerProjDTO = JSONUtil.toBean(jsonStr, DeveloperProjDTO.class);

		System.out.println(developerProjDTO);
	}

}
