package com.wichelns.devaproject.app.simple;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Date;

/**
 * @author zhijia
 * @date 2024/2/14 1:17
 */
@Slf4j
public class SimpleTests {

	@Test
	void bit() {
		System.out.println(~(5));
		System.out.println(~(-1));
		System.out.println(~(-1<<3));
	}

	@Test
	void t1() {
		Integer n = 126;
		long l = (long) n;
		System.out.println(l);
	}

	@Test
	void t2() {
		System.out.println(Collections.singletonMap("hu", 45));
	}

	@Test
	void testDate() {
		System.out.println(DateUtil.offsetDay(new Date(), 7));
	}

	@Test
	void testStr() {
		System.out.println(StrUtil.format("{}-{}", 111, "jkl"));
	}

	@Test
	void t3() {
		log.error("t{}-p{}:", 1, 2, new BusinessException("err"));
	}

}
