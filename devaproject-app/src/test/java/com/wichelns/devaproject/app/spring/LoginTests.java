package com.wichelns.devaproject.app.spring;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import com.wichelns.devaproject.app.DevaprojectApp;
import com.wichelns.devaproject.app.module.login.param.LoginParam;
import com.wichelns.devaproject.app.module.login.service.LoginService;
import com.wichelns.devaproject.app.module.user.enums.UserTypeEnum;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhijia
 * @date 2024/2/23 15:50
 */
@SpringBootTest(classes = DevaprojectApp.class)
public class LoginTests {
	@Resource
	private LoginService loginService;

	@Test
	void loginOrSignUpMulti() {
		List<String> phoneList = loadPhoneList();
		int start = phoneList.size() / 2;
		List<String> sub = CollUtil.sub(phoneList, start, phoneList.size());

		LoginParam param = new LoginParam();
		param.setUserType(UserTypeEnum.DEVELOPER_USER.getCode());
		param.setPassword("e10adc3949ba59abbe56e057f20f883e");// 123456
		param.setContact("dev>>>");
		// dev
		for (int i = 0; i < start; i++) {
			param.setPhone(phoneList.get(i));
			loginService.loginOrSignUp(param);
		}

		param.setUserType(UserTypeEnum.REQUIREMENT_USER.getCode());
		param.setContact("req>>>");
		// req
		for (String phone : sub) {
			param.setPhone(phone);
			loginService.loginOrSignUp(param);
		}
	}

	List<String> loadPhoneList() {
		InputStream inputStream;
		try {
			// Linux环境下获取jar包classpath中的文件
			ClassLoader cl = this.getClass().getClassLoader();
			inputStream = cl.getResourceAsStream("classpath:virtual-phone.csv");
			// 如果是Linux环境  这个inputStram就不会为空
			if (null == inputStream) {
				// 走这里是因为是开发环境
				File targetFile = ResourceUtils.getFile("classpath:virtual-phone.csv");
				inputStream = Files.newInputStream(targetFile.toPath());
			}
			List<CsvRow> rows = CsvUtil.getReader()
					.read(new InputStreamReader(inputStream))
					.getRows();
			return rows.stream().map(row -> row.get(0)).collect(Collectors.toList());
		} catch (Exception e) {
			throw new Error("加载失败");
		}

	}

}
