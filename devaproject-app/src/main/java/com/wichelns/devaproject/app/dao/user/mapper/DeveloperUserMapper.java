package com.wichelns.devaproject.app.dao.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wichelns.devaproject.app.module.user.entity.DeveloperUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhijia
 * @date 2024/2/14 11:02
 */
@Mapper
public interface DeveloperUserMapper extends BaseMapper<DeveloperUser> {
	int incRecvCountById(@Param("devUserIdList") List<Long> devUserIdList);
	int incDeliverCountById(@Param("devUserIdList")List<Long> devUserIdList);
	int incStarsById(@Param("devUserIdList") List<Long> devUserIdList, @Param("increment") int increment);

}
