package com.wichelns.devaproject.app.module.message.request.strategy.query;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.dao.message.repo.RequestRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamRepo;
import com.wichelns.devaproject.app.dao.user.repo.DeveloperUserRepo;
import com.wichelns.devaproject.app.module.message.request.convert.RequestConvert;
import com.wichelns.devaproject.app.module.message.request.entity.Request;
import com.wichelns.devaproject.app.module.message.request.enums.OperationEnum;
import com.wichelns.devaproject.app.module.message.request.param.ReqParam;
import com.wichelns.devaproject.app.module.message.request.strategy.ReqQueryStrategy;
import com.wichelns.devaproject.app.module.message.request.vo.AbstractReqVO;
import com.wichelns.devaproject.app.module.project.enums.DeveloperTypeEnum;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.app.module.user.entity.DeveloperUser;
import com.wichelns.devaproject.common.enums.BaseStatus;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 查询【发出】的【入队邀请】Request
 *
 * @author zhijia
 * @date 2024/2/28 23:41
 */
@Component
public class SInviteJoinTeamStrategy implements ReqQueryStrategy {
	private static final OperationEnum OPERATION = OperationEnum.INVITATION_JOIN_TEAM;
	private static final int IS_SENDER = BaseStatus.Y.getCode();
	@Resource
	private TeamRepo teamRepo;
	@Resource
	private RequestRepo requestRepo;
	@Resource
	private DeveloperUserRepo developerUserRepo;

	@Override
	public boolean support(OperationEnum operationEnum, int isSender) {
		return OPERATION.equals(operationEnum) && IS_SENDER == isSender;
	}


	@Override
	public IPage<AbstractReqVO> pageRequest(ReqParam param, Integer pageNo, Integer pageSize) {
		Long devUserId = Jwt.getLoginUserId();
		// 1. 查询用户是leader的团队
		Map<Integer, Team> teamMap = teamRepo.lambdaQuery().eq(Team::getLeaderId, devUserId).select(Team::getTeamId, Team::getTeamName).list()
				.stream().collect(Collectors.toMap(Team::getTeamId, t -> t));
		if (teamMap.isEmpty()) {
			return new Page<>();
		}
		// 2. 根据团队ID source去查找Request
		LambdaQueryWrapper<Request> qwForReq = Wrappers.<Request>lambdaQuery().in(Request::getSourceId, teamMap.keySet())
				.eq(Request::getSourceType, DeveloperTypeEnum.DEVELOPER_TEAM.getCode()).orderByDesc(Request::getCreatedTime);
		Page<Request> requestPage = requestRepo.page(new Page<>(pageNo, pageSize), qwForReq);
		if (CollUtil.isEmpty(requestPage.getRecords())) {
			return new Page<>();
		}
		// 3. 根据Request的targetId查询用户信息
		List<Long> devUserIdList = requestPage.getRecords().stream().map(Request::getTargetId).collect(Collectors.toList());
		Map<Long, DeveloperUser> userMap = developerUserRepo.listByIds(devUserIdList).stream().collect(Collectors.toMap(DeveloperUser::getDevUserId, u -> u));

		return requestPage.convert(request -> RequestConvert.INSTANCE.domain2VO(request, teamMap.get(request.getSourceId().intValue()), userMap.get(request.getTargetId())));
	}

}
