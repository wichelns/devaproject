package com.wichelns.devaproject.app.module.team.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson2.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.dao.team.repo.RespTagLibRepo;
import com.wichelns.devaproject.app.module.team.dto.RespTagDTO;
import com.wichelns.devaproject.app.module.team.entity.RespTagLib;
import com.wichelns.devaproject.app.module.team.service.RespTagLibService;
import com.wichelns.devaproject.app.module.team.vo.RespTagLibVO;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import com.yoo.stark.toolkit.name.NameUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;

/**
 * @author zhijia
 * @date 2024/3/3 15:24
 */
@Service
public class RespTagLibServiceImpl implements RespTagLibService {
	@Resource
	private RespTagLibRepo tagLibRepo;

	@Override
	public void addGroup(String groupName) {
		Long loginUserId = Jwt.getLoginUserId();
		RespTagLib tagLib = new RespTagLib();
		tagLib.setDevUserId(loginUserId);

		LambdaQueryWrapper<RespTagLib> qwForTag = Wrappers.<RespTagLib>lambdaQuery()
				.eq(RespTagLib::getDevUserId, loginUserId).orderByDesc(RespTagLib::getGroupId);
		List<RespTagLib> tagLibList = tagLibRepo.page(new Page<>(1, 1), qwForTag).getRecords();

		tagLib.setGroupId(tagLibList.isEmpty() ? 1 : tagLibList.get(0).getGroupId() + 1);
		tagLib.setGroupName(groupName);
		tagLib.setRespTagList("[]");

		tagLibRepo.save(tagLib);
	}

	@Override
	public void addTag(Integer groupId, String tagName) {
		Long loginUserId = Jwt.getLoginUserId();
		Optional<RespTagLib> tagOp = tagLibRepo.lambdaQuery().eq(RespTagLib::getDevUserId, loginUserId)
				.eq(RespTagLib::getGroupId, groupId).oneOpt();
		RespTagLib tagLib = tagOp.orElseThrow(() -> new BusinessException(ErrorCodeEnum.GL9999404, "group:" + groupId));
		List<RespTagDTO> tagDTOList = JSONArray.parseArray(tagLib.getRespTagList(), RespTagDTO.class);

		// 组内标签maxId
		OptionalInt maxTagIdOp = tagDTOList.stream().mapToInt(RespTagDTO::getTagId).max();
		RespTagDTO tagDTO = new RespTagDTO();
		tagDTO.setTagId(maxTagIdOp.isPresent() ? maxTagIdOp.getAsInt() + 1 : 1);
		tagDTO.setTagName(tagName);

		tagDTOList.add(tagDTO);
		tagLibRepo.lambdaUpdate().set(RespTagLib::getRespTagList, JSONArray.toJSONString(tagDTOList))
				.eq(RespTagLib::getId, tagLib.getId()).update();
	}

	@Override
	public List<RespTagLibVO> queryLib() {
		return tagLibRepo.lambdaQuery().eq(RespTagLib::getDevUserId, Jwt.getLoginUserId()).list()
				.stream().map(respTagLib -> {
					RespTagLibVO respTagLibVO = new RespTagLibVO();

					BeanUtil.copyProperties(respTagLib, respTagLibVO, NameUtil.string(RespTagLibVO::getRespTagList));

					List<RespTagLibVO.RespTagVO> respTagVOList = JSONArray.parseArray(respTagLib.getRespTagList(), RespTagLibVO.RespTagVO.class);
					respTagLibVO.setRespTagList(respTagVOList);

					return respTagLibVO;
				}).collect(Collectors.toList());
	}

}
