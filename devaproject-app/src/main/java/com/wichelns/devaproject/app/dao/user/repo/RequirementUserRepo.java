package com.wichelns.devaproject.app.dao.user.repo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.user.mapper.RequirementUserMapper;
import com.wichelns.devaproject.app.module.user.entity.RequirementUser;
import org.springframework.stereotype.Repository;

/**
 * @author zhijia
 * @date 2024/2/14 11:03
 */
@Repository
public class RequirementUserRepo extends ServiceImpl<RequirementUserMapper, RequirementUser> implements IService<RequirementUser> {
}
