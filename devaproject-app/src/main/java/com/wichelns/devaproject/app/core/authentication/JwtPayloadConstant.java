package com.wichelns.devaproject.app.core.authentication;

import com.auth0.jwt.impl.PublicClaims;

/**
 * @author zhijia
 * @date 2024/2/15 22:36
 */
public class JwtPayloadConstant {
	public static final String CLAIM_USERID = "userId";
	public static final String CLAIM_USERTYPE = "userType";
	public static final String CLAIM_EXP = PublicClaims.EXPIRES_AT;
}
