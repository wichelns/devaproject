package com.wichelns.devaproject.app.module.system.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhijia
 * @date 2024/2/16 0:07
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/system")
@Api(value = "系统公共API", tags = "Web - SystemApi")
public class SystemApi {

	@GetMapping(value = "/smoke")
	@ApiOperation(httpMethod = "GET", value = "smoke")
	public String smoke() {
		return "health";
	}

}
