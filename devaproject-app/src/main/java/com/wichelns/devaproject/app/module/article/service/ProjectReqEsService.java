package com.wichelns.devaproject.app.module.article.service;

import com.wichelns.devaproject.app.module.article.dto.ProReqDto;
import com.wichelns.devaproject.app.module.article.param.ProjectReqRequestParams;
import com.wichelns.devaproject.app.module.article.entity.ProjectRequirement;

import java.util.List;

/**
 * @Author zhuyu
 * @update 2024/3/3 17:33
 */
public interface ProjectReqEsService {
    /***
     * 查询项目需求列表
     */
    List<ProReqDto> search(ProjectReqRequestParams projectReqRequestParams);
    /**
     * 新增or修改项目需求列表（es）
     */
    void insertOrUpdateEsById(Integer projectId);
    /**
     * 删除项目需求列表（es 设置deleted为空）
     */
    void deleteById(ProjectRequirement projectRequirement);
    /**
     * 删除项目需求列表（es）
     */
    void deleteById2(ProjectRequirement projectRequirement);
    /***
     * 补全查询
     * @param prefix
     * @return
     */
    List<String> getSuggestion(String prefix);

    /***
     * 恢复删除
     * @param projectRequirement
     */
    void undeleteById(ProjectRequirement projectRequirement);
}
