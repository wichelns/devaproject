package com.wichelns.devaproject.app.module.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wichelns.devaproject.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhijia
 * @date 2024/2/14 10:46
 */
@ApiModel(value = "系统文档")
@TableName(value = "sys_doc", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
public class SysDoc extends BaseEntity {
	// ID、type、content、status、extra（操作者ID、上一版本ID）
	@TableId(type = IdType.AUTO)
	@ApiModelProperty(value = "文档ID")
	private Integer docId;
	@ApiModelProperty(value =
			"文档类型 10-用户协议 20-隐私协议 30-需求用户手册40-开发者手册 50-业务配置 60-系统配置")
	private Integer type;
	@ApiModelProperty(value = "内容", notes = "注意：业务配置、系统配置须为JSON格式")
	private String content;
	@ApiModelProperty(value = "状态")
	private Integer status;
	@ApiModelProperty(value = "额外信息 包括操作者、上一版本ID，系统配置过的ID桶也需要记录在内，校验ID桶分配重复")
	private String extra;
}
