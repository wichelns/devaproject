package com.wichelns.devaproject.app.config;


import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson2.support.spring.data.redis.FastJsonRedisSerializer;
import com.wichelns.devaproject.app.constant.MQConstant;
import com.wichelns.devaproject.app.core.properties.DevaprojectProperties;
import com.wichelns.devaproject.common.annotation.Topic;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.Map;

/**
 * redis配置
 */
@Configuration
@EnableConfigurationProperties({DevaprojectProperties.class})
@ImportAutoConfiguration(SpringUtil.class)
public class RedisConfig {


    /**
     * Redis配置
     */
    @Bean
    @ConditionalOnClass(RedisOperations.class)
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);

        // 序列化
        FastJsonRedisSerializer<Object> fastJsonRedisSerializer = new FastJsonRedisSerializer<>(Object.class);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

        // key采用 String的序列化方式
        template.setKeySerializer(stringRedisSerializer);
        // hash的 key也采用 String的序列化方式
        template.setHashKeySerializer(stringRedisSerializer);
        // value序列化方式采用 fastjson
        template.setValueSerializer(fastJsonRedisSerializer);
        // hash的 value序列化方式采用 fastjson
        template.setHashValueSerializer(fastJsonRedisSerializer);
        template.afterPropertiesSet();

        return template;
    }

    /**
     * 配置redis监听类，开启监听: pubsub模式
     */
    @Bean
    @ConditionalOnProperty(value = "mq-type", havingValue = MQConstant.MQ_TYPE_PUBSUB)
    public RedisMessageListenerContainer redisMessageListenerContainer(RedisConnectionFactory connectionFactory, ApplicationContext applicationContext) {
        RedisMessageListenerContainer listenerContainer = new RedisMessageListenerContainer();
        listenerContainer.setConnectionFactory(connectionFactory);

        //设置监听的对象（类）,可以配置多个
        Map<String, MessageListener> listenerMap = applicationContext.getBeansOfType(MessageListener.class);
        for (Map.Entry<String, MessageListener> listenerEntry : listenerMap.entrySet()) {
            MessageListener messageListener = listenerEntry.getValue();
            String simpleName = messageListener.getClass().getSimpleName();
            Topic annotation = messageListener.getClass().getAnnotation(Topic.class);
            listenerContainer.addMessageListener(messageListener, new PatternTopic(annotation.value()));
        }
        return listenerContainer;
    }
}
