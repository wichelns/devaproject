package com.wichelns.devaproject.app.dao.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wichelns.devaproject.app.module.system.entity.SysDoc;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhijia
 * @date 2024/2/14 10:57
 */
@Mapper
public interface SysDocMapper extends BaseMapper<SysDoc> {
	int updateContentById(SysDoc sysDoc);
}
