package com.wichelns.devaproject.app.module.article.logic;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.wichelns.devaproject.app.dao.article.repo.ProjectRequirementRepo;
import com.wichelns.devaproject.app.module.article.entity.ProjectRequirement;
import com.wichelns.devaproject.app.module.article.exception.ArticleNormalException;
import com.wichelns.devaproject.app.module.article.util.TagUtil;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import com.wichelns.devaproject.common.toolkit.DevaprojectUtil;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

import static com.wichelns.devaproject.app.module.article.constant.ArticleConstant.*;

/**
 * @author zhijia
 * @date 2024/3/2 13:45
 */
@Component
public class ProjectReqLogic {
	@Resource
	private ProjectRequirementRepo projectRequirementRepo;

	public void checkCanPublish(ProjectRequirement projectReq) {
		int titleLen = projectReq.getTitle().length();
		int contentLen = projectReq.getContent().length();
		Double reward = projectReq.getReward();
		List<String> tagList = DevaprojectUtil.arrStrToList(projectReq.getTagList());

		//调整taglist格式
		projectReq.setTagList(TagUtil.checkTagList(tagList));

		if (titleLen >= TITLE_MIN_LEN && titleLen <= TITLE_MAX_LEN
				&& contentLen >= CONTENT_MIN_LEN && contentLen <= CONTENT_MAX_LEN
				&& reward >= REWARD_MIN
				&& CollUtil.isNotEmpty(tagList)) {
			return;
		}
		throw new BusinessException(ErrorCodeEnum.PR10060001);
	}

	/**
	 * 校验用户权限、项目状态
	 *
	 * @param projectId     待检查的项目ID
	 * @param toCheckStatus 受检状态
	 */
	public void projectCheck(Long reqUserId, Integer projectId, Object... toCheckStatus) {
		// 判断该项目是否为该用户的项目
		LambdaQueryChainWrapper<ProjectRequirement> qwForPR = projectRequirementRepo.lambdaQuery()
				.eq(ProjectRequirement::getProjectId, projectId)
				.eq(ProjectRequirement::getReqUserId, reqUserId);
		if (projectRequirementRepo.count(qwForPR) == 0) {
			throw new ArticleNormalException(USER_PROJECT_MISMATCH);
		}

		// 判断项目状态是否正确
		qwForPR.in(ProjectRequirement::getStatus, toCheckStatus);
		if (projectRequirementRepo.count(qwForPR) == 0) {
			throw new ArticleNormalException(PROJECT_STATUS);
		}
	}

}
