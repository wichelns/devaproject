package com.wichelns.devaproject.app.module.team.param;

import com.wichelns.devaproject.common.model.BaseParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

/**
 * @author zhijia
 * @date 2024/2/28 23:26
 */
@Data
@ApiModel(description = "团队Param")
@EqualsAndHashCode(callSuper = false)
public class TeamParam extends BaseParam {
	@ApiModelProperty(value = "团队名称")
	private Integer teamId;
	@Length(min = 2, max = 20, groups = add.class, message = "团队名称5~20字符")
	@NotNull(groups = add.class, message = "团队名称不能为空")
	@ApiModelProperty(value = "团队名称")
	private String teamName;
	@ApiModelProperty(value = "团队头像")
	private String avatar;
	@Length(min = 5, max = 100, groups = add.class, message = "团队介绍5~100字符")
	@NotNull(groups = add.class, message = "团队介绍不能为空")
	@ApiModelProperty(value = "团队介绍")
	private String profile;
	@Range(min = 2, max = 20, groups = add.class, message = "团队最大人数为2~20")
	@NotNull(groups = add.class, message = "团队最大人数不能为空")
	@ApiModelProperty(value = "团队最大人数")
	private Integer maxMemberCount;
}
