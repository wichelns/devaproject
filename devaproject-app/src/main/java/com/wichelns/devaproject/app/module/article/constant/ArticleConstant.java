package com.wichelns.devaproject.app.module.article.constant;

/**
 * @author 淡漠
 * @version 1.0
 */
public interface ArticleConstant {
    //正则表达式限制只包含字母、数字和中文字符
    String NORMAL_CHARACTERS="^[a-zA-Z0-9\\u4e00-\\u9fa5]+$";
    //参数信息错误
    String PARAMETER_ERROR="参数信息错误";
    //id格式错误
    String ID_FORMAT_ERROR="id格式错误";
    //用户与项目不匹配
    String USER_PROJECT_MISMATCH="用户与项目不匹配";
    //项目状态不正确
    String PROJECT_STATUS="项目状态不正确";

    /*
    * 参数校验
    * */
    int TITLE_MIN_LEN = 1;
    int TITLE_MAX_LEN = 20;
    int DESCRIPTION_MAX_LEN = 100;
    int CONTENT_MIN_LEN = 50;
    int CONTENT_MAX_LEN = 2000;
    double REWARD_MIN = 0.00;

}
