package com.wichelns.devaproject.app.monitor.transaction.dto;

import lombok.Data;

import java.util.List;

/**
 * @author zhijia
 * @date 2023/9/7 22:47
 */
@Data
public class TransactionDTO {
    private String connectionId;
    private long trxBeginTimeMillis;
    private long trxEndTimeMillis;
    private String stackTraceStr;
    private List<String> sqlList;
}
