package com.wichelns.devaproject.app.module.project.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wichelns.devaproject.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * @author zhijia
 * @date 2024/2/19 19:08
 */
@ApiModel(value = "团队项目")
@TableName(value = "team_project", autoResultMap = true)
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
public class TeamProject extends BaseEntity {
	// ID、项目id、团队id、leaderId、extra、参选介绍、状态（候选中、开发指认中、开发中、交付中、已交付、已放弃、被解雇）、交付时间、评语、星星
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "团队项目ID")
	private Long teamProjId;
	@ApiModelProperty(value = "项目ID")
	private Integer projectId;
	@ApiModelProperty(value = "团队ID")
	private Integer teamId;
	@ApiModelProperty(value = "队长ID")
	private Long leaderId;
	@ApiModelProperty(value = "额外信息")
	private String extra;
	@ApiModelProperty(value = "参选介绍")
	private String joinIntro;
	@ApiModelProperty(value = "状态")
	private Integer status;
	@ApiModelProperty(value = "交付时间")
	private Date deliverTime;
	@ApiModelProperty(value = "评语")
	private String comment;
	@ApiModelProperty(value = "星星数")
	private Integer stars;

	@Tolerate
	public TeamProject() {}

}
