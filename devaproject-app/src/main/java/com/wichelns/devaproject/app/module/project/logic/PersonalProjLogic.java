package com.wichelns.devaproject.app.module.project.logic;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wichelns.devaproject.app.module.project.convert.DeveloperProjConvert;
import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.module.project.entity.PersonalProject;
import com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import org.springframework.stereotype.Component;

import java.util.Arrays;

import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum.DISMISSED;
import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum.QUITED;

@Component
public class PersonalProjLogic extends AbstractDevProjLogic {

	@Override
	public void checkCanJoin(Integer projectId, Long devUserId, Integer teamId) {
		// 1
		LambdaQueryWrapper<PersonalProject> qwForPP = Wrappers.<PersonalProject>lambdaQuery()
				.eq(PersonalProject::getDevUserId, devUserId)
				.eq(PersonalProject::getProjectId, projectId)
				.isNull(PersonalProject::getTeamProjId)
				.notIn(PersonalProject::getStatus, Arrays.asList(QUITED.getCode(), DISMISSED.getCode()));
		if (personalProjectRepo.count(qwForPP) != 0) {
			throw new BusinessException(ErrorCodeEnum.PC10040000);
		}
		// 3 todo
	}

	@Override
	public void checkCanManageDevProj(Long devUserId, Long devProjId) {
		Integer count = personalProjectRepo.lambdaQuery()
				.eq(PersonalProject::getPersonalProjId, devProjId)
				.eq(PersonalProject::getDevUserId, devUserId)
				.isNull(PersonalProject::getTeamProjId).count();
		if (count != 1) {
			throw new BusinessException(ErrorCodeEnum.PC10040001);
		}
	}

	@Override
	DeveloperProjDTO getDTOById4Update(Long devProjId) {
		PersonalProject personalProject = personalProjectRepo.getBaseMapper().lockForUpdateById(devProjId);
		return DeveloperProjConvert.INSTANCE.domain2DTO(personalProject);
	}

	@Override
	void changeDevProjStatus(Long devProjId, DeveloperProjStatusEnum targetStatus) {
		personalProjectRepo.lambdaUpdate().set(PersonalProject::getStatus, targetStatus.getCode())
				.eq(PersonalProject::getPersonalProjId, devProjId).update();
	}

	@Override
	void evaluateDevProj(Long devProjId, String comment, Integer stars) {
		personalProjectRepo.lambdaUpdate().set(PersonalProject::getComment, comment)
				.set(PersonalProject::getStars, stars)
				.eq(PersonalProject::getPersonalProjId, devProjId).update();
	}
}