package com.wichelns.devaproject.app.module.team.api;

import com.wichelns.devaproject.app.module.secure.annotation.DevelopUserValid;
import com.wichelns.devaproject.app.module.team.service.TeamMemberService;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/16 0:07
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/teammember")
@Api(value = "团队API", tags = "Web - TeamApi")
public class TeamMemberApi {
	@Resource
	private TeamMemberService teamMemberService;

	@GetMapping(value = "/query")
	@ApiOperation(httpMethod = "GET", value = "查询队员列表")
	@DevelopUserValid
	public Wrapper<?> query() {

		return WrapMapper.ok();
	}

	@PostMapping(value = "/kick")
	@ApiOperation(httpMethod = "POST", value = "踢人")
	@DevelopUserValid
	public Wrapper<?> kick(@ApiParam(value = "队员ID") @RequestParam(value = "memberId") Long memberId) {
		teamMemberService.kick(memberId);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/quit")
	@ApiOperation(httpMethod = "POST", value = "退出团队")
	@DevelopUserValid
	public Wrapper<?> quit(@ApiParam(value = "团队ID") @RequestParam(value = "teamId") Integer teamId) {
		teamMemberService.quitTeam(teamId);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/remark")
	@ApiOperation(httpMethod = "POST", value = "备注队员")
	@DevelopUserValid
	public Wrapper<?> remark(@ApiParam(value = "队员ID") @RequestParam(value = "memberId") Long memberId,
							 @ApiParam(value = "备注") @RequestParam(value = "remark") String remark) {
		teamMemberService.remark(memberId, remark);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/attachRespTag")
	@ApiOperation(httpMethod = "POST", value = "分配职责标签")
	@DevelopUserValid
	public Wrapper<?> attachRespTag(@ApiParam(value = "队员ID") @RequestParam(value = "memberId") Long memberId,
									@ApiParam(value = "职责标签ID") @RequestParam(value = "respTagId") String respTagId) {
		teamMemberService.attachRespTag(memberId, respTagId);
		return WrapMapper.ok();
	}



}
