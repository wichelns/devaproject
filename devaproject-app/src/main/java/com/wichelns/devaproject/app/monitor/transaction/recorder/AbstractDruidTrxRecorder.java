package com.wichelns.devaproject.app.monitor.transaction.recorder;

import cn.hutool.core.util.ArrayUtil;
import com.alibaba.druid.filter.FilterChain;
import com.alibaba.druid.proxy.jdbc.ConnectionProxy;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;

/**
 * @author zhijia
 * @date 2023/9/8 12:51
 */
public abstract class AbstractDruidTrxRecorder {

    public abstract void afterTrxBegin(FilterChain chain, ConnectionProxy connection) throws SQLException;

    public abstract void afterTrxCommit(FilterChain chain, ConnectionProxy connection) throws SQLException;
    
    public abstract void afterTrxRollback(FilterChain chain, ConnectionProxy connection) throws SQLException;

    public abstract void afterTrxRollback(FilterChain chain, ConnectionProxy connection, Savepoint savepoint) throws SQLException;

    protected String getConnectionId(Connection connection) throws SQLException {
        if (connection == null || connection.isClosed()) {
            return null;
        }

        ResultSet resultSet = connection.prepareStatement("SELECT connection_id()").executeQuery();
        if (resultSet.next()) {
            return resultSet.getString(1);
        } else {
            return null;
        }
    }

    protected String getStackTraceStr() {
        return ArrayUtil.join(Thread.currentThread().getStackTrace(), "\n\tat ");
    }

}
