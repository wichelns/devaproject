package com.wichelns.devaproject.app.dao.team.repo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.team.mapper.RespTagLibMapper;
import com.wichelns.devaproject.app.module.team.entity.RespTagLib;
import org.springframework.stereotype.Repository;

/**
 * @author zhijia
 * @date 2024/3/3 14:21
 */
@Repository
public class RespTagLibRepo extends ServiceImpl<RespTagLibMapper, RespTagLib> implements IService<RespTagLib> {
}
