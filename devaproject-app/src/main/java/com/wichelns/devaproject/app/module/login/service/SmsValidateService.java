package com.wichelns.devaproject.app.module.login.service;

import cn.hutool.core.lang.Pair;

/**
 * 描述：验证相关
 *
 * @author Yoo
 * @date 2020/9/30
 * @since 1.0
 */
public interface SmsValidateService {

    /**
     * 生成验证码
     *
     * @param key     验证码存储的Key
     * @param timeout 过期时间 单位：秒
     * @return
     */
    Pair<String, String> genValidateCode(String key, Integer timeout);

    /**
     * 校验验证码
     *
     * @param key  验证码存储的Key
     * @param code 验证码
     * @return
     */
    Boolean checkValidateCode(String key, String code);
}
