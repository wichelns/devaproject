package com.wichelns.devaproject.app.core.context;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.auth0.jwt.interfaces.Claim;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.wichelns.devaproject.app.constant.CacheConstant.LOGIN_USER_TEMPLATE;
import static com.wichelns.devaproject.app.constant.CacheConstant.TOKEN_TIMEOUT;
import static com.wichelns.devaproject.app.core.authentication.JwtPayloadConstant.CLAIM_USERID;
import static com.wichelns.devaproject.app.core.authentication.JwtPayloadConstant.CLAIM_USERTYPE;

/**
 * 用户上下文信息
 */
@Component
public class SysUserContext {

    public static SysUserContext holder() {
        return SpringUtil.getBean(SysUserContext.class);
    }

    @Resource
    private RedisTemplate<String, String> redisTemplate;
	private static final DefaultRedisScript<Long> NEXT_ID_SCRIPT;
	private static final long LEFT_SHIFT_BITS = 25;
	private static final long MAX_SUFFIX = ~(-1 << LEFT_SHIFT_BITS);

	static {
		NEXT_ID_SCRIPT = new DefaultRedisScript<>();
		NEXT_ID_SCRIPT.setLocation(new ClassPathResource("next-id.lua"));
		NEXT_ID_SCRIPT.setResultType(Long.class);
	}


	@SuppressWarnings("unchecked")
    public Long nextUserId() {
		// 整个步骤循环，因为获取到的ID桶可能是无效的了
		while (true) {
			Map<Object, Object> validIdBucketKeys = SystemContext.holder().opsForSystem().getValidIdBucketKeys();
			if (validIdBucketKeys.isEmpty()) {
				throw new BusinessException(ErrorCodeEnum.UAC10010027);
			}
			// 随机一个元素 key->leadingNum
			Map.Entry<Object, Object> entry = (Map.Entry<Object, Object>) RandomUtil.randomEle(validIdBucketKeys.entrySet().toArray());
			Long nextId = nextId(entry.getKey(), entry.getValue());
			if (nextId != -1) {// 无效ID桶
				return nextId;
			}
		}
    }

	/**
	 * @param key ID桶的key
	 * @param leadingNum 左位移前的十进制数，用于区分ID桶
	 * @return 下一个ID，不为null，-1表示无效ID
	 */
	private Long nextId(Object key, Object leadingNum) {
		long increment = RandomUtil.randomLong(1L, 10L);
		long initId = Long.parseLong(leadingNum.toString()) << LEFT_SHIFT_BITS;
		long upperbound = initId | MAX_SUFFIX;
		return redisTemplate.execute(NEXT_ID_SCRIPT, Collections.singletonList(key.toString()), increment, initId, upperbound);
	}

	public boolean tokenNotExpired(Long userId, Integer userType) {
		String token = redisTemplate.opsForValue().get(StrUtil.format(LOGIN_USER_TEMPLATE, userType, userId));
		return token != null;
	}


    public void refreshToken(String token) {
		Map<String, Claim> claimMap = Jwt.parseToken(token);
		Long userId = claimMap.get(CLAIM_USERID).asLong();
		Integer userType = claimMap.get(CLAIM_USERTYPE).asInt();

		long loginTimeout = Long.parseLong(SystemContext.holder().opsForSystem().getValue(TOKEN_TIMEOUT, "10000"));
        redisTemplate.expire(StrUtil.format(LOGIN_USER_TEMPLATE, userType, userId), loginTimeout, TimeUnit.SECONDS);
    }

	public static long parseIdLeadingNum(long userId) {
		return userId >>> LEFT_SHIFT_BITS;
	}

	public static long parseIdLowerBound(long userId) {
		return parseIdLeadingNum(userId) << LEFT_SHIFT_BITS;
	}

}
