package com.wichelns.devaproject.app.core.file.common.dto;

import lombok.Data;

@Data
public class PreUploadDTO {
    private String url;
    private String preUrl;
}
