package com.wichelns.devaproject.app.module.message.request.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.dao.message.repo.RequestRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamMemberRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamRepo;
import com.wichelns.devaproject.app.module.message.request.entity.Request;
import com.wichelns.devaproject.app.module.message.request.enums.RequestStatusEnum;
import com.wichelns.devaproject.app.module.message.request.param.ReqParam;
import com.wichelns.devaproject.app.module.message.request.service.RequestService;
import com.wichelns.devaproject.app.module.message.request.strategy.ReqQueryStrategy;
import com.wichelns.devaproject.app.module.message.request.strategy.ReqQueryStrategyFactory;
import com.wichelns.devaproject.app.module.message.request.vo.AbstractReqVO;
import com.wichelns.devaproject.app.module.project.enums.DeveloperTypeEnum;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.app.module.team.entity.TeamMember;
import com.wichelns.devaproject.app.module.team.enums.MemberIdentityEnum;
import com.wichelns.devaproject.app.module.team.logic.TeamLogic;
import com.wichelns.devaproject.common.exception.BusinessException;
import com.wichelns.devaproject.common.toolkit.HttpContextUtil;
import com.wichelns.devaproject.common.toolkit.IpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.wichelns.devaproject.app.module.message.request.enums.OperationEnum.INVITATION_JOIN_TEAM;
import static com.wichelns.devaproject.app.module.message.request.enums.OperationEnum.REQUEST_JOIN_TEAM;
import static com.wichelns.devaproject.common.enums.ErrorCodeEnum.*;

/**
 * @author zhijia
 * @date 2024/2/29 20:52
 */
@Slf4j
@Service
public class RequestServiceImpl implements RequestService {
	@Resource
	private ReqQueryStrategyFactory reqQueryStrategyFactory;
	@Resource
	private RequestRepo requestRepo;
	@Resource
	private TeamRepo teamRepo;
	@Resource
	private TeamMemberRepo teamMemberRepo;


	@Override
	public void sendReq(ReqParam param) {
		Long devUserId = Jwt.getLoginUserId();
		if (INVITATION_JOIN_TEAM.getCode().equals(param.getOperation())) {
			if (!TeamLogic.isLeader(param.getSourceId().intValue(), devUserId)) {
				throw new BusinessException(GL99990401, "非队长，权限不足");
			}
		}
		if (Stream.of(INVITATION_JOIN_TEAM, REQUEST_JOIN_TEAM).anyMatch(op -> op.getCode().equals(param.getOperation()))) {
			boolean teamOp = INVITATION_JOIN_TEAM.getCode().equals(param.getOperation());
			// 查看是否团队成员
			Long toCheckId = teamOp ? param.getTargetId() : devUserId;
			Long teamId = teamOp ? param.getSourceId() : param.getTargetId();
			if (TeamLogic.isMember(teamId.intValue(), toCheckId)) {
				throw new BusinessException(TC10050001);
			}
			Request request = Request.builder()
					.operation(param.getOperation())
					.sourceId(teamOp ? teamId : devUserId)
					.sourceType(teamOp ? DeveloperTypeEnum.DEVELOPER_TEAM.getCode() : DeveloperTypeEnum.DEVELOPER_USER.getCode())
					.targetId(teamOp ? param.getTargetId() : teamId)
					.targetType(teamOp ? DeveloperTypeEnum.DEVELOPER_USER.getCode() : DeveloperTypeEnum.DEVELOPER_TEAM.getCode())
					.build();
			requestRepo.save(request);
		}
	}

	@Override
	public IPage<AbstractReqVO> pageReq(int op, int isSender, int pageNo, int pageSize) {
		// 获取对应查询策略
		ReqQueryStrategy reqQueryStrategy = reqQueryStrategyFactory.getStrategy(op, isSender);
		IPage<AbstractReqVO> reqVOPage = reqQueryStrategy.pageRequest(null, pageNo, pageSize);
		// 置为已读
		List<Long> toMarkRead = reqVOPage.getRecords().stream().filter(reqVO -> RequestStatusEnum.TO_READ.getCode().equals(reqVO.getStatus()))
				.map(AbstractReqVO::getReqId).collect(Collectors.toList());
		if (CollUtil.isNotEmpty(toMarkRead)) {
			requestRepo.lambdaUpdate().set(Request::getStatus, RequestStatusEnum.TO_OPERATE.getCode())
					.in(Request::getReqId, toMarkRead).update();
		}
		return reqVOPage;
	}

	@Transactional
	@Override
	public void confirmReq(long reqId) {
		Long loginUserId = Jwt.getLoginUserId();
		// todo 前端防抖处理
		Request request = requestRepo.getById(reqId);
		if (!RequestStatusEnum.TO_OPERATE.getCode().equals(request.getStatus())) {
			throw new BusinessException(GL99990100, "请求状态非法");
		}
		// 入队处理
		if (Stream.of(INVITATION_JOIN_TEAM, REQUEST_JOIN_TEAM).anyMatch(op -> op.getCode().equals(request.getOperation()))) {
			boolean invite = INVITATION_JOIN_TEAM.getCode().equals(request.getOperation());
			// 1.检查是否在团队中
			Long toCheckId = invite ? request.getTargetId() : request.getSourceId();
			Long teamId = invite ? request.getSourceId() : request.getTargetId();
			if (TeamLogic.isMember(teamId.intValue(), toCheckId)) {
				throw new BusinessException(TC10050001);
			}
			// 2.是否解散了
			Team team = teamRepo.getById(teamId);
			if (team == null) {
				throw new BusinessException(GL9999404, "团队id=" + teamId);
			}
			// 3.权限校验
			if (!invite && !TeamLogic.isLeader(teamId.intValue(), loginUserId)) {
				throw new BusinessException(GL99990401, "非队长，权限不足");
			}
			if (invite && ObjectUtil.notEqual(loginUserId, toCheckId)) {
				// todo 记录相关信息，风险防控
				String remoteAddr = IpUtil.getRemoteAddr(HttpContextUtil.getHttpServletRequest());
				log.warn("非本人操作, IP=[{}]", remoteAddr);
				riskControl(remoteAddr);
				throw new BusinessException(GL99990401);// 不是本人操作
			}
			// 4.更新为已确认、团队人数校验增加
			requestRepo.lambdaUpdate().set(Request::getStatus, RequestStatusEnum.CONFIRMED.getCode())
					.eq(Request::getReqId, reqId).update();
			if (teamRepo.getBaseMapper().incCurMemById(teamId.intValue(), 1) != 1) {
				throw new BusinessException(TC10050002);
			}
			// 5.新增member
			TeamMember member = new TeamMember();
			member.setDevUserId(toCheckId);
			member.setTeamId(teamId.intValue());
			member.setIdentity(MemberIdentityEnum.COMMON_MEMBER.getCode());
			teamMemberRepo.save(member);
		}
	}

	@Override
	public void rejectReq(long reqId) {
		Long loginUserId = Jwt.getLoginUserId();
		Request request = requestRepo.getById(reqId);
		if (!RequestStatusEnum.TO_OPERATE.getCode().equals(request.getStatus())) {
			throw new BusinessException(GL99990100, "请求状态非法");
		}
		if (REQUEST_JOIN_TEAM.getCode().equals(request.getOperation())) {
			Long teamId = request.getTargetId();
			if (!TeamLogic.isLeader(teamId.intValue(), loginUserId)) {
				throw new BusinessException(GL99990401, "非队长，权限不足");
			}
		}
		if (INVITATION_JOIN_TEAM.getCode().equals(request.getOperation())) {
			if (ObjectUtil.notEqual(loginUserId, request.getTargetId())) {
				// todo 记录相关信息，风险防控
				String remoteAddr = IpUtil.getRemoteAddr(HttpContextUtil.getHttpServletRequest());
				log.warn("非本人操作, IP=[{}]", remoteAddr);
				riskControl(remoteAddr);
				throw new BusinessException(GL99990401);// 不是本人操作
			}
		}
		requestRepo.lambdaUpdate().set(Request::getStatus, RequestStatusEnum.REJECTED.getCode())
				.eq(Request::getReqId, reqId).update();
	}

	private void riskControl(String remoteAddr) {

	}
}
