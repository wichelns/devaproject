package com.wichelns.devaproject.app.module.message.request.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 抽象请求VO类
 *
 * @author zhijia
 * @date 2024/2/28 23:25
 */
@Getter
@Setter
public abstract class AbstractReqVO {
	@ApiModelProperty(value = "请求ID")
	private Long reqId;
	@ApiModelProperty(value = "状态 10-未读 20-已读未操作 30-已确认 40-已拒绝")
	private Integer status;
}
