package com.wichelns.devaproject.app.module.message.request.strategy;

import com.wichelns.devaproject.app.module.message.request.enums.OperationEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhijia
 * @date 2024/2/28 23:19
 */
@Component
@RequiredArgsConstructor
public class ReqQueryStrategyFactory {
	private final List<ReqQueryStrategy> strategyList;

	public ReqQueryStrategy getStrategy(int code, int isSender) {
		OperationEnum attempt = OperationEnum.getByCode(code);
		for (ReqQueryStrategy reqQueryStrategy : strategyList) {
			if (reqQueryStrategy.support(attempt, isSender)) {
				return reqQueryStrategy;
			}
		}

		throw new BusinessException("操作非法");// todo 异常
	}

}
