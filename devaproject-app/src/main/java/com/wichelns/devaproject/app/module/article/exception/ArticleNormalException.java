package com.wichelns.devaproject.app.module.article.exception;

/**
 * @author 淡漠
 * @version 1.0
 */
public class ArticleNormalException extends RuntimeException{
    public ArticleNormalException(String message){
        super(message);
    }
}
