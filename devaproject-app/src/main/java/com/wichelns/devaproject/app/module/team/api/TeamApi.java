package com.wichelns.devaproject.app.module.team.api;

import com.wichelns.devaproject.app.module.secure.annotation.DevelopUserValid;
import com.wichelns.devaproject.app.module.team.param.TeamParam;
import com.wichelns.devaproject.app.module.team.service.TeamService;
import com.wichelns.devaproject.common.model.BaseParam;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/16 0:07
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/team")
@Api(value = "团队API", tags = "Web - TeamApi")
public class TeamApi {

	@Resource
	private TeamService teamService;

	@PostMapping(value = "/addOrEdit")
	@ApiOperation(httpMethod = "POST", value = "新建/编辑团队")
	@DevelopUserValid
	public Wrapper<?> addOrEdit(@RequestBody @Validated(BaseParam.add.class) TeamParam param) {
		teamService.addOrEdit(param);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/changeLeader")
	@ApiOperation(httpMethod = "POST", value = "转让队长")
	@DevelopUserValid
	public Wrapper<?> changeLeader(@ApiParam(value = "团队ID") @RequestParam(value = "teamId") Integer teamId,
								   @ApiParam(value = "目标队员ID") @RequestParam(value = "targetMemId") Long targetMemId) {
		teamService.changeLeader(teamId, targetMemId);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/breakUp")
	@ApiOperation(httpMethod = "POST", value = "解散团队")
	@DevelopUserValid
	public Wrapper<?> breakUp(@ApiParam(value = "团队ID") @RequestParam(value = "teamId") Integer teamId) {
		teamService.breakUp(teamId);
		return WrapMapper.ok();
	}

}
