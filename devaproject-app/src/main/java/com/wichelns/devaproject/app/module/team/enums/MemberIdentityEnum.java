package com.wichelns.devaproject.app.module.team.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhijia
 * @date 2024/2/23 13:01
 */
@Getter
@AllArgsConstructor
public enum MemberIdentityEnum {
	LEADER(1, "队长"),
	COMMON_MEMBER(5, "普通成员");

	private final Integer code;
	private final String desc;
}
