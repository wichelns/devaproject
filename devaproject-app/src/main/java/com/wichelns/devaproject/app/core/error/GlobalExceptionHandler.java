package com.wichelns.devaproject.app.core.error;

import cn.hutool.core.util.StrUtil;
import com.wichelns.devaproject.app.module.article.exception.ArticleNormalException;
import com.wichelns.devaproject.app.module.article.exception.ParameterException;
import com.wichelns.devaproject.common.exception.BusinessException;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Map;

/**
 * 全局的的异常拦截器
 *
 * @author yoo
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    //对参数不符合规范的异常进行处理，返回不符合规范的字段以及错误信息
    @ResponseBody
    @ExceptionHandler(value = {ParameterException.class})
    public Wrapper<Map<String, Object>> handleException(ParameterException e) {
        // 执行自定义异常处理逻辑
        return WrapMapper.parameterError(e.getHashMap());
    }

    @ResponseBody
    @ExceptionHandler(value = {ArticleNormalException.class})
    public Wrapper<Object> handleException(ArticleNormalException e) {
        // 执行自定义异常处理逻辑
        return WrapMapper.parameterError(e.getMessage());
    }

    /**
     * 拦截参数校验错误异常
     *
     * @author xuyuxiang
     * @date 2020/3/18 19:41
     */
    @ExceptionHandler(BindException.class)
    @ResponseBody
    public Wrapper paramError(BindException e) {
        String argNotValidMessage = getArgNotValidMessage(e.getBindingResult());
        log.error(">>> 参数校验错误异常，具体信息为：{}", argNotValidMessage);
        return WrapMapper.wrap(Wrapper.CLIENT_ERROR_CODE, argNotValidMessage);
    }

    /**
     * 业务异常.
     *
     * @param e the e
     * @return the wrapper
     */
    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Wrapper businessException(BusinessException e) {
        log.warn("业务异常={}", e.getMessage(), e);
        return WrapMapper.wrap(e.getCode(), e.getMessage());
    }

    /**
     * 全局异常.
     *
     * @param e the e
     * @return the wrapper
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Wrapper exception(Exception e) {
        log.error("保存全局异常信息 ex={}", e.getMessage(), e);
        return WrapMapper.error("(;´༎ຶٹ༎ຶ`) 系统遇到了一点小问题");
    }

    /**
     * 获取请求参数不正确的提示信息
     * <p>
     * 多个信息，拼接成用逗号分隔的形式
     *
     * @author yubaoshan
     * @date 2020/5/5 16:50
     */
    private String getArgNotValidMessage(BindingResult bindingResult) {
        if (bindingResult == null) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();

        //多个错误用逗号分隔
        List<ObjectError> allErrorInfos = bindingResult.getAllErrors();
        for (ObjectError error : allErrorInfos) {
            stringBuilder.append(StrUtil.COMMA).append(error.getDefaultMessage());
        }

        //最终把首部的逗号去掉
        return StrUtil.removePrefix(stringBuilder.toString(), StrUtil.COMMA);
    }
}
