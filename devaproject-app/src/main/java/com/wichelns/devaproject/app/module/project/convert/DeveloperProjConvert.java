package com.wichelns.devaproject.app.module.project.convert;

import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.module.project.entity.PersonalProject;
import com.wichelns.devaproject.app.module.project.entity.TeamProject;
import com.wichelns.devaproject.app.module.project.enums.DeveloperProjTypeEnum;
import com.wichelns.devaproject.common.model.BaseConvert;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @author zhijia
 * @date 2024/2/22 23:19
 */
@Mapper(imports = DeveloperProjTypeEnum.class)
public interface DeveloperProjConvert extends BaseConvert {
	DeveloperProjConvert INSTANCE = Mappers.getMapper(DeveloperProjConvert.class);

	@Mapping(target = "devProjType", expression = "java(DeveloperProjTypeEnum.PERSONAL_PROJECT.getCode())")
	@Mapping(target = "devProjId", source = "personalProjId")
	DeveloperProjDTO domain2DTO(PersonalProject personalProject);

	@Mapping(target = "devProjType", expression = "java(DeveloperProjTypeEnum.TEAM_PROJECT.getCode())")
	@Mapping(target = "devProjId", source = "teamProjId")
	DeveloperProjDTO domain2DTO(TeamProject teamProject);

}
