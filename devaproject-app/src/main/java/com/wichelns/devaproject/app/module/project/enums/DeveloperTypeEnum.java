package com.wichelns.devaproject.app.module.project.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhijia
 * @date 2024/2/15 21:37
 */
@Getter
@AllArgsConstructor
public enum DeveloperTypeEnum {
	DEVELOPER_USER(10, "开发者个人"),
	DEVELOPER_TEAM(20, "开发者团队");

	private final Integer code;
	private final String desc;
}
