package com.wichelns.devaproject.app.dao.project.repo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.project.mapper.TeamProjectMapper;
import com.wichelns.devaproject.app.module.project.entity.TeamProject;
import org.springframework.stereotype.Repository;

/**
 * @author zhijia
 * @date 2024/2/14 10:59
 */
@Repository
public class TeamProjectRepo extends ServiceImpl<TeamProjectMapper, TeamProject> implements IService<TeamProject>  {
}
