package com.wichelns.devaproject.app.core.aop;

import cn.hutool.core.util.StrUtil;
import com.wichelns.devaproject.common.annotation.ErrorLog;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


/**
 * 打印消息处理器异常情况日志，格式：类名-方法名: 参数值0 | ...
 */
@Slf4j
@Order(ErrorLog.ORDER)
@Aspect
@Component
public class ErrorLogAop {

    @Pointcut("@annotation(com.wichelns.devaproject.common.annotation.ErrorLog)")
    public void errorLogPointcut() {
    }

    @Around("errorLogPointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String className = signature.getMethod().getDeclaringClass().getSimpleName();
        String methodName = signature.getMethod().getName();
        Object[] args = joinPoint.getArgs();

        StringBuilder logMessage = new StringBuilder();
        logMessage.append(className).append("-").append(methodName).append(": ");
        for (Object arg : args) {
            logMessage.append(arg).append(" | ");
        }
        String logMsg = logMessage.toString();
        logMsg = StrUtil.removeSuffix(logMsg, " | ");

        try {
            return joinPoint.proceed();
        } catch (Exception e) {
            log.info("detail={}", logMsg);
            log.error("msghandler执行失败", e);
            throw e;
        }
    }
}
