package com.wichelns.devaproject.app.module.message.request.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wichelns.devaproject.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;

/**
 * @author zhijia
 * @date 2024/2/28 11:47
 */
@ApiModel(value = "请求")
@TableName(value = "request", autoResultMap = true)
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
public class Request extends BaseEntity {
	// ID、来源Id、来源类型、目标Id、目标类型、操作、extra、状态（未读、已读未操作、已确认、已拒绝）
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "请求ID")
	private Long reqId;
	@ApiModelProperty(value = "来源ID")
	private Long sourceId;
	@ApiModelProperty(value = "来源类型 10-个人 20-团队")
	private Integer sourceType;
	@ApiModelProperty(value = "目标ID")
	private Long targetId;
	@ApiModelProperty(value = "目标类型 10-个人 20-团队")
	private Integer targetType;
	@ApiModelProperty(value = "操作 1-入队邀请 5-入队申请 10-好友申请")
	private Integer operation;
	@ApiModelProperty(value = "extra信息")
	private String extra;
	@ApiModelProperty(value = "状态 10-未读 20-已读未操作 30-已确认 40-已拒绝")
	private Integer status;

	@Tolerate
	public Request() {}
}
