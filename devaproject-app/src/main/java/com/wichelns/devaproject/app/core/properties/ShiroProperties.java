package com.wichelns.devaproject.app.core.properties;

import lombok.Data;

/**
 * Shiro配置
 */
@Data
public class ShiroProperties {
    /**
     * 普通token过期时间
     */
    private int tokenTimeout;
    /**
     * 勾选rememberMe token过期时间
     */
    private int rememberMeTokenTimeout;
    /**
     * 需要认证url
     */
    private String anonUrl;
    /**
     * 鉴权是否关闭
     */
    private Boolean close;
}