package com.wichelns.devaproject.app.module.project.dto;

import lombok.Data;

import java.util.List;

/**
 * @author zhijia
 * @date 2024/2/21 13:11
 */
@Data
public class TeamProjExtraDTO {
	private String avatar;
	private String teamName;
	private List<Long> devUserIdList;
}
