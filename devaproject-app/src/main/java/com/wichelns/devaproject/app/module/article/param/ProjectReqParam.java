package com.wichelns.devaproject.app.module.article.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.util.List;

import static com.wichelns.devaproject.app.module.article.constant.ArticleConstant.*;

/**
 * @author 淡漠
 * @version 1.0
 */
@Getter
@Setter
public class ProjectReqParam {
	@ApiModelProperty(value = "项目需求ID")
	private Integer projectId;

	@NotBlank(message = "标题不能为空")
	@Length(min = TITLE_MIN_LEN, max = TITLE_MAX_LEN, message = "标题不能大于20个字符")
	@Pattern(regexp = NORMAL_CHARACTERS, message = "标题不得包含特殊字符")
	@ApiModelProperty(value = "标题")
	private String title;

	@Length(max = DESCRIPTION_MAX_LEN, message = "简介不能大于100个字符")
	@Pattern(regexp = NORMAL_CHARACTERS, message = "简介不得包含特殊字符")
	@ApiModelProperty(value = "描述/项目简介")
	private String description;


	@ApiModelProperty(value = "内容")
	private String content;

	@NotNull(message = "报酬不能为空")
	@DecimalMin(value = "0.00", message = "最少0.00报酬")
	@ApiModelProperty(value = "报酬", example = "1.00")
	private Double reward;

	@NotEmpty(message = "标签列表不能为空")
	@ApiModelProperty(value = "标签列表 ['ab','bc',...]")
	private List<String> tagList;

	@ApiModelProperty(value = "是否直接发布 1-是 0-否")
	private Integer dirPublish;

	private static final long serialVersionUID = 1L;
}
