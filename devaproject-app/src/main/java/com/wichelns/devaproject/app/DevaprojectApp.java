package com.wichelns.devaproject.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * 客户端启动类
 *
 * @author zhijia
 * @date 2024/2/9 17:53
 */
@EnableWebMvc
@SpringBootApplication
public class DevaprojectApp {

	static {
		// 使用validationQuery进行心跳检测，防止warn:discard long time none received connection
		// 暂时写在这里 todo move
		System.setProperty("druid.mysql.usePingMethod", "false");
	}

	public static void main(String[] args) {
		SpringApplication.run(DevaprojectApp.class, args);
	}
}
