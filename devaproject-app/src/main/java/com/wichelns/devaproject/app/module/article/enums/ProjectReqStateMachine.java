package com.wichelns.devaproject.app.module.article.enums;

import com.wichelns.devaproject.app.core.fsm.AbstractStateMachine;
import com.wichelns.devaproject.app.core.fsm.BaseStateMachine;

import static com.wichelns.devaproject.app.module.article.enums.ProjectReqEvent.*;
import static com.wichelns.devaproject.app.module.article.enums.ProjectReqState.*;

/**
 * @author zhijia
 * @date 2024/2/17 15:43
 */
public class ProjectReqStateMachine extends AbstractStateMachine<ProjectReqState, ProjectReqEvent> {

	private static final BaseStateMachine<ProjectReqState, ProjectReqEvent> INSTANCE = new ProjectReqStateMachine();

	public static ProjectReqState getTargetState_(ProjectReqState sourceState, ProjectReqEvent event) {
		return INSTANCE.getTargetState(sourceState, event);
	}

	{
		// 【新增】
		accept(null, ADD_DRAFT, DRAFT);
		accept(null, PUBLISH, PUBLISHED);

		// 【草稿】
		accept(DRAFT, MODIFY, DRAFT);
		accept(DRAFT, PUBLISH, PUBLISHED);

		// 【已发布】
		accept(PUBLISHED, MODIFY, PUBLISHED);
		accept(PUBLISHED, WITHDRAW, DRAFT);
		accept(PUBLISHED, APPOINT_DEVELOPER, DEV_APPOINT_CONFIRMING);

		// 【开发指认中】 不支持撤回操作
		accept(DEV_APPOINT_CONFIRMING, CANCEL_APPOINTMENT, PUBLISHED);// 0个指认时才会转换状态
		accept(DEV_APPOINT_CONFIRMING, MODIFY, DEV_APPOINT_CONFIRMING);
		accept(DEV_APPOINT_CONFIRMING, APPOINT_DEVELOPER, DEV_APPOINT_CONFIRMING);// 因为支持同时指认多个

		// 【开发中】不支持撤回操作、指认开发者、取消指认
		accept(DEVELOPING, MODIFY, DEVELOPING);
		accept(DEVELOPING, DISMISS_DEVELOPER, PUBLISHED);

		// 【交付待确认中】 不支持修改、撤回、指认开发者、取消指认、解雇开发者操作
		accept(DELIVERY_CONFIRMING, CONFIRM_DELIVERY, COMMENT_AWAITING);
		accept(DELIVERY_CONFIRMING, DENY_DELIVERY, DEVELOPING);

		// 【待评价】
		accept(COMMENT_AWAITING, SUBMIT_COMMENT, COMMENT_FINISHED);
	}

}
