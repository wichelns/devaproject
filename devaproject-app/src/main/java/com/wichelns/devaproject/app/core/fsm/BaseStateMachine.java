package com.wichelns.devaproject.app.core.fsm;

/**
 * 状态机接口
 *
 * @author zhijia
 * @date 2024/2/17 15:15
 */
public interface BaseStateMachine<S extends BaseState, E extends BaseEvent> {

	/**
	 * @param sourceState 源状态
	 * @param event        事件
	 * @param targetState 目标状态
	 */
	void accept(S sourceState, E event, S targetState);

	/**
	 * @param sourceState 源状态
	 * @param event 事件
	 * @return 目标状态
	 */
	S getTargetState(S sourceState, E event);

}
