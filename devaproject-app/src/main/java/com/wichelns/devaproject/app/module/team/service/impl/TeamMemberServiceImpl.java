package com.wichelns.devaproject.app.module.team.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.dao.team.repo.TeamMemberRepo;
import com.wichelns.devaproject.app.module.team.dto.MemberDTO;
import com.wichelns.devaproject.app.module.team.entity.TeamMember;
import com.wichelns.devaproject.app.module.team.logic.TeamLogic;
import com.wichelns.devaproject.app.module.team.logic.TeamMemberLogic;
import com.wichelns.devaproject.app.module.team.service.TeamMemberService;
import com.wichelns.devaproject.app.mq.dto.MessageDTO;
import com.wichelns.devaproject.app.mq.producer.Producer;
import com.wichelns.devaproject.common.exception.BusinessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static com.wichelns.devaproject.app.constant.MQConstant.KEY_MEMBER_OUT;
import static com.wichelns.devaproject.common.enums.ErrorCodeEnum.*;

/**
 * @author zhijia
 * @date 2024/3/2 21:04
 */
@Service
public class TeamMemberServiceImpl implements TeamMemberService {
	@Resource
	private TeamMemberLogic teamMemberLogic;
	@Resource
	private TeamMemberRepo teamMemberRepo;
	@Resource
	private Producer producer;


	@Transactional
	@Override
	public void kick(long memberId) {
		Long loginUserId = Jwt.getLoginUserId();
		TeamMember member = teamMemberRepo.getById(memberId);
		// 权限校验、目标非自己
		if (TeamLogic.isLeader(member.getTeamId(), loginUserId)) {
			if (loginUserId.equals(member.getDevUserId())) {
				throw new BusinessException(TC10050004);
			}
			// 团队状态校验（未解散）
			if (TeamLogic.teamExists(member.getTeamId())) {
				// 移除队员
				// 团队当前人数-1
				teamMemberLogic.removeMember(member.getTeamId(), memberId);
				// mq异步处理teamProject的extra信息
				MemberDTO memberDTO = BeanUtil.copyProperties(member, MemberDTO.class);
				MessageDTO messageDTO = new MessageDTO(KEY_MEMBER_OUT, memberDTO);
				producer.produce(messageDTO);
				return;
			}
			throw new BusinessException(GL9999404, member.getTeamId() + "已解散或不存在");
		}
		throw new BusinessException(GL99990401, "非队长，权限不足");
	}

	@Transactional
	@Override
	public void quitTeam(int teamId) {
		Long loginUserId = Jwt.getLoginUserId();
		// 身份校验（队长不能退出）
		if (TeamLogic.isLeader(teamId, loginUserId)) {
			throw new BusinessException(TC10050005);
		}
		// 团队状态校验（未解散）
		if (TeamLogic.teamExists(teamId)) {
			// 移除队员
			// 团队当前人数-1
			teamMemberLogic.removeMember(teamId, loginUserId);
			// mq异步处理teamProject的extra信息
			MemberDTO memberDTO = BeanUtil.copyProperties(teamMemberRepo.getById(loginUserId), MemberDTO.class);
			MessageDTO messageDTO = new MessageDTO(KEY_MEMBER_OUT, memberDTO);
			producer.produce(messageDTO);
			return;
		}
		throw new BusinessException(GL9999404, teamId + "已解散或不存在");
	}

	@Override
	public void remark(long memberId, String remark) {
		checkRemark(remark);
		Long loginUserId = Jwt.getLoginUserId();
		if (ObjectUtil.notEqual(loginUserId, memberId)) {// 仅允许修改自己的
			throw new BusinessException(GL99990401);
		}
		teamMemberRepo.lambdaUpdate().set(TeamMember::getRemark, remark)
				.eq(TeamMember::getMemberId, memberId).update();

	}

	@Override
	public void attachRespTag(long memberId, String respTagId) {
		TeamMember member = teamMemberRepo.getById(memberId);
		// 权限校验
		if (TeamLogic.isLeader(member.getTeamId(), Jwt.getLoginUserId())) {
			// 团队状态校验
			if (TeamLogic.teamExists(member.getTeamId())) {
				teamMemberRepo.lambdaUpdate().set(TeamMember::getRespTagId, respTagId)
						.eq(TeamMember::getMemberId, memberId).update();
				return;
			}
			throw new BusinessException(GL9999404, member.getTeamId() + "已解散或不存在");
		}
		throw new BusinessException(GL99990401, "非队长，权限不足");
	}

	private void checkRemark(String remark) {
		// todo remark审核
	}

}
