package com.wichelns.devaproject.app.dao.team.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wichelns.devaproject.app.module.team.entity.RespTagLib;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhijia
 * @date 2024/3/3 14:20
 */
@Mapper
public interface RespTagLibMapper extends BaseMapper<RespTagLib> {
}
