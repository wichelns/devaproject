package com.wichelns.devaproject.app.module.article.service.impl;

import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.dao.article.repo.ProjectRequirementRepo;
import com.wichelns.devaproject.app.module.article.convert.ProjectReqConvert;
import com.wichelns.devaproject.app.module.article.entity.ProjectRequirement;
import com.wichelns.devaproject.app.module.article.logic.ProjectReqLogic;
import com.wichelns.devaproject.app.module.article.param.ProjectReqParam;
import com.wichelns.devaproject.app.module.article.service.ProjectReqEsService;
import com.wichelns.devaproject.app.module.article.service.ProjectReqService;
import com.wichelns.devaproject.app.module.article.logic.TagLogic;
import com.wichelns.devaproject.common.enums.BaseStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

import static com.wichelns.devaproject.app.module.article.enums.ProjectReqState.*;

/**
 * @author 淡漠
 * @version 1.0
 */
@Service
public class ProjectReqServiceImpl implements ProjectReqService {
	@Resource
	private ProjectRequirementRepo projectRequirementRepo;
	@Resource
	private ProjectReqLogic projectReqLogic;
	@Resource
	private ProjectReqEsService projectReqEsService;
	@Resource
	private TagLogic tagLogic;

	// 新建草稿或者新建草稿直接发布或者更新草稿,发布状态，开发指认待确认、开发中
	@Transactional
	public void saveOrUpdate(ProjectReqParam projectReqParam) {
		Long loginUserId = Jwt.getLoginUserId();
		ProjectRequirement projectRequirement = ProjectReqConvert.INSTANCE.dto2ProjectReq(projectReqParam);

		if (projectRequirement.getProjectId() == null) {
			// 新增
			projectRequirement.setReqUserId(loginUserId);
			boolean published = BaseStatus.Y.getCode().equals(projectReqParam.getDirPublish());
			if (published) {
				projectRequirement.setStatus(PUBLISHED.getCode());
				// 发布校验
				projectReqLogic.checkCanPublish(projectRequirement);
			}
			boolean save = projectRequirementRepo.save(projectRequirement);
			if (published && save){
				tagLogic.increaseHeat(projectRequirement.getTagList());
				projectReqEsService.insertOrUpdateEsById(projectRequirement.getProjectId());
			}
		} else {
			// 更新
			// 权限与状态校验 todo 一致性锁定读
			projectReqLogic.projectCheck(loginUserId, projectRequirement.getProjectId(), DRAFT.getCode(), PUBLISHED.getCode()
					, DEVELOPING.getCode(), DEV_APPOINT_CONFIRMING.getCode());

			// 已发布下的校验
			List<Integer> statusList = Arrays.asList(PUBLISHED.getCode(), DEVELOPING.getCode(), DEV_APPOINT_CONFIRMING.getCode());
			Integer count = projectRequirementRepo.lambdaQuery().eq(ProjectRequirement::getProjectId, projectRequirement.getProjectId())
					.in(ProjectRequirement::getStatus, statusList).count();
			if (count == 1) {
				projectReqLogic.checkCanPublish(projectRequirement);
			}

			boolean update = projectRequirementRepo.updateById(projectRequirement);
			if (count == 1 && update) {
				tagLogic.increaseHeat(projectReqParam.getTagList());
				projectReqEsService.insertOrUpdateEsById(projectRequirement.getProjectId());
			}
		}
	}


	//草稿的直接发布与发布项目的直接撤回
	@Transactional
	public void togglePublish(Integer projectId) {
		// 权限及状态校验 todo 一致性锁定读
		projectReqLogic.projectCheck(Jwt.getLoginUserId(), projectId, PUBLISHED, DRAFT);

		ProjectRequirement projectReq = projectRequirementRepo.getById(projectId);
		if (DRAFT.getCode().equals(projectReq.getStatus())) {
			// 发布校验
			projectReqLogic.checkCanPublish(projectReq);
			boolean update = projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, PUBLISHED.getCode())
					.eq(ProjectRequirement::getProjectId, projectId).update();
			if (update) {
				tagLogic.increaseHeat(projectReq.getTagList());
				projectReqEsService.insertOrUpdateEsById(projectReq.getProjectId());
			}
		} else {
			// 撤回
			boolean update = projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, DRAFT.getCode())
					.eq(ProjectRequirement::getProjectId, projectId).update();
			if (update) {
				projectReqEsService.deleteById(projectReq);
			}
		}
	}

}
