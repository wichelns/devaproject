package com.wichelns.devaproject.app.module.login.vo;

import com.wichelns.devaproject.app.module.user.vo.UserVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "登陆VO")
@AllArgsConstructor
public class LoginVO {
    @ApiModelProperty(value = "token")
    private String token;
    @ApiModelProperty(value = "用户信息")
    private UserVO userVO;
}