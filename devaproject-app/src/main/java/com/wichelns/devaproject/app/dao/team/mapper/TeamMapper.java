package com.wichelns.devaproject.app.dao.team.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wichelns.devaproject.app.module.team.entity.Team;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author zhijia
 * @date 2024/2/21 20:29
 */
@Mapper
public interface TeamMapper extends BaseMapper<Team> {
	int incRecvCountById(@Param("teamId") int teamId);
	int incDeliverCountById(@Param("teamId") int teamId);
	int incStarsById(@Param("teamId") int teamId, @Param("increment") int increment);
	int incCurMemById(@Param("teamId") int teamId, @Param("increment") int increment);
}
