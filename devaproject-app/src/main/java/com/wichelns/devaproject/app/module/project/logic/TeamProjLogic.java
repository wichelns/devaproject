package com.wichelns.devaproject.app.module.project.logic;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wichelns.devaproject.app.dao.team.repo.TeamRepo;
import com.wichelns.devaproject.app.module.project.convert.DeveloperProjConvert;
import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.module.project.entity.TeamProject;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;

import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum.DISMISSED;
import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum.QUITED;

@Component
public class TeamProjLogic extends AbstractDevProjLogic {
	@Resource
	private TeamRepo teamRepo;

	@Override
	public void checkCanJoin(Integer projectId, Long devUserId, Integer teamId) {
		// 1
		LambdaQueryWrapper<TeamProject> qwForTP = Wrappers.<TeamProject>lambdaQuery()
				.eq(TeamProject::getTeamId, teamId)
				.eq(TeamProject::getProjectId, projectId)
				.notIn(TeamProject::getStatus, Arrays.asList(QUITED.getCode(), DISMISSED.getCode()));
		if (teamProjectRepo.count(qwForTP) != 0) {
			throw new BusinessException(ErrorCodeEnum.PC10040000);
		}
		// 2
		Integer count = teamRepo.lambdaQuery().eq(Team::getTeamId, teamId)
				.eq(Team::getLeaderId, devUserId).count();
		if (count != 1) {
			throw new BusinessException(ErrorCodeEnum.PC10040001);
		}
		// 3 todo
	}

	@Override
	public void checkCanManageDevProj(Long devUserId, Long devProjId) {
		Integer count = teamProjectRepo.lambdaQuery()
				.eq(TeamProject::getTeamProjId, devProjId)
				.eq(TeamProject::getLeaderId, devUserId).count();
		if (count != 1) {
			throw new BusinessException(ErrorCodeEnum.PC10040001);
		}
	}

	@Override
	DeveloperProjDTO getDTOById4Update(Long devProjId) {
		TeamProject teamProject = teamProjectRepo.getBaseMapper().lockForUpdateById(devProjId);
		return DeveloperProjConvert.INSTANCE.domain2DTO(teamProject);
	}

	@Override
	void changeDevProjStatus(Long devProjId, DeveloperProjStatusEnum targetStatus) {
		teamProjectRepo.lambdaUpdate().set(TeamProject::getStatus, targetStatus.getCode())
				.eq(TeamProject::getTeamProjId, devProjId).update();
	}

	@Override
	void evaluateDevProj(Long devProjId, String comment, Integer stars) {
		teamProjectRepo.lambdaUpdate().set(TeamProject::getComment, comment)
				.set(TeamProject::getStars, stars)
				.eq(TeamProject::getTeamProjId, devProjId).update();
	}
}