package com.wichelns.devaproject.app.job;

import com.wichelns.devaproject.app.module.article.logic.TagLogic;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

import static com.wichelns.devaproject.common.constant.GlobalConstant.TAG_HEAT;

@Component
public class TagUpdateScheduler {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private TagLogic tagLogic;

    // 每天晚上12点执行一次
    @Scheduled(cron = "0 0 0 * * ?")
    public void updateTopTags() {

        Map<String, Integer> tagHeat = tagLogic.getTagHeatForWeek();

        // 将前十个标签及热度值存入 Redis 的 ZSET 中
        for (Map.Entry<String, Integer> entry : tagHeat.entrySet()) {
            String tag = entry.getKey();
            Integer heat = entry.getValue();
            redisTemplate.opsForZSet().add(TAG_HEAT, tag, heat);
        }

    }
}