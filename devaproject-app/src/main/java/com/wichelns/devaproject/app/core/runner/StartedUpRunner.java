package com.wichelns.devaproject.app.core.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Order
@Slf4j
@Component
public class StartedUpRunner implements ApplicationRunner {

    @Resource
    private ConfigurableApplicationContext context;

    @Override
    public void run(ApplicationArguments args) {
        if (context.isActive()) {
            log.info("系统启动SUCCESS");
            Map<String, RedisMessageListenerContainer> beansOfType = context.getBeansOfType(RedisMessageListenerContainer.class);
        }
    }

}
