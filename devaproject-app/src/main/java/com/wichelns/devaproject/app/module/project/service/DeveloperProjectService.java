package com.wichelns.devaproject.app.module.project.service;

import com.wichelns.devaproject.app.module.project.param.DeveloperProjParam;

/**
 * @author zhijia
 * @date 2024/2/21 0:02
 */
public interface DeveloperProjectService {
	/**
	 * 参选项目
	 */
	void joinProject(DeveloperProjParam param);

	/**
	 * 修改参选介绍
	 */
	void editJoinIntro(DeveloperProjParam param);

	/**
	 * 答应开发指认
	 */
	void confirmDevAppointment(DeveloperProjParam param);

	/**
	 * 项目交付申请
	 */
	void deliver(DeveloperProjParam param);

	/**
	 * 放弃项目开发
	 */
	void quitProject(Long devProjId);
}
