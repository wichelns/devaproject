package com.wichelns.devaproject.app.dao.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wichelns.devaproject.app.module.article.entity.ProjectRequirement;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhijia
 * @date 2024/2/22 17:20
 */
@Mapper
public interface ProjectRequirementMapper extends BaseMapper<ProjectRequirement> {
}
