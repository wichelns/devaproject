package com.wichelns.devaproject.app.config;

import com.wichelns.devaproject.app.core.properties.DevaprojectProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.Resource;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 描述：
 *
 * @author Yoo
 * @date 2020/9/26
 * @since 1.0
 */
@Slf4j
@Configuration
@EnableAsync
@EnableScheduling
public class AsyncTaskExecutorConfiguration implements AsyncConfigurer {

    @Resource
    private DevaprojectProperties devaprojectProperties;
    // todo 线程池调优

    @Override
    @Bean(name = "taskExecutor")
    public Executor getAsyncExecutor() {
        log.debug("Creating Async Task Executor");
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //核心线程池大小 建议设置为机器CPU核心数
        executor.setCorePoolSize(devaprojectProperties.getTask().getCorePoolSize());
        //线程池维护线程最大数量
        executor.setMaxPoolSize(devaprojectProperties.getTask().getMaxPoolSize());
        //闲置线程最大空闲时间
        executor.setKeepAliveSeconds(devaprojectProperties.getTask().getKeepAliveSeconds());
        //缓冲队列大小，默认LinkedBlockingQueue
        executor.setQueueCapacity(devaprojectProperties.getTask().getQueueCapacity());
        //线程前缀
        executor.setThreadNamePrefix(devaprojectProperties.getTask().getThreadNamePrefix());
        //TODO 拒绝策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());
        // 等待任务执行完
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setAwaitTerminationSeconds(30);
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new SimpleAsyncUncaughtExceptionHandler();
    }
}
