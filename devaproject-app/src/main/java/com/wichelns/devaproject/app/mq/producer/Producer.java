package com.wichelns.devaproject.app.mq.producer;

import com.wichelns.devaproject.app.mq.dto.MessageDTO;

/**
 * 生产者统一接口
 *
 * @author zhijia
 * @date 2024/2/11 15:38
 */
public interface Producer {

	/**
	 * 生产消息 todo traceId链路追踪
	 */
	void produce(MessageDTO messageDto);
}
