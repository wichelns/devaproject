package com.wichelns.devaproject.app.module.article.enums;

import lombok.Getter;

/***
 * @author xph
 */
@Getter
public enum SortByEnum {
//    reward,viewCount,createdTime
    REWARD_ACS(1,"报酬升序"),
    REWARD_DESC(2,"报酬报酬降序"),
    VIEW_COUNT_ACS(3,"浏览量升序"),
    VIEW_COUNT_DESC(4,"浏览量降序"),
    CREATED_TIME_ACS(5,"创建时间升序"),
    CREATED_TIME_DESC(6,"创建时间降序")
    ;

    private final Integer code;
    private final String desc;


    SortByEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
