package com.wichelns.devaproject.app.module.project.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhijia
 * @date 2024/2/15 21:37
 */
@Getter
@AllArgsConstructor
public enum DeveloperProjTypeEnum {
	PERSONAL_PROJECT(10, "个人项目"),
	TEAM_PROJECT(20, "团队项目");

	private final Integer code;
	private final String desc;
}
