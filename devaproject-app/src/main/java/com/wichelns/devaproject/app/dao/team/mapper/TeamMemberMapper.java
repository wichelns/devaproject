package com.wichelns.devaproject.app.dao.team.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.app.module.team.entity.TeamMember;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhijia
 * @date 2024/2/21 20:29
 */
@Mapper
public interface TeamMemberMapper extends BaseMapper<TeamMember> {
}
