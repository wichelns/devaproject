package com.wichelns.devaproject.app.core.authentication;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 描述：
 *
 * @author Yoo
 * @date 2020/9/29
 * @since 1.0
 */
@Configuration
public class ShiroConfig {

	/**
	 * Jwt过滤器配置
	 */
	@Bean
	public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
		// 配置securityManager
		shiroFilterFactoryBean.setSecurityManager(securityManager);
		// 配置免认证url
		Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
		// 在 Shiro过滤器链上加入 JWTFilter
		LinkedHashMap<String, Filter> filters = new LinkedHashMap<>();
		filters.put("jwt", new JwtFilter());
		shiroFilterFactoryBean.setFilters(filters);
		// 所有请求都要经过 jwt过滤器
		// if (!devaprojectProperties.getShiro().getClose()) {
		// }
		filterChainDefinitionMap.put("/api/**", "jwt");
		shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
		return shiroFilterFactoryBean;
	}

	/**
	 * 相当于切点 为了支持@RequiresPermissions和@RequiresRoles注解
	 */
	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
		authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
		return authorizationAttributeSourceAdvisor;
	}

	@Bean
	public SecurityManager securityManager(ShiroRealm shiroRealm) {
		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
		// 注入shiroRealm
		securityManager.setRealm(shiroRealm);
		return securityManager;
	}
}
