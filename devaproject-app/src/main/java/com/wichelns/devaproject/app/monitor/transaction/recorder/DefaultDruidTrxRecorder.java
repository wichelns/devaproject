package com.wichelns.devaproject.app.monitor.transaction.recorder;

import com.alibaba.druid.filter.FilterChain;
import com.alibaba.druid.proxy.jdbc.ConnectionProxy;
import com.alibaba.druid.support.profile.ProfileEntryKey;
import com.alibaba.druid.support.profile.Profiler;
import com.wichelns.devaproject.app.monitor.transaction.dto.TransactionDTO;
import com.wichelns.devaproject.app.monitor.transaction.job.LongTrxManageJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 记录事务相关状态
 *
 * @author zhijia
 * @date 2023/9/8 9:37
 */
@Slf4j
@Component
public class DefaultDruidTrxRecorder extends AbstractDruidTrxRecorder {

    @Override
    public void afterTrxBegin(FilterChain chain, ConnectionProxy connection) throws SQLException {
        // 记录connectionId、开始时间、初始调用链；将该事务信息交予 job 管理判断长事务与告警
        String stackTrace = getStackTraceStr();
        String connectionId = getConnectionId(connection);
        long beginTimeMillis = System.currentTimeMillis();
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setConnectionId(connectionId);
        transactionDTO.setTrxBeginTimeMillis(beginTimeMillis);
        transactionDTO.setStackTraceStr(stackTrace);
        transactionDTO.setSqlList(Collections.emptyList());
        LongTrxManageJob.trxMap.put(connectionId, transactionDTO);
    }
    
    @Override
    public void afterTrxCommit(FilterChain chain, ConnectionProxy connection) throws SQLException {
        doRecordTrxEnd(chain, connection);
    }
    
    @Override
    public void afterTrxRollback(FilterChain chain, ConnectionProxy connection) throws SQLException {
        doRecordTrxEnd(chain, connection);
    }


    @Override
    public void afterTrxRollback(FilterChain chain, ConnectionProxy connection, Savepoint savepoint) {
        // todo 回滚某个保存点如何处理？
    }

    private void doRecordTrxEnd(FilterChain chain, ConnectionProxy connection) throws SQLException {
        String connectionId = getConnectionId(connection);
        String stackTraceStr = getStackTraceStr();
        List<String> sqlList = Profiler.getStatsMap() != null ? Profiler.getStatsMap().keySet().stream()
                .filter(key -> Profiler.PROFILE_TYPE_SQL.equals(key.getType()))
                .map(ProfileEntryKey::getName).collect(Collectors.toList()) : Collections.emptyList();

        LongTrxManageJob.trxMap.computeIfPresent(connectionId, (s, transactionDTO) -> {
            transactionDTO.setTrxEndTimeMillis(System.currentTimeMillis());
            transactionDTO.setStackTraceStr(stackTraceStr);
            transactionDTO.setSqlList(sqlList);
            return transactionDTO;
        });
    }
}
