package com.wichelns.devaproject.app.module.login.api;

import com.wichelns.devaproject.app.module.login.param.LoginParam;
import com.wichelns.devaproject.app.module.login.service.LoginService;
import com.wichelns.devaproject.app.module.login.vo.LoginVO;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/16 0:24
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/auth")
@Api(value = "登录API", tags = "Web - LoginApi")
public class LoginApi {

	@Resource
	private LoginService loginService;

	@PostMapping(value = "/login")
	@ApiOperation(httpMethod = "POST", value = "登录")
	public Wrapper<LoginVO> login(@RequestBody @Validated LoginParam loginParam) {
		return WrapMapper.ok(loginService.loginOrSignUp(loginParam));
	}

	@PostMapping(value = "/logout")
	@ApiOperation(httpMethod = "POST", value = "退出登录")
	public Wrapper<?> logout() {
		String token = (String) SecurityUtils.getSubject().getPrincipal();
		loginService.logout(token);
		return WrapMapper.ok();
	}

}
