package com.wichelns.devaproject.app.module.message.request.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhijia
 * @date 2024/2/28 11:56
 */
@Getter
@AllArgsConstructor
public enum RequestStatusEnum {
	TO_READ(10, "未读"),
	TO_OPERATE(20, "已读未操作"),
	CONFIRMED(30, "已确认"),
	REJECTED(40, "已拒绝"),
	;

	private final Integer code;
	private final String desc;
}
