package com.wichelns.devaproject.app.module.message.announce.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wichelns.devaproject.app.module.message.announce.vo.AnnounceVO;

/**
 * @author zhijia
 * @date 2024/3/4 10:35
 */
public interface AnnounceService {
	/**
	 * 拉取通知
	 */
	IPage<AnnounceVO> pullAnnounce(int pageNo, int pageSize);

	/**
	 * 拉取公告板
	 */
	IPage<AnnounceVO> pullPublic(int pageNo, int pageSize);

	/**
	 * 已读一条
	 */
	void readOne(int announceId);

	/**
	 * 一键已读
	 */
	void readAll();
}
