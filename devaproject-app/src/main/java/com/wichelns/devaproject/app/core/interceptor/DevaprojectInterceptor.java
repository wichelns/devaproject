package com.wichelns.devaproject.app.core.interceptor;


import cn.hutool.core.util.IdUtil;
import com.wichelns.devaproject.common.toolkit.IpUtil;
import com.wichelns.devaproject.common.toolkit.ThreadLocalMap;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.wichelns.devaproject.common.constant.GlobalConstant.IP_KEY;


/**
 * 全局拦截
 */
@Slf4j
@Component
public class DevaprojectInterceptor implements HandlerInterceptor {

    /**
     * 调用时间：Controller方法处理之前
     * 执行顺序：链式Interceptor情况下，Interceptor按照声明的顺序一个接一个执行
     * 若返回false，则中断执行，注意：不会进入afterCompletion
     *
     * @param request  the request
     * @param response the response
     * @param handler  the handler
     * @return the boolean
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        //日志链路
        MDC.put("trace_uuid", IdUtil.fastSimpleUUID());
        //设置ip
        String ipAddr = IpUtil.getRemoteAddr(request);
        ThreadLocalMap.put(IP_KEY, ipAddr);
        return true;
    }


    /**
     * 调用前提：preHandle返回true
     * 调用时间：Controller方法处理完之后，DispatcherServlet进行视图的渲染之前，也就是说在这个方法中你可以对ModelAndView进行操作
     *
     * @param request  the request
     * @param response the response
     * @param arg2     the arg 2
     * @param mv       the mv
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2, ModelAndView mv) {
    }

    /**
     * 调用前提：preHandle返回true
     * 调用时间：DispatcherServlet进行视图的渲染之后
     *
     * @param request  the request
     * @param response the response
     * @param arg2     the arg 2
     * @param ex       the ex
     * @throws Exception the exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object arg2, Exception ex) throws Exception {
        ThreadLocalMap.remove();
    }
}
