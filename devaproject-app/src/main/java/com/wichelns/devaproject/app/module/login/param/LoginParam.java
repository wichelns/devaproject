package com.wichelns.devaproject.app.module.login.param;

import com.wichelns.devaproject.common.model.BaseParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "登陆注册Param")
@EqualsAndHashCode(callSuper = false)
public class LoginParam extends BaseParam {
    @NotNull(message = "手机号不能为空")
    @ApiModelProperty(value = "手机号不能为空", required = true)
    private String phone;
    @NotNull(message = "用户身份不能为空")
    @ApiModelProperty(value = "用户身份", required = true, notes = "10-需求用户 20-开发者用户")
    private Integer userType;
    @NotNull(message = "密码不能为空")
    @ApiModelProperty(value = "用户密码", required = true, notes = "前端传送前需要加密")
    private String password;
    @ApiModelProperty(value = "联系方式")
    private String contact;

    /**
     * 参数校验分组：登陆
     */
    public @interface login {
    }

    /**
     * 参数校验分组：注册
     */
    public @interface regist {
    }
}

