package com.wichelns.devaproject.app.module.user.convert;

import com.wichelns.devaproject.app.module.user.dto.UserDTO;
import com.wichelns.devaproject.app.module.user.entity.DeveloperUser;
import com.wichelns.devaproject.app.module.user.entity.RequirementUser;
import com.wichelns.devaproject.app.module.user.enums.UserTypeEnum;
import com.wichelns.devaproject.app.module.user.vo.UserVO;
import com.wichelns.devaproject.common.model.BaseConvert;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @author zhijia
 * @date 2024/2/16 12:38
 */
@Mapper(imports = UserTypeEnum.class)
public interface UserConvert extends BaseConvert {
	UserConvert INSTANCE = Mappers.getMapper(UserConvert.class);

	@Mapping(source = "reqUserId", target = "userId")
	@Mapping(target = "userType", expression = "java(UserTypeEnum.REQUIREMENT_USER.getCode())")
	UserDTO domain2DTO(RequirementUser reqUser);
	@Mapping(source = "devUserId", target = "userId")
	@Mapping(target = "userType", expression = "java(UserTypeEnum.DEVELOPER_USER.getCode())")
	UserDTO domain2DTO(DeveloperUser devUser);

	UserVO dto2VO(UserDTO userDTO);

	@Mapping(source = "userId", target = "reqUserId")
	RequirementUser dto2ReqUser(UserDTO userDTO);
	@Mapping(source = "userId", target = "devUserId")
	DeveloperUser dto2DevUser(UserDTO userDTO);
}
