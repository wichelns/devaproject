package com.wichelns.devaproject.app.core.file.api;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.core.authentication.JwtPayloadConstant;
import com.wichelns.devaproject.app.core.context.SystemContext;
import com.wichelns.devaproject.app.core.file.common.dto.PreUploadDTO;
import com.wichelns.devaproject.app.core.file.param.FileUploadParam;
import com.wichelns.devaproject.common.annotation.BusinessLog;
import com.wichelns.devaproject.common.exception.BusinessException;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.wichelns.devaproject.common.enums.ErrorCodeEnum.GL_SYS_ERROR;


/**
 * 描述：
 *
 * @author Yoo
 * @date 2020/9/26
 * @since 1.0
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/file")
@Api(value = "文件相关", tags = "Web - FileApi")
public class FileApi {

    @PostMapping(value = "/preUrl")
    @ApiOperation(httpMethod = "POST", value = "生成预上传URL")
    @BusinessLog(title = "生成预上传URL")
    public Wrapper<PreUploadDTO> genPreUrl(@RequestBody @Validated(FileUploadParam.preUrl.class) FileUploadParam param) {
        String token = (String) SecurityUtils.getSubject().getPrincipal();
        Long userId = Jwt.parseToken(token).get(JwtPayloadConstant.CLAIM_USERID).asLong();

        String filename = SystemContext.holder().opsForSystem().getValue("file.rootDir", "") + "/" + userId + "/" + param.getFileName();
        filename = StrUtil.removeAll(filename, ',', ' ');//去除逗号和空格避免分割符冲突
        filename += RandomUtil.randomString(10);
        try {
            return WrapMapper.ok(SystemContext.holder().getFileOperator().getPreUrl(filename, 100));
        } catch (Exception e) {
            log.error("file upload error", e);
        }
        throw new BusinessException(GL_SYS_ERROR);
    }
}