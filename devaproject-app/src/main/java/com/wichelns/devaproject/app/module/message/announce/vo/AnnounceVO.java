package com.wichelns.devaproject.app.module.message.announce.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author zhijia
 * @date 2024/3/4 10:37
 */
@Data
public class AnnounceVO {
	@ApiModelProperty(value = "通知ID")
	private Integer announceId;
	@ApiModelProperty(value = "标题")
	private String title;
	@ApiModelProperty(value = "内容")
	private String content;
	@ApiModelProperty(value = "跳转链接")
	private String refLink;
	@ApiModelProperty(value = "通知类型")
	private Integer announceType;
	@ApiModelProperty(value = "接收者ID")
	private Long receiverId;
	@ApiModelProperty(value = "接收者类型")
	private Integer receiverType;
	@ApiModelProperty(value = "状态 1-已读 0-未读", notes = "只有目标是个人的时候才需要这个")
	private Integer status;

	private Date createdTime;
	private Date updatedTime;
}
