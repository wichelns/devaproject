package com.wichelns.devaproject.app.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ParameterType;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableOpenApi
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
@Profile("dev")
public class SwaggerConfig {


    @Bean
    public Docket swaggerApi() {
        RequestParameter parameter = new RequestParameterBuilder()
                .name("Authorization")
                .in(ParameterType.HEADER)
                .required(false)
                .build();

        List<RequestParameter> globalRequestParameters = new ArrayList<>();
        globalRequestParameters.add(parameter);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.wichelns.devaproject.app.module"))
                .paths(PathSelectors.any())
                .build().globalRequestParameters(globalRequestParameters);
    }

    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("利用swagger2构建的API文档")
                .description("用restful风格写接口")
                .termsOfServiceUrl("")
                .version("1.0")
                .build();
    }

}