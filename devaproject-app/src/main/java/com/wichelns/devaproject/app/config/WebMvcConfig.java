package com.wichelns.devaproject.app.config;

import com.wichelns.devaproject.app.core.interceptor.DevaprojectInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;


@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Resource
    private DevaprojectInterceptor devaprojectInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(devaprojectInterceptor).addPathPatterns("/**");
    }
}
