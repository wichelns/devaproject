package com.wichelns.devaproject.app.module.user.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 信誉墙项：接取项目数、交付项目数、得星数、得星率
 *
 * @author zhijia
 * @date 2024/2/20 23:00
 */
@Getter
@AllArgsConstructor
public enum CreditItemEnum {
	RECEIVE_COUNT(1, "接取项目数"),
	DELIVER_COUNT(5, "交付项目数"),
	STARS_COUNT(10, "得星数"),
	STARS_PERCENTAGE(15, "得星率"),
	;

	private final Integer code;
	private final String desc;
}
