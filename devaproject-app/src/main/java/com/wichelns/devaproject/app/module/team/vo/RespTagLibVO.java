package com.wichelns.devaproject.app.module.team.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author zhijia
 * @date 2024/3/3 15:03
 */
@ApiModel(value = "职责标签VO")
@Data
public class RespTagLibVO {
	@ApiModelProperty(value = "ID")
	private Integer id;
	@ApiModelProperty(value = "开发者用户ID")
	private Long devUserId;
	@ApiModelProperty(value = "组ID")
	private Integer groupId;
	@ApiModelProperty(value = "组名")
	private String groupName;
	@ApiModelProperty(value = "职责标签列表")
	private List<RespTagVO> respTagList;

	@Data
	public static class RespTagVO {
		private Integer tagId;
		private String tagName;
	}

}
