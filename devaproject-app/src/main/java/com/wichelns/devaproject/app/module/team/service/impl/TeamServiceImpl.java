package com.wichelns.devaproject.app.module.team.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.dao.project.repo.TeamProjectRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamMemberRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamRepo;
import com.wichelns.devaproject.app.dao.user.repo.DeveloperUserRepo;
import com.wichelns.devaproject.app.module.project.entity.TeamProject;
import com.wichelns.devaproject.app.module.project.logic.TeamProjLogic;
import com.wichelns.devaproject.app.module.team.dto.TeamDTO;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.app.module.team.entity.TeamMember;
import com.wichelns.devaproject.app.module.team.enums.MemberIdentityEnum;
import com.wichelns.devaproject.app.module.team.logic.TeamLogic;
import com.wichelns.devaproject.app.module.team.param.TeamParam;
import com.wichelns.devaproject.app.module.team.service.TeamService;
import com.wichelns.devaproject.app.module.user.entity.DeveloperUser;
import com.wichelns.devaproject.app.mq.dto.MessageDTO;
import com.wichelns.devaproject.app.mq.producer.Producer;
import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

import static com.wichelns.devaproject.app.constant.MQConstant.KEY_LEADER_CHANGE;
import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum.*;
import static com.wichelns.devaproject.common.enums.ErrorCodeEnum.GL99990401;
import static com.wichelns.devaproject.common.enums.ErrorCodeEnum.TC10050003;

/**
 * @author zhijia
 * @date 2024/3/1 15:35
 */
@Slf4j
@Service
public class TeamServiceImpl implements TeamService {
	@Resource
	private TeamRepo teamRepo;
	@Resource
	private DeveloperUserRepo developerUserRepo;
	@Resource
	private TeamMemberRepo teamMemberRepo;
	@Resource
	private TeamProjectRepo teamProjectRepo;
	@Resource
	private TeamProjLogic teamProjLogic;
	@Resource
	private Producer producer;


	@Transactional
	@Override
	public void addOrEdit(TeamParam param) {
		Long loginUserId = Jwt.getLoginUserId();

		Team team = new Team();
		team.setTeamName(param.getTeamName());
		team.setAvatar(param.getAvatar());
		team.setProfile(param.getProfile());
		team.setMaxMemberCount(param.getMaxMemberCount());

		Integer teamId = param.getTeamId();
		if (teamId == null) {
			// 新增
			DeveloperUser devUser = developerUserRepo.getById(loginUserId);
			team.setLeaderId(loginUserId);
			team.setLeaderName(devUser.getUsername());
			teamRepo.save(team);

			TeamMember member = new TeamMember();
			member.setDevUserId(loginUserId);
			member.setTeamId(team.getTeamId());// 获取数据库生成主键值
			member.setIdentity(MemberIdentityEnum.LEADER.getCode());
			teamMemberRepo.save(member);
		} else {
			// 编辑
			team.setTeamId(teamId);
			// 权限校验
			if (!TeamLogic.isLeader(teamId, loginUserId)) {
				throw new BusinessException(GL99990401, "非队长，权限不足");
			}
			teamRepo.updateById(team);
		}
	}

	@Transactional
	@Override
	public void changeLeader(int teamId, long targetMemId) {
		// 权限校验
		Long loginUserId = Jwt.getLoginUserId();
		if (!TeamLogic.isLeader(teamId, loginUserId)) {
			throw new BusinessException(GL99990401, "非队长，权限不足");
		}
		TeamMember member = teamMemberRepo.getById(targetMemId);
		DeveloperUser devUser = developerUserRepo.getById(member.getDevUserId());
		teamRepo.lambdaUpdate().set(Team::getLeaderId, member.getDevUserId())
				.set(Team::getLeaderName, devUser.getUsername())
				.eq(Team::getTeamId, teamId).update();
		// 身份互换
		teamMemberRepo.lambdaUpdate().set(TeamMember::getIdentity, MemberIdentityEnum.COMMON_MEMBER.getCode())
				.eq(TeamMember::getTeamId, teamId)
				.eq(TeamMember::getDevUserId, loginUserId).update();
		teamMemberRepo.lambdaUpdate().set(TeamMember::getIdentity, MemberIdentityEnum.LEADER.getCode())
				.eq(TeamMember::getMemberId, targetMemId).update();
		// 发送mq
		TeamDTO teamDTO = new TeamDTO();
		teamDTO.setTeamId(teamId);
		teamDTO.setFromLeaderId(loginUserId);
		teamDTO.setToLeaderId(member.getDevUserId());
		MessageDTO messageDto = new MessageDTO(KEY_LEADER_CHANGE, teamDTO);
		producer.produce(messageDto);
	}

	@Override
	public void breakUp(int teamId) {
		// 权限校验
		Long loginUserId = Jwt.getLoginUserId();
		if (!TeamLogic.isLeader(teamId, loginUserId)) {
			throw new BusinessException(GL99990401, "非队长，权限不足");
		}
		// 检查开发者项目关联
		// 有交付中的，不允许解散
		Integer count = teamProjectRepo.lambdaQuery().eq(TeamProject::getTeamId, teamId)
				.eq(TeamProject::getStatus, DELIVERING.getCode()).count();
		if (count != 0) {
			throw new BusinessException(TC10050003, TC10050003.msg() + "-有交付中的项目");
		}
		// 其余置为已放弃
		LambdaQueryWrapper<TeamProject> qwForTP = Wrappers.<TeamProject>lambdaQuery().eq(TeamProject::getTeamId, teamId)
				.in(TeamProject::getStatus, APPOINT_AWAITING.getCode(), DEV_APPOINT_CONFIRMING.getCode(), DEVELOPING.getCode())
				.select(TeamProject::getTeamProjId);
		List<Long> teamProjIdList = teamProjectRepo.listObjs(qwForTP, o -> Long.valueOf(o.toString()));
		// teamProjLogic.quitProject() 会开启编程式事务！！！
		teamProjIdList.forEach(teamProjId -> teamProjLogic.quitProject(loginUserId, teamProjId));

		teamRepo.removeById(teamId);
	}
}
