package com.wichelns.devaproject.app.module.user.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhijia
 * @date 2024/2/15 21:37
 */
@Getter
@AllArgsConstructor
public enum UserTypeEnum {
	REQUIREMENT_USER(10, "需求用户"),
	DEVELOPER_USER(20, "开发者用户");

	private final Integer code;
	private final String desc;
}
