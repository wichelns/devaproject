package com.wichelns.devaproject.app.core.properties;

import lombok.Data;

/**
 * The class Tencent properties.
 *
 */
@Data
public class TencentProperties {
	private TencentCaptchaProperties captcha = new TencentCaptchaProperties();

	/**
	 * secretId
	 */
	private String secretId;

	/**
	 * secretKey
	 */
	private String secretKey;

	@Data
	public class TencentCaptchaProperties {
		/**
		 * APPID
		 */
		private Long appId;

		/**
		 * App Secret Key
		 */
		private String appSecretKey;
	}
}