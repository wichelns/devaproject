package com.wichelns.devaproject.app.module.user.enums;

import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yoo
 */

@Getter
@AllArgsConstructor
public enum UserStatusEnum {
    NORMAL(1, "正常"),
    BAN(99, "冻结");

    private final Integer code;
    private final String desc;

    public static UserStatusEnum getByCode(Integer code) {
        for (UserStatusEnum value : UserStatusEnum.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        throw new BusinessException(ErrorCodeEnum.GL9999404);
    }

    public static UserStatusEnum getByDesc(String desc) {
        for (UserStatusEnum value : UserStatusEnum.values()) {
            if (value.getDesc().equals(desc)) {
                return value;
            }
        }
        throw new BusinessException(ErrorCodeEnum.GL9999404);
    }
}
