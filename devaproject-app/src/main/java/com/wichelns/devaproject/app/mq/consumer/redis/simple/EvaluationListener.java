package com.wichelns.devaproject.app.mq.consumer.redis.simple;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.module.project.enums.DeveloperProjTypeEnum;
import com.wichelns.devaproject.app.mq.consumer.handler.PersonalProjectHandler;
import com.wichelns.devaproject.app.mq.consumer.handler.TeamProjectHandler;
import com.wichelns.devaproject.common.annotation.Topic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static com.wichelns.devaproject.app.constant.MQConstant.*;

/**
 * 确认项目交付监听
 *
 * @author zhijia
 * @date 2024/2/23 11:53
 */
@Topic(KEY_EVALUATION)
@Slf4j
@Component
@ConditionalOnProperty(value = "mq-type", havingValue = MQ_TYPE_PUBSUB)
public class EvaluationListener implements MessageListener {
	@Resource
	private PersonalProjectHandler personalProjectHandler;
	@Resource
	private TeamProjectHandler teamProjectHandler;
	@Resource
	private TaskExecutor taskExecutor;

	@Override
	public void onMessage(Message message, byte[] pattern) {
		String jsonStr = JSONObject.parseObject(new String(message.getBody()), String.class);
		DeveloperProjDTO developerProjDTO = JSONUtil.toBean(jsonStr, DeveloperProjDTO.class);

		taskExecutor.execute(() -> {
			if (DeveloperProjTypeEnum.PERSONAL_PROJECT.getCode().equals(developerProjDTO.getDevProjType())) {
				personalProjectHandler.handleEvaluation(developerProjDTO);
			} else {
				teamProjectHandler.handleEvaluation(developerProjDTO);
			}
		});
	}

}
