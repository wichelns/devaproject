package com.wichelns.devaproject.app.module.article.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

/***
 * @author xph
 */
@ApiModel(value = "项目需求搜索接收类")
@Data
public class ProjectReqRequestParams {
    @ApiModelProperty(value = "查找的标签")
    private String tags;
    @ApiModelProperty(value = "项目需求的名称")
    private String title;
    @ApiModelProperty(value = "排序依靠")
    private Integer sortBy;
    @ApiModelProperty(value = "页数")
    private Integer page;
    @ApiModelProperty(value = "每页的数量")
    private Integer size;
}
