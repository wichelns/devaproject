package com.wichelns.devaproject.app.core.file.storage.s3;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.URLUtil;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.HttpMethod;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.wichelns.devaproject.app.core.context.SystemContext;
import com.wichelns.devaproject.app.core.file.common.dto.PreUploadDTO;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.net.URL;
import java.util.Date;

/**
 * s3cloud上传文件
 */
@Slf4j
public class S3Operator {
    private AmazonS3 s3client;

    private void initializeAmazon() {
        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setProtocol(Protocol.HTTP);

        //Init Config
        String endpoint = SystemContext.holder().opsForSystem().getValue("file.endpoint");
        String accessKeyId = SystemContext.holder().opsForSystem().getValue("file.accessKeyId");
        String accessKeySecret = SystemContext.holder().opsForSystem().getValue("file.accessKeySecret");
        String region = SystemContext.holder().opsForSystem().getValue("file.region");

        AWSStaticCredentialsProvider credential = new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKeyId, accessKeySecret));
        EndpointConfiguration endpointConfiguration = new EndpointConfiguration(endpoint, region);
        this.s3client = AmazonS3ClientBuilder.standard().withCredentials(credential)
                .withClientConfiguration(clientConfig)
                .withEndpointConfiguration(endpointConfiguration).build();
    }

    public void refreshClient() {
        initializeAmazon();
    }

    public void upload(String pathAndName, File file) {
        try {
            String bucketName = SystemContext.holder().opsForSystem().getValue("file.bucketName");
            String rootDir = SystemContext.holder().opsForSystem().getValue("file.rootDir");

            String bucketPath = bucketName + "/" + rootDir;
            s3client.putObject(new PutObjectRequest(bucketPath, pathAndName, file)
                    .withCannedAcl(CannedAccessControlList.PublicRead));
        } catch (Exception e) {
            log.error("s3 upload fail", e);
            throw e;
        }
    }

    public Boolean exist(String objectName) {
        String bucketName = SystemContext.holder().opsForSystem().getValue("file.bucketName");
        return s3client.doesObjectExist(bucketName, objectName);
    }

    public PreUploadDTO getPreUrl(String pathAndName, Integer time) {
        String bucketName = SystemContext.holder().opsForSystem().getValue("file.bucketName");
        String rootDir = SystemContext.holder().opsForSystem().getValue("file.rootDir");

        DateTime expiration = DateUtil.offsetSecond(new Date(), time);
        URL url = s3client.generatePresignedUrl(bucketName, rootDir + "/" + pathAndName, expiration, HttpMethod.PUT);
        // 文件访问地址
        String urlImage = URLUtil.decode(url.getPath());
        // 预签名put地址
        String preUrl = url.getProtocol() + "://" + url.getHost() +
                URLUtil.decode(url.getFile());

        PreUploadDTO preUrlDTO = new PreUploadDTO();
        preUrlDTO.setUrl(urlImage);
        preUrlDTO.setPreUrl(preUrl);
        return preUrlDTO;
    }

    public void deleteFile(String pathAndName) {
        String bucketName = SystemContext.holder().opsForSystem().getValue("file.bucketName");
        String rootDir = SystemContext.holder().opsForSystem().getValue("file.rootDir");

        s3client.deleteObject(bucketName, rootDir + pathAndName);
    }
}
