package com.wichelns.devaproject.app.module.team.service;

import com.wichelns.devaproject.app.module.team.vo.RespTagLibVO;

import java.util.List;

/**
 * @author zhijia
 * @date 2024/3/3 14:23
 */
public interface RespTagLibService {
	/**
	 * 新增职责标签组
	 */
	void addGroup(String groupName);

	/**
	 * 新增职责标签
	 */
	void addTag(Integer groupId, String tagName);

	/**
	 * 查询职责标签库
	 */
	List<RespTagLibVO> queryLib();
}
