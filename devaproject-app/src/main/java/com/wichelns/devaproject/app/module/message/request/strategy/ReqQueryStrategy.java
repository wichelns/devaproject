package com.wichelns.devaproject.app.module.message.request.strategy;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wichelns.devaproject.app.module.message.request.enums.OperationEnum;
import com.wichelns.devaproject.app.module.message.request.param.ReqParam;
import com.wichelns.devaproject.app.module.message.request.vo.AbstractReqVO;

/**
 * 查询Req策略类接口；S开头表示查自己发送的，R开头表自己接收到的
 *
 * @author zhijia
 * @date 2024/2/28 23:07
 */
public interface ReqQueryStrategy {
	boolean support(OperationEnum operationEnum, int isSender);

	IPage<AbstractReqVO> pageRequest(ReqParam param, Integer pageNo, Integer pageSize);

}
