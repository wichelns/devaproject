package com.wichelns.devaproject.app.module.team.api;

import com.wichelns.devaproject.app.module.secure.annotation.DevelopUserValid;
import com.wichelns.devaproject.app.module.team.service.RespTagLibService;
import com.wichelns.devaproject.app.module.team.vo.RespTagLibVO;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhijia
 * @date 2024/2/16 0:07
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/resptag")
@Api(value = "职责标签库API", tags = "Web - RespTagLibApi")
public class RespTagLibApi {
	@Resource
	private RespTagLibService respTagLibService;

	@PostMapping(value = "/addGroup")
	@ApiOperation(httpMethod = "POST", value = "新增职责标签组")
	@DevelopUserValid
	public Wrapper<?> addGroup(@ApiParam(value = "组名") @RequestParam(value = "groupName") String groupName) {
		respTagLibService.addGroup(groupName);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/addTag")
	@ApiOperation(httpMethod = "POST", value = "新增职责标签")
	@DevelopUserValid
	public Wrapper<?> addTag(@ApiParam(value = "组ID") @RequestParam(value = "groupId") Integer groupId,
							 @ApiParam(value = "标签名") @RequestParam(value = "tagName") String tagName) {
		respTagLibService.addTag(groupId, tagName);
		return WrapMapper.ok();
	}

	@GetMapping(value = "/queryLib")
	@ApiOperation(httpMethod = "GET", value = "查询职责标签组", notes = "前端可缓存起来，不用每次都请求服务器;更新操作后再拉取即可")
	@DevelopUserValid
	public Wrapper<List<RespTagLibVO>> queryLib() {
		return WrapMapper.ok(respTagLibService.queryLib());
	}

}
