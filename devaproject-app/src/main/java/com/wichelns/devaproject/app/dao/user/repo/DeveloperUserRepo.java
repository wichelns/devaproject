package com.wichelns.devaproject.app.dao.user.repo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.user.mapper.DeveloperUserMapper;
import com.wichelns.devaproject.app.module.user.entity.DeveloperUser;
import org.springframework.stereotype.Repository;

/**
 * @author zhijia
 * @date 2024/2/14 11:03
 */
@Repository
public class DeveloperUserRepo extends ServiceImpl<DeveloperUserMapper, DeveloperUser> implements IService<DeveloperUser> {
}
