package com.wichelns.devaproject.app.core.authentication;

import com.wichelns.devaproject.app.core.context.SysUserContext;
import com.wichelns.devaproject.common.exception.BusinessException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.stereotype.Component;

import static com.wichelns.devaproject.common.enums.ErrorCodeEnum.GL99990403;


/**
 * 描述：
 *
 * @author Yoo
 * @date 2020/9/29
 * @since 1.0
 */
@Component
public class ShiroRealm extends AuthorizingRealm {

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    /**
     * 授权，获取用户角色和权限
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection token) {
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();

        //获取用户角色集合
        //todo 加载默认角色
        //User user = userRepo.findById(userId);
        //simpleAuthorizationInfo.setRoles(Collections.singleton(user.getRole().getRoleName()));
        //获取用户权限集合
        //todo 加载默认权限
        //List<Permission> permissions = userService.findAllPermission(mobile).orElse(Collections.emptyList());
        //todo 权限认证
        simpleAuthorizationInfo.setObjectPermissions(null);
        return simpleAuthorizationInfo;
    }

    /**
     * 认证，校验用户身份
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String token = (String) authenticationToken.getCredentials();

        //校验token
        if (Jwt.verify(token)) {
            SysUserContext.holder().refreshToken(token);
            return new SimpleAuthenticationInfo(token, token, getName());
        }

        throw new BusinessException(GL99990403);
    }
}
