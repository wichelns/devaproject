package com.wichelns.devaproject.app.module.team.logic;

import com.wichelns.devaproject.app.dao.team.repo.TeamMemberRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamRepo;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/29 21:15
 */
@Component
public class TeamMemberLogic {
	@Resource
	private TeamRepo teamRepo;
	@Resource
	private TeamMemberRepo teamMemberRepo;

	public void removeMember(Integer teamId, Long memberId) {
		boolean removed = teamMemberRepo.removeById(memberId);
		if (removed) {
			teamRepo.getBaseMapper().incCurMemById(teamId, -1);
		}
	}

}
