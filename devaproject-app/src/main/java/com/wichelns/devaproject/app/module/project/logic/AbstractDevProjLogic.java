package com.wichelns.devaproject.app.module.project.logic;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.wichelns.devaproject.app.core.support.RedisUtil;
import com.wichelns.devaproject.app.dao.article.repo.ProjectRequirementRepo;
import com.wichelns.devaproject.app.dao.project.repo.PersonalProjectRepo;
import com.wichelns.devaproject.app.dao.project.repo.TeamProjectRepo;
import com.wichelns.devaproject.app.module.article.entity.ProjectRequirement;
import com.wichelns.devaproject.app.module.article.enums.ProjectReqState;
import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.module.project.entity.PersonalProject;
import com.wichelns.devaproject.app.module.project.entity.TeamProject;
import com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum;
import com.wichelns.devaproject.app.module.project.enums.DeveloperProjTypeEnum;
import com.wichelns.devaproject.app.mq.dto.MessageDTO;
import com.wichelns.devaproject.app.mq.producer.Producer;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static com.wichelns.devaproject.app.constant.CacheConstant.DEV_PROJECTS_STATUS_CHANGE;
import static com.wichelns.devaproject.app.constant.MQConstant.KEY_DEVELOPER_CONFIRM_DEV;
import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum.*;

/**
 * 个人项目与团队项目的共同逻辑处理
 *
 * @author zhijia
 * @date 2024/2/20 10:31
 */
@Slf4j
public abstract class AbstractDevProjLogic {
	@Resource
	protected PersonalProjectRepo personalProjectRepo;
	@Resource
	protected TeamProjectRepo teamProjectRepo;
	@Resource
	protected ProjectRequirementRepo projectRequirementRepo;
	@Resource
	protected Producer producer;
	@Resource
	protected RedisUtil redisUtil;

	/**
	 * 下列情况将抛出异常，代表不允许参加：
	 * 1.开发者所选身份已经参选（不包括放弃过、被解雇过的情况）；
	 * 2.权限不足（团队身份，但不是leader）；
	 * 3.参选人数已达上限
	 */
	public abstract void checkCanJoin(Integer projectId, Long devUserId, Integer teamId);

	/**
	 * 验证管理项目开发权限
	 */
	public abstract void checkCanManageDevProj(Long devUserId, Long devProjId);

	abstract DeveloperProjDTO getDTOById4Update(Long devProjId);

	abstract void evaluateDevProj(Long devProjId, String comment, Integer stars);

	abstract void changeDevProjStatus(Long devProjId, DeveloperProjStatusEnum targetStatus);

	@Transactional
	public void deliver(Long devUserId, Long devProjId) {
		// 1.权限校验
		checkCanManageDevProj(devUserId, devProjId);
		// 2.一致性锁定读
		DeveloperProjDTO developerProjDTO = getDTOById4Update(devProjId);
		// 3.状态校验
		if (ObjectUtil.notEqual(DEVELOPING.getCode(), developerProjDTO.getStatus())) {
			throw new BusinessException(ErrorCodeEnum.PC10040004);
		}
		// 4.更新逻辑
		changeDevProjStatus(devProjId, DELIVERING);
		projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.DELIVERY_CONFIRMING.getCode())
				.eq(ProjectRequirement::getProjectId, developerProjDTO.getProjectId()).update();
	}

	public void confirmDev(Long devUserId, Long devProjId) {
		// 1.权限校验
		checkCanManageDevProj(devUserId, devProjId);

		Integer projectId = getDTOById4Update(devProjId).getProjectId();
		// 2.并发确认开发、并发取消指认与确认开发防控 todo 细分粒度，可将其划分为多个锁
		redisUtil.trxWithinLock(StrUtil.format(DEV_PROJECTS_STATUS_CHANGE, projectId), () -> {
			// 3.状态校验
			DeveloperProjDTO developerProjDTO = getDTOById4Update(devProjId);
			if (ObjectUtil.notEqual(DEV_APPOINT_CONFIRMING.getCode(), developerProjDTO.getStatus())) {
				throw new BusinessException(ErrorCodeEnum.PC10040003);
			}

			// 4.更新逻辑
			changeDevProjStatus(devProjId, DEVELOPING);
			projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.DEVELOPING.getCode())
					.eq(ProjectRequirement::getProjectId, projectId).update();

			// 5.发送mq
			MessageDTO messageDto = new MessageDTO(KEY_DEVELOPER_CONFIRM_DEV, developerProjDTO);
			producer.produce(messageDto);

			boolean isPersonal = DeveloperProjTypeEnum.PERSONAL_PROJECT.getCode().equals(developerProjDTO.getDevProjType());
			log.info("开发者[{}]-类型[{}]:确认开发项目[{}]", isPersonal ? developerProjDTO.getDevUserId() : developerProjDTO.getTeamId(),
					developerProjDTO.getDevProjType(), projectId);
		});
	}

	public void quitProject(Long devUserId, Long devProjId) {
		// 1.权限校验
		checkCanManageDevProj(devUserId, devProjId);

		Integer projectId = getDTOById4Update(devProjId).getProjectId();
		redisUtil.trxWithinLock(StrUtil.format(DEV_PROJECTS_STATUS_CHANGE, projectId), () -> {
			DeveloperProjDTO developerProjDTO = getDTOById4Update(devProjId);
			// 2.项目状态校验：候选中、开发待确认、开发中 todo 优化不同状态逻辑拆分 状态模式/状态机？
			if (Stream.of(APPOINT_AWAITING, DEV_APPOINT_CONFIRMING, DEVELOPING).noneMatch(e -> e.getCode().equals(developerProjDTO.getStatus()))) {
				throw new BusinessException(ErrorCodeEnum.PC10040002);
			}

			// 3.开发者项目置为已放弃
			changeDevProjStatus(devProjId, QUITED);

			if (ObjectUtil.notEqual(APPOINT_AWAITING.getCode(), developerProjDTO.getStatus())) {
				// 4.项目需求的状态变更处理
				List<Integer> statusList = Arrays.asList(DEV_APPOINT_CONFIRMING.getCode(), DEVELOPING.getCode(),
						DELIVERING.getCode(), DELIVERED.getCode());
				Integer personal = personalProjectRepo.lambdaQuery().eq(PersonalProject::getProjectId, projectId)
						.in(PersonalProject::getStatus, statusList)
						.isNull(PersonalProject::getTeamProjId).count();
				Integer team = teamProjectRepo.lambdaQuery().eq(TeamProject::getProjectId, projectId)
						.in(TeamProject::getStatus, statusList).count();

				if (DEV_APPOINT_CONFIRMING.getCode().equals(developerProjDTO.getStatus()) && personal + team == 0) {
					// 开发待确认状态：放弃后，若有其他待确认指认的、开发中、交付中、已交付的，则需求状态不变，否则回滚为“已发布”
					projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.PUBLISHED.getCode())
							.eq(ProjectRequirement::getProjectId, projectId).update();
				} else if (DEVELOPING.getCode().equals(developerProjDTO.getStatus())) {
					// 开发中：放弃后，若有其他待确认指认的，则回滚为“开发待确认”，否则回滚到“已发布”
					if (personal + team > 0) {
						projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.DEV_APPOINT_CONFIRMING.getCode())
								.eq(ProjectRequirement::getProjectId, projectId).update();
					} else {
						projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.PUBLISHED.getCode())
								.eq(ProjectRequirement::getProjectId, projectId).update();
					}
				}
			}
		});
	}

}