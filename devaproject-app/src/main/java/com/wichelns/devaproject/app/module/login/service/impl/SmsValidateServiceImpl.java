package com.wichelns.devaproject.app.module.login.service.impl;

import cn.hutool.core.lang.Pair;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.wichelns.devaproject.app.module.login.service.SmsValidateService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

import static cn.hutool.core.util.RandomUtil.BASE_NUMBER;

/**
 * @author Yoo
 * @date 2020/9/30
 * @since 1.0
 */
@Service
public class SmsValidateServiceImpl implements SmsValidateService {

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    private final String SMS_KEY_PREFIX = "sms:validate:";

    @Override
    public Pair<String, String> genValidateCode(@NotNull String key, @NotNull Integer timeout) {
        //生成随机数
        String randomCode = RandomUtil.randomString(BASE_NUMBER, 6);
        //存入redis
        redisTemplate.opsForValue().set(SMS_KEY_PREFIX + key, randomCode, timeout, TimeUnit.SECONDS);
        return new Pair<>(key, randomCode);
    }

    @Override
    public Boolean checkValidateCode(@NotNull String key, @NotNull String code) {
        String value = redisTemplate.opsForValue().get(SMS_KEY_PREFIX + key);
        return ObjectUtil.equal(code, value);
    }
}
