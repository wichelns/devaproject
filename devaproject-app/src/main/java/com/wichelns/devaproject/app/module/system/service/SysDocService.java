package com.wichelns.devaproject.app.module.system.service;

import com.wichelns.devaproject.app.module.system.entity.SysDoc;

/**
 * @author zhijia
 * @date 2024/2/14 15:22
 */
public interface SysDocService {
	SysDoc getSysDoc(Integer docType);
}
