package com.wichelns.devaproject.app.core.support;

/**
 * @author zhijia
 * @date 2023/10/18 16:50
 */
public interface RedisLock {
    /**
     * 上锁基入口
     */
    boolean baseLock(String key, String value);
    /**
     * 解锁基入口
     */
    void baseUnlock(String key, String value);
}
