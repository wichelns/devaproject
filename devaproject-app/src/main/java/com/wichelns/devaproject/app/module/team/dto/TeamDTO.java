package com.wichelns.devaproject.app.module.team.dto;

import lombok.Data;

/**
 * @author zhijia
 * @date 2024/3/1 17:32
 */
@Data
public class TeamDTO {
	private Long fromLeaderId;
	private Long toLeaderId;
	private Integer teamId;
}
