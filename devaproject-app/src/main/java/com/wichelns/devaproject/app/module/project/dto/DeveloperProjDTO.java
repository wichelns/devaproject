package com.wichelns.devaproject.app.module.project.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author zhijia
 * @date 2024/2/22 21:49
 */
@Data
public class DeveloperProjDTO {
	@ApiModelProperty(value = "项目类型 10-个人 20-团队")
	private Integer devProjType;

	@ApiModelProperty(value = "个人/团队项目ID")
	private Long devProjId;
	@ApiModelProperty(value = "项目ID")
	private Integer projectId;
	@ApiModelProperty(value = "开发者ID")
	private Long devUserId;
	@ApiModelProperty(value = "团队项目ID")
	private Long teamProjId;
	@ApiModelProperty(value = "团队ID")
	private Integer teamId;
	@ApiModelProperty(value = "队长ID")
	private Long leaderId;
	@ApiModelProperty(value = "额外信息")
	private String extra;
	@ApiModelProperty(value = "额外信息 dto格式")
	private TeamProjExtraDTO extraDTO;
	@ApiModelProperty(value = "参选介绍")
	private String joinIntro;
	@ApiModelProperty(value = "状态")
	private Integer status;
	@ApiModelProperty(value = "交付时间")
	private Date deliverTime;
	@ApiModelProperty(value = "评语")
	private String comment;
	@ApiModelProperty(value = "星星数")
	private Integer stars;

	// private Integer deleted;
	// private Integer version;
	// private Date createdTime;
	// private Date updatedTime;
}
