package com.wichelns.devaproject.app.module.article.util;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 淡漠
 * @version 1.0
 */
public class TagUtil {
    public static String checkTag(String target){
        return target.trim().toLowerCase().replaceAll("\\s+"," ");
    }
    public static String checkTagList(List<String> target){
        return "["+target.stream().map(TagUtil::checkTag).collect(Collectors.joining(","))+"]";
    }
}
