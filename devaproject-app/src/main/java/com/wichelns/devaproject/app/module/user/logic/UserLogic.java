package com.wichelns.devaproject.app.module.user.logic;

import cn.hutool.core.util.RandomUtil;
import org.springframework.stereotype.Component;

/**
 * @author zhijia
 * @date 2024/2/16 1:07
 */
@Component
public class UserLogic {

	public String getDefaultName(String phone) {
		return RandomUtil.randomString(6) + "_" + phone.substring(7);
	}

	public String getDefaultAvatar() {
		return "";// todo
	}
}
