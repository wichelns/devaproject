package com.wichelns.devaproject.app.dao.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wichelns.devaproject.app.module.user.entity.CreditRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhijia
 * @date 2024/2/20 23:58
 */
@Mapper
public interface CreditRecordMapper extends BaseMapper<CreditRecord> {
}
