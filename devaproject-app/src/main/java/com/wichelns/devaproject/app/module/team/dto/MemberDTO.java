package com.wichelns.devaproject.app.module.team.dto;

import lombok.Data;

/**
 * @author zhijia
 * @date 2024/3/1 17:32
 */
@Data
public class MemberDTO {
	private Integer teamId;
	private Long memberId;
	private Long devUserId;
	private Integer identity;
}
