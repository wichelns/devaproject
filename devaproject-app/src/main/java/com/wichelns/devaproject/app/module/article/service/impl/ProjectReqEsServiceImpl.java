package com.wichelns.devaproject.app.module.article.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.wichelns.devaproject.app.dao.article.repo.ProjectRequirementRepo;
import com.wichelns.devaproject.app.dao.user.repo.RequirementUserRepo;
import com.wichelns.devaproject.app.module.article.dto.ProReqDto;
import com.wichelns.devaproject.app.module.article.entity.ProjectRequirement;
import com.wichelns.devaproject.app.module.article.enums.SortByEnum;
import com.wichelns.devaproject.app.module.article.param.ProjectReqRequestParams;
import com.wichelns.devaproject.app.module.article.service.ProjectReqEsService;
import com.wichelns.devaproject.app.module.user.entity.RequirementUser;
import com.wichelns.devaproject.common.enums.BaseStatus;
import com.wichelns.devaproject.common.model.BaseEntity;
import com.yoo.stark.toolkit.name.NameUtil;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author zhuyu
 * @update 2024/3/3 17:35
 */
@Service
public class ProjectReqEsServiceImpl implements ProjectReqEsService {
	@Resource
	private RequirementUserRepo requirementUserRepo;
	@Resource
	private RestHighLevelClient client;
	@Resource
	private ProjectRequirementRepo projectRequirementRepo;

	@Override
	public List<ProReqDto> search(ProjectReqRequestParams proReqRequestParams) {
		SearchRequest request = new SearchRequest("project_requirement");
		String name = proReqRequestParams.getTitle();
		BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
//        List<String> tags  = JSONArray.parseArray(proReqRequestParams.getTags(),String.class);
		List<String> tags = new ArrayList<>();
		if (proReqRequestParams.getTags() != null) {
			String[] tagArr = proReqRequestParams.getTags().substring(1, proReqRequestParams.getTags().length() - 1).split(",");
			tags = Arrays.asList(tagArr);
		}

		System.out.println(name);
		// 判断name和tag属性是否为空，为空全查
		if ((name == null || name.equals("")) && tags.isEmpty()) {
			queryBuilder.must(QueryBuilders.matchAllQuery());
		} else {
			// 对name或者tag单独查询
			if (name != null && !name.equals("")) {
				queryBuilder.must(QueryBuilders.matchQuery("title", name));
			}
			if (CollUtil.isNotEmpty(tags)) {
				for (String tag : tags) {
					if (StrUtil.isNotBlank(tag))
						queryBuilder.must(QueryBuilders.termQuery("tagList", tag));
				}
			}
		}

		queryBuilder.must(QueryBuilders.termQuery(NameUtil.string(BaseEntity::getDeleted),
				BaseStatus.N.getCode()));

		request.source().query(queryBuilder);

		// 排序 报酬、观看量、创建时间升序降序查询
		Integer sortBy = proReqRequestParams.getSortBy();
		if (SortByEnum.REWARD_ACS.getCode().equals(sortBy)) {
			request.source().sort("reward", SortOrder.ASC);
		} else if (SortByEnum.REWARD_DESC.getCode().equals(sortBy)) {
			request.source().sort("reward", SortOrder.DESC);
		} else if (SortByEnum.VIEW_COUNT_ACS.getCode().equals(sortBy)) {
			request.source().sort("viewCount", SortOrder.ASC);
		} else if (SortByEnum.VIEW_COUNT_DESC.getCode().equals(sortBy)) {
			request.source().sort("viewCount", SortOrder.DESC);
		} else if (SortByEnum.CREATED_TIME_ACS.getCode().equals(sortBy)) {
			request.source().sort("createdTime", SortOrder.ASC);
		} else if (SortByEnum.CREATED_TIME_DESC.getCode().equals(sortBy)) {
			request.source().sort("createdTime", SortOrder.DESC);
		}

		// 分页，设置一下初始值
		int page = 1;
		int size = 10;
		if (proReqRequestParams.getPage() != null) page = proReqRequestParams.getPage();
		if (proReqRequestParams.getSize() != null) size = proReqRequestParams.getSize();
		request.source().from((page - 1) * size).size(size);

		SearchResponse search = null;
		try {
			search = client.search(request, RequestOptions.DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}
		SearchHits hits = search.getHits();
		SearchHit[] hits1 = hits.getHits();
		List<ProReqDto> list = new ArrayList<>();
		for (SearchHit hit : hits1) {
			ProReqDto list1 = JSON.parseObject(hit.getSourceAsString(), ProReqDto.class);
			list.add(list1);
		}
		return list;
	}

	@Override
	public void insertOrUpdateEsById(Integer projectId) {
		ProReqDto proReqDto = new ProReqDto();
		ProjectRequirement projectRequirement = projectRequirementRepo.getById(projectId);
		BeanUtils.copyProperties(projectRequirement, proReqDto);

		// 查询项目列表的用户信息
		RequirementUser requirementUser = requirementUserRepo.getById(projectRequirement.getReqUserId());
		proReqDto.setUsername(requirementUser.getUsername());
		proReqDto.setAvatar(requirementUser.getAvatar());
		proReqDto.setPhone(requirementUser.getPhone());

		String[] tagArr = projectRequirement.getTagList().substring(1, projectRequirement.getTagList().length() - 1).split(", *");
		List<String> tags = Arrays.asList(tagArr);
		proReqDto.setTagList(tags);


		// 设置补充查询的属性
		ArrayList<String> list = new ArrayList<>();
		list.add(projectRequirement.getTitle());
		proReqDto.setSuggestion(list);


		// 1、准备request对象
		IndexRequest request = new IndexRequest("project_requirement").id(projectRequirement.getProjectId().toString());
		// 2、载入json文件
		request.source(JSON.toJSONString(proReqDto), XContentType.JSON);
		// 3、发送请求
		try {
			client.index(request, RequestOptions.DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<String> getSuggestion(String prefix) {
		SearchRequest request = new SearchRequest("project_requirement");
		request.source().suggest(new SuggestBuilder().addSuggestion("suggestion",
				SuggestBuilders.completionSuggestion("suggestion")
						.prefix(prefix)
						.skipDuplicates(true)
						.size(10)
		));
		SearchResponse search = null;
		try {
			search = client.search(request, RequestOptions.DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}
		List<String> list = new ArrayList<>();
		Suggest suggest = search.getSuggest();
		CompletionSuggestion suggestion = suggest.getSuggestion("suggestion");
		List<CompletionSuggestion.Entry.Option> options1 = suggestion.getOptions();
		for (CompletionSuggestion.Entry.Option option : options1) {
			String string = option.getText().string();
			list.add(string);
		}

		return list;
	}

	@Override
	public void undeleteById(ProjectRequirement projectRequirement) {
		projectRequirement.setDeleted(BaseStatus.N.getCode());
		insertOrUpdateEsById(projectRequirement.getProjectId());
	}

	@Override
	public void deleteById(ProjectRequirement projectRequirement) {
		projectRequirement.setDeleted(BaseStatus.Y.getCode());
		insertOrUpdateEsById(projectRequirement.getProjectId());
	}

	public void deleteById2(ProjectRequirement projectRequirement) {
		DeleteRequest request = new DeleteRequest("project_requirement");
		try {
			client.delete(request, RequestOptions.DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
