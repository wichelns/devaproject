package com.wichelns.devaproject.app.module.article.enums;

import com.wichelns.devaproject.app.core.fsm.BaseEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 需求用户对项目状态可能有影响的操作事件
 *
 * @author zhijia
 * @date 2024/2/17 14:34
 */
@Getter
@AllArgsConstructor
public enum ProjectReqEvent implements BaseEvent {
	ADD_DRAFT(5, "新增草稿"),
	PUBLISH(15, "发布项目需求"),
	WITHDRAW(22, "撤回"),
	MODIFY(24, "修改"),
	APPOINT_DEVELOPER(26, "指认开发者"),
	CANCEL_APPOINTMENT(32, "取消指认"),
	DISMISS_DEVELOPER(42, "解雇开发者"),
	CONFIRM_DELIVERY(52, "确认项目交付"),
	DENY_DELIVERY(54, "否认交付"),
	SUBMIT_COMMENT(62, "提交评价"),
	;

	private final Integer code;
	private final String desc;
}
