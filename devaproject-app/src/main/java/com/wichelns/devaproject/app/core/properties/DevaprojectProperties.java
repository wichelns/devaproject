package com.wichelns.devaproject.app.core.properties;


import com.wichelns.devaproject.common.constant.GlobalConstant;
import lombok.Data;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

/**
 * properties.
 */
@Data
@SpringBootConfiguration
@PropertySource(value = {"classpath:devaproject.properties"})
@ConfigurationProperties(prefix = GlobalConstant.ROOT_PREFIX)
public class DevaprojectProperties {
    private AsyncTaskProperties task = new AsyncTaskProperties();
    private ShiroProperties shiro = new ShiroProperties();
}
