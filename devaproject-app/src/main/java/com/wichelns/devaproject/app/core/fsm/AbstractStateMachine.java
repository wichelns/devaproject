package com.wichelns.devaproject.app.core.fsm;

import cn.hutool.core.lang.Pair;

import java.util.HashMap;
import java.util.Map;

/**
 * 抽象状态机类：提供存储结构以及公共实现。
 * 
 * @author zhijia
 * @date 2024/2/17 15:24
 */
public abstract class AbstractStateMachine<S extends BaseState, E extends BaseEvent> implements BaseStateMachine<S, E> {
	private final Map<Pair<S, E>, S> container = new HashMap<>();// 不需要使用ConcurrentHashMap

	@Override
	public void accept(S sourceState, E event, S targetState) {
		container.put(Pair.of(sourceState, event), targetState);
	}

	@Override
	public S getTargetState(S sourceState, E event) {
		return container.get(Pair.of(sourceState, event));
	}
	
}
