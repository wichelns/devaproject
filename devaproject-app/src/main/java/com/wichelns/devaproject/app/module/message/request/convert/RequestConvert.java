package com.wichelns.devaproject.app.module.message.request.convert;

import com.wichelns.devaproject.app.module.message.request.entity.Request;
import com.wichelns.devaproject.app.module.message.request.vo.DevUserReqVO;
import com.wichelns.devaproject.app.module.message.request.vo.TeamReqVO;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.app.module.user.entity.DeveloperUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @author zhijia
 * @date 2024/2/29 16:35
 */
@Mapper
public interface RequestConvert {
	RequestConvert INSTANCE = Mappers.getMapper(RequestConvert.class);

	@Mapping(source = "request.createdTime", target = "inviteTime")
	@Mapping(source = "request.createdTime", target = "reqTime")
	TeamReqVO domain2VO(Request request, Team team);

	@Mapping(source = "request.status", target = "status")
	@Mapping(source = "request.createdTime", target = "inviteTime")
	@Mapping(source = "request.createdTime", target = "reqTime")
	@Mapping(source = "devUser.avatar", target = "avatar")
	@Mapping(source = "devUser.profile", target = "profile")
	@Mapping(source = "devUser.deliverProjectCount", target = "deliverProjectCount")
	@Mapping(source = "devUser.recvProjectCount", target = "recvProjectCount")
	@Mapping(source = "devUser.stars", target = "stars")
	DevUserReqVO domain2VO(Request request, Team team, DeveloperUser devUser);

}
