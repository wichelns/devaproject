package com.wichelns.devaproject.app.dao.article.repo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.article.mapper.ProjectRequirementMapper;
import com.wichelns.devaproject.app.module.article.entity.ProjectRequirement;
import org.springframework.stereotype.Repository;

/**
 * @author zhijia
 * &#064;date  2024/2/22 17:21
 */
@Repository
public class ProjectRequirementRepo extends ServiceImpl<ProjectRequirementMapper, ProjectRequirement> implements IService<ProjectRequirement> {
}
