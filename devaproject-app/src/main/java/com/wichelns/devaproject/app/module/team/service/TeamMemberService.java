package com.wichelns.devaproject.app.module.team.service;

/**
 * @author zhijia
 * @date 2024/3/2 21:04
 */
public interface TeamMemberService {
	/**
	 * 踢人
	 */
	void kick(long memberId);

	/**
	 * 退出团队
	 */
	void quitTeam(int teamId);

	/**
	 * 备注队友
	 */
	void remark(long memberId, String remark);

	/**
	 * 分配职责标签
	 *
	 * @param respTagId 格式：职责标签组-组内标签ID
	 */
	void attachRespTag(long memberId, String respTagId);

}
