package com.wichelns.devaproject.app.monitor.transaction.filter;

import com.alibaba.druid.filter.FilterChain;
import com.alibaba.druid.filter.FilterEventAdapter;
import com.alibaba.druid.proxy.jdbc.ConnectionProxy;
import com.wichelns.devaproject.app.monitor.transaction.recorder.AbstractDruidTrxRecorder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.sql.Savepoint;

/**
 * 事务过滤器，负责事务监控
 *
 * @author zhijia
 * @date 2023/9/7 12:06
 */
@Component
public class DruidTrxFilter extends FilterEventAdapter {
    @Resource
    private AbstractDruidTrxRecorder druidTrxRecorder;// todo 优化为执行链


    @Override
    public void connection_setAutoCommit(FilterChain chain, ConnectionProxy connection, boolean autoCommit) throws SQLException {
        super.connection_setAutoCommit(chain, connection, autoCommit);
        if (!autoCommit) {
            druidTrxRecorder.afterTrxBegin(chain, connection);
        }
    }

    @Override
    public void connection_commit(FilterChain chain, ConnectionProxy connection) throws SQLException {
        super.connection_commit(chain, connection);
        druidTrxRecorder.afterTrxCommit(chain, connection);
    }

    @Override
    public void connection_rollback(FilterChain chain, ConnectionProxy connection) throws SQLException {
        super.connection_rollback(chain, connection);
        druidTrxRecorder.afterTrxRollback(chain, connection);
    }

    @Override
    public void connection_rollback(FilterChain chain, ConnectionProxy connection, Savepoint savepoint) throws SQLException {
        super.connection_rollback(chain, connection, savepoint);
        druidTrxRecorder.afterTrxRollback(chain, connection, savepoint);
    }

}
