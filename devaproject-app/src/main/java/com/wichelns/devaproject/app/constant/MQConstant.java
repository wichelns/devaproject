package com.wichelns.devaproject.app.constant;

/**
 * @author zhijia
 * @date 2024/2/23 13:37
 */
public interface MQConstant {

	/* **************消息队列类型******************* */
	/**
	 * 消息队列类型
	 */
	String MQ_TYPE_PUBSUB = "redis-pubsub";


	/* ************** 消息队列路由/话题 ******************* */

	/**
	 * 团队加入项目需求候选
	 */
	String KEY_PROJECT_TEAM_JOIN = "PROJECT_TEAM_JOIN";
	/**
	 * 确认开发项目
	 */
	String KEY_DEVELOPER_CONFIRM_DEV = "DEVELOPER_CONFIRM_DEV";
	/**
	 * 确认项目交付
	 */
	String KEY_DELIVERY_CONFIRM = "DELIVERY_CONFIRM";
	/**
	 * 项目交付评价
	 */
	String KEY_EVALUATION = "EVALUATION";
	/**
	 * 队长转让
	 */
	String KEY_LEADER_CHANGE = "LEADER_CHANGE";
	/**
	 * 成员退出
	 */
	String KEY_MEMBER_OUT = "MEMBER_OUT";
}
