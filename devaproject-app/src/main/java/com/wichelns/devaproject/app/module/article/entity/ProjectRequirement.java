package com.wichelns.devaproject.app.module.article.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wichelns.devaproject.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * @author zhijia
 * @date 2024/2/16 21:35
 */
@ApiModel(value = "项目需求")
@TableName(value = "project_requirement", autoResultMap = true)
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
public class ProjectRequirement extends BaseEntity {
	// ID、标题、简介、内容、作者ID、报酬、浏览量、标签列表、状态（草稿、已发布、等待开发确认中、开发中、交付确认中、已交付...）、交付时间
	@ApiModelProperty(value = "项目需求ID")
	@TableId(type = IdType.AUTO)
	private Integer projectId;
	@ApiModelProperty(value = "标题")
	private String title;
	@ApiModelProperty(value = "描述/项目简介")
	private String description;
	@ApiModelProperty(value = "内容")
	private String content;
	@ApiModelProperty(value = "需求用户ID")
	private Long reqUserId;
	@ApiModelProperty(value = "报酬")
	private Double reward;
	@ApiModelProperty(value = "浏览量")
	private Integer viewCount;
	@ApiModelProperty(value = "标签列表 ['ab','bc',...]")
	private String tagList;
	@ApiModelProperty(value = "项目需求状态 10-草稿 20-已发布 30-等待开发确认中 40-开发中 50-交付确认中 60-已交付待评价 70-已评价")
	private Integer status;
	@ApiModelProperty(value = "交付时间")
	private Date deliverTime;

	@Tolerate
	public ProjectRequirement() {}

}
