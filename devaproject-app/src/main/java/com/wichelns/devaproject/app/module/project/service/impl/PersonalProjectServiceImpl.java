package com.wichelns.devaproject.app.module.project.service.impl;

import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.dao.project.repo.PersonalProjectRepo;
import com.wichelns.devaproject.app.module.project.entity.PersonalProject;
import com.wichelns.devaproject.app.module.project.logic.AbstractDevProjLogic;
import com.wichelns.devaproject.app.module.project.param.DeveloperProjParam;
import com.wichelns.devaproject.app.module.project.service.DeveloperProjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum.APPOINT_AWAITING;

/**
 * @author zhijia
 * @date 2024/2/21 0:02
 */
@Slf4j
@Service("personal")
public class PersonalProjectServiceImpl implements DeveloperProjectService {
	@Resource
	private PersonalProjectRepo personalProjectRepo;
	@Resource
	private AbstractDevProjLogic personalProjLogic;


	@Override
	public void joinProject(DeveloperProjParam param) {
		Long devUserId = Jwt.getLoginUserId();

		// todo 前端防抖处理
		// 校验能否参加
		personalProjLogic.checkCanJoin(param.getProjectId(), devUserId, null);

		PersonalProject personalProject = PersonalProject.builder()
				.projectId(param.getProjectId())
				.devUserId(devUserId)
				.joinIntro(param.getJoinIntro())
				.status(APPOINT_AWAITING.getCode()).build();
		personalProjectRepo.save(personalProject);
	}

	@Override
	public void editJoinIntro(DeveloperProjParam param) {
		// 权限校验
		personalProjLogic.checkCanManageDevProj(Jwt.getLoginUserId(), param.getDeveloperProjId());

		personalProjectRepo.lambdaUpdate().set(PersonalProject::getJoinIntro, param.getJoinIntro())
				.eq(PersonalProject::getPersonalProjId, param.getDeveloperProjId()).update();
	}

	@Override
	public void confirmDevAppointment(DeveloperProjParam param) {
		personalProjLogic.confirmDev(Jwt.getLoginUserId(), param.getDeveloperProjId());
	}

	@Override
	public void deliver(DeveloperProjParam param) {
		personalProjLogic.deliver(Jwt.getLoginUserId(), param.getDeveloperProjId());
	}

	@Override
	public void quitProject(Long devProjId) {
		Long devUserId = Jwt.getLoginUserId();
		personalProjLogic.quitProject(devUserId, devProjId);
	}
}
