package com.wichelns.devaproject.app.dao.team.repo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.team.mapper.TeamMapper;
import com.wichelns.devaproject.app.module.team.entity.Team;
import org.springframework.stereotype.Repository;

/**
 * @author zhijia
 * @date 2024/2/21 20:29
 */
@Repository
public class TeamRepo extends ServiceImpl<TeamMapper, Team> implements IService<Team> {
}
