package com.wichelns.devaproject.app.core.aop;

import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

/**
 * 业务日志aop切面
 *
 * @author xuyuxiang
 * @date 2020/3/20 11:47
 */
@Slf4j
@Aspect
@Component
public class JobMonitorAop {

    /**
     * 日志切入点
     *
     * @author yoo
     * @date 2022/7/03 17:10
     */
    @Pointcut("@annotation(org.springframework.scheduling.annotation.Scheduled)")
    private void pointcut() {
    }

    /**
     * 操作成功返回结果记录日志
     *
     * @author yoo
     * @date 2022/7/03 11:51
     */
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        //日志链路
        MDC.put("trace_uuid", IdUtil.fastSimpleUUID());

        String className = point.getTarget().getClass().getSimpleName();
        long start = System.currentTimeMillis();
        Object result;
        try {
            result = point.proceed();
            log.info("Job执行成功[{}][{}ms]", className, System.currentTimeMillis() - start);
        }catch (Exception e) {
            log.info("Job执行失败[{}][{}ms]", className, System.currentTimeMillis() - start);
            log.error("Job执行失败", e);
            throw e;
        }
        return result;
    }
}
