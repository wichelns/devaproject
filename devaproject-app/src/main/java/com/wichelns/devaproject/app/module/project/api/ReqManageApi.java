package com.wichelns.devaproject.app.module.project.api;

import com.wichelns.devaproject.app.module.project.param.ReqManageParam;
import com.wichelns.devaproject.app.module.project.service.ReqManageService;
import com.wichelns.devaproject.app.module.secure.annotation.RequireUserValid;
import com.wichelns.devaproject.common.model.BaseParam;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/26 11:45
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/req/devproj")
@Api(value = "需求用户与开发者项目交互API", tags = "Web - ReqManageApi")
public class ReqManageApi {
	@Resource
	private ReqManageService reqManageService;

	@PostMapping(value = "/toggleAppoint")
	@ApiOperation(httpMethod = "POST", value = "指认或取消指认")
	@RequireUserValid
	public Wrapper<?> toggleAppoint(@RequestBody @Validated(BaseParam.updateInfo.class) ReqManageParam param) {
		reqManageService.toggleAppoint(param);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/confirmOrReject")
	@ApiOperation(httpMethod = "POST", value = "确认或拒绝项目交付申请")
	@RequireUserValid
	public Wrapper<?> confirmOrReject(@RequestBody @Validated({BaseParam.updateInfo.class, ReqManageParam.confirmFlag.class}) ReqManageParam param) {
		reqManageService.confirmOrReject(param);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/dismiss")
	@ApiOperation(httpMethod = "POST", value = "解雇开发")
	@RequireUserValid
	public Wrapper<?> dismiss(@RequestBody @Validated(BaseParam.updateInfo.class) ReqManageParam param) {
		reqManageService.dismiss(param);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/evaluate")
	@ApiOperation(httpMethod = "POST", value = "项目交付评价")
	@RequireUserValid
	public Wrapper<?> evaluate(@RequestBody @Validated({BaseParam.updateInfo.class, ReqManageParam.evaluate.class}) ReqManageParam param) {
		reqManageService.evaluate(param);
		return WrapMapper.ok();
	}

}
