package com.wichelns.devaproject.app.module.message.request.param;

import com.wichelns.devaproject.common.model.BaseParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @author zhijia
 * @date 2024/2/28 23:26
 */
@Data
@ApiModel(description = "请求Param")
@EqualsAndHashCode(callSuper = false)
public class ReqParam  extends BaseParam {
	@NotNull(groups = add.class, message = "来源ID不可为空")
	@ApiModelProperty(value = "来源ID")
	private Long sourceId;
	@ApiModelProperty(value = "来源类型 10-个人 20-团队")
	private Integer sourceType;
	@NotNull(groups = add.class, message = "目标ID不可为空")
	@ApiModelProperty(value = "目标ID")
	private Long targetId;
	@ApiModelProperty(value = "目标类型 10-个人 20-团队")
	private Integer targetType;
	@NotNull(groups = add.class, message = "操作码不可为空")
	@ApiModelProperty(value = "操作 1-入队邀请 5-入队申请 10-好友申请")
	private Integer operation;
}
