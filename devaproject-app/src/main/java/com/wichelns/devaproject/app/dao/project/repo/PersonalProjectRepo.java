package com.wichelns.devaproject.app.dao.project.repo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.project.mapper.PersonalProjectMapper;
import com.wichelns.devaproject.app.module.project.entity.PersonalProject;
import com.wichelns.devaproject.app.module.system.entity.SysDoc;
import org.springframework.stereotype.Repository;

/**
 * @author zhijia
 * @date 2024/2/14 10:59
 */
@Repository
public class PersonalProjectRepo extends ServiceImpl<PersonalProjectMapper, PersonalProject> implements IService<PersonalProject>  {
}
