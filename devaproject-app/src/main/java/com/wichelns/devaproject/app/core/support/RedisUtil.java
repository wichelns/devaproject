package com.wichelns.devaproject.app.core.support;

import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicReference;

import static com.wichelns.devaproject.common.enums.ErrorCodeEnum.UAC10010027;

/**
 * @author zhijia
 * @date 2024/2/21 23:40
 */
@Slf4j
@Component
public class RedisUtil {
	@Resource
	private DataSourceTransactionManager dataSourceTransactionManager;
	@Resource
	private TransactionDefinition transactionDefinition;
	@Resource
	private DefaultRedisLock redisLock;

	public void roundedLock(String key, Runnable task, Runnable bizFallback, Runnable exFallback) {
		if (redisLock.lock(key)) {// 并发数据安全、“限流”
			try {
				task.run();// 核心
			} catch (BusinessException biz) {
				if (bizFallback != null) {
					bizFallback.run();
				}
				throw biz;
			} catch (Exception ex) {
				log.error("异常：", ex);
				if (exFallback != null) {
					exFallback.run();
				}
				throw new BusinessException(UAC10010027);
			} finally {
				redisLock.unlock(key);
			}
		} else {
			log.info("获取锁失败,key[{}]", key);
			throw new BusinessException(UAC10010027);
		}
	}

	public void trxWithinLock(String key, Runnable task) {
		AtomicReference<TransactionStatus> transactionStatus = new AtomicReference<>();
		Runnable rollbackFunc = () -> dataSourceTransactionManager.rollback(transactionStatus.get());
		Runnable trxWrapper = () -> {
			// 手动开启事务
			transactionStatus.set(dataSourceTransactionManager.getTransaction(transactionDefinition));
			task.run();
			// 提交事务
			dataSourceTransactionManager.commit(transactionStatus.get());
		};
		roundedLock(key, trxWrapper, rollbackFunc, rollbackFunc);
	}

}
