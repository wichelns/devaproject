package com.wichelns.devaproject.app.module.message.request.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author zhijia
 * @date 2024/2/29 16:29
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TeamReqVO extends AbstractReqVO {
	/*
	* team
	* */
	@ApiModelProperty(value = "团队ID")
	private Integer teamId;
	@ApiModelProperty(value = "队长ID")
	private Long leaderId;
	@ApiModelProperty(value = "队长名称")
	private String leaderName;
	@ApiModelProperty(value = "头像")
	private String avatar;
	@ApiModelProperty(value = "团名名称")
	private String teamName;
	@ApiModelProperty(value = "简介")
	private String profile;
	@ApiModelProperty(value = "当前人数")
	private Integer curMemberCount;
	@ApiModelProperty(value = "最大人数")
	private Integer maxMemberCount;

	/*
	* request
	* */
	@ApiModelProperty(value = "邀请时间")
	private Date inviteTime;
	@ApiModelProperty(value = "申请时间")
	private Date reqTime;

}
