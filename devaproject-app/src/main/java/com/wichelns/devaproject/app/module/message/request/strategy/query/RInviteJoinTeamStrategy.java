package com.wichelns.devaproject.app.module.message.request.strategy.query;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.dao.message.repo.RequestRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamRepo;
import com.wichelns.devaproject.app.module.message.request.convert.RequestConvert;
import com.wichelns.devaproject.app.module.message.request.entity.Request;
import com.wichelns.devaproject.app.module.message.request.enums.OperationEnum;
import com.wichelns.devaproject.app.module.message.request.param.ReqParam;
import com.wichelns.devaproject.app.module.message.request.strategy.ReqQueryStrategy;
import com.wichelns.devaproject.app.module.message.request.vo.AbstractReqVO;
import com.wichelns.devaproject.app.module.project.enums.DeveloperTypeEnum;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.common.enums.BaseStatus;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 查询【接收】的【入队邀请】Request
 *
 * @author zhijia
 * @date 2024/2/28 23:41
 */
@Component
public class RInviteJoinTeamStrategy implements ReqQueryStrategy {
	private static final OperationEnum OPERATION = OperationEnum.INVITATION_JOIN_TEAM;
	private static final int IS_SENDER = BaseStatus.N.getCode();
	@Resource
	private RequestRepo requestRepo;
	@Resource
	private TeamRepo teamRepo;

	@Override
	public boolean support(OperationEnum operationEnum, int isSender) {
		return OPERATION.equals(operationEnum) && IS_SENDER == isSender;
	}

	@Override
	public IPage<AbstractReqVO> pageRequest(ReqParam param, Integer pageNo, Integer pageSize) {
		// 1. 根据target=devUserId查询Request
		Long devUserId = Jwt.getLoginUserId();
		LambdaQueryWrapper<Request> qwForReq = Wrappers.<Request>lambdaQuery().eq(Request::getTargetId, devUserId)
				.eq(Request::getTargetType, DeveloperTypeEnum.DEVELOPER_USER.getCode())
				.orderByDesc(Request::getCreatedTime);
		Page<Request> requestPage = requestRepo.page(new Page<>(pageNo, pageSize), qwForReq);

		if (CollUtil.isEmpty(requestPage.getRecords())) {
			return new Page<>();
		}

		// 2. 根据source=teamId查询团队
		List<Long> teamIdList = requestPage.getRecords().stream().map(Request::getSourceId).collect(Collectors.toList());
		Map<Integer, Team> teamMap = teamRepo.listByIds(teamIdList).stream().collect(Collectors.toMap(Team::getTeamId, t -> t));// team可能逻辑删除了

		return requestPage.convert(request -> RequestConvert.INSTANCE.domain2VO(request, teamMap.get(request.getSourceId().intValue())));
	}

}
