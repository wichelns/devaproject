package com.wichelns.devaproject.app.core.context;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.wichelns.devaproject.app.core.file.storage.s3.S3Operator;
import com.wichelns.devaproject.app.dao.system.repo.SysDocRepo;
import com.wichelns.devaproject.app.module.system.entity.SysDoc;
import com.wichelns.devaproject.app.module.system.enums.SysDocEnum;
import com.wichelns.devaproject.common.enums.BaseStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.wichelns.devaproject.app.constant.CacheConstant.*;

/**
 * 系统相关上下文，提供<b>业务配置</b>与<b>系统配置</b>的缓存服务
 */
@Slf4j
@Component
public class SystemContext implements InitializingBean {
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private SysDocRepo sysDocRepo;
    private ConfigContext systemConfig;
    private ConfigContext businessConfig;
    private S3Operator s3Operator;

    public static SystemContext holder() {
        return SpringUtil.getBean(SystemContext.class);
    }

    @Override
    public void afterPropertiesSet() {
        systemConfig = new SysConfigContext(SysDocEnum.SYSTEM_CONFIG.getCode(), SYSTEM_CONFIG_CONTEXT);
        businessConfig = new ConfigContext(SysDocEnum.BUSINESS_CONFIG.getCode(), BUSINESS_CONFIG_CONTEXT);
    }

    public SysConfigContext opsForSystem() {
        return (SysConfigContext) systemConfig;
    }

    public ConfigContext opsForBusiness() {
        return businessConfig;
    }

    public class ConfigContext {
        protected final Integer docType;
        protected final String contextKey;

        public ConfigContext(Integer docType, String contextKey) {
            this.docType = docType;
            this.contextKey = contextKey;
        }

        public String getValue(String field, String defaultValue) {
            String value = redisTemplate.<String, String>opsForHash().get(contextKey, field);
            if (StrUtil.isNotBlank(value)) {
                return value;
            }
            // 暂时没必要考虑缓存击穿或穿透问题
            SysDoc sysDoc = sysDocRepo.lambdaQuery().eq(SysDoc::getType, docType).eq(SysDoc::getStatus, BaseStatus.Y.getCode()).one();
            if (sysDoc != null && StrUtil.isNotBlank(sysDoc.getContent())) {
                Object targetValue = JSONUtil.getByPath(JSONUtil.parse(sysDoc.getContent()), field);
                //缓存
                if (targetValue != null) {
                    value = targetValue.toString();
                    redisTemplate.<String, String>opsForHash().put(contextKey, field, value);
                    return value;
                }
            }

            return defaultValue;
        }

        public String getValue(String field) {
            return getValue(field, "");
        }
        
    }

    public class SysConfigContext extends ConfigContext {
        private static final String ID_BUCKET_KEY_PATH = "id-buckets";

        public SysConfigContext(Integer docType, String contextKey) {
            super(docType, contextKey);
        }

        @Override
        public String getValue(String field, String defaultValue) {
            if (ID_BUCKET_KEY_PATH.equals(field)) {
                return "";
            }
            return super.getValue(field, defaultValue);
        }

        public Map<Object, Object> getValidIdBucketKeys() {
            // 查缓存
            Map<Object, Object> validIdBucketKeys = redisTemplate.opsForHash().entries(VALID_ID_BUCKETS);
            // 命中且非空则返回
            if (CollUtil.isNotEmpty(validIdBucketKeys)) {
                return validIdBucketKeys;
            }
            log.info("缓存ID桶missing");
            // 未命中或为空则查DB
            SysDoc sysConfig = sysDocRepo.lambdaQuery().eq(SysDoc::getType, docType)
                    .eq(SysDoc::getStatus, BaseStatus.Y.getCode()).one();
            // 解析为json对象
            JSON sysConfigJson = JSONUtil.parse(sysConfig.getContent());
            JSONArray target = (JSONArray) sysConfigJson.getByPath(ID_BUCKET_KEY_PATH);//[{"b1":126},{"b2":147},...]
            if (target == null || target.isEmpty()) {
                return Collections.emptyMap();
            }
            // 改DB
            sysConfigJson.putByPath(ID_BUCKET_KEY_PATH, Collections.emptyList());// 移除所有可用ID桶
            sysConfig.setContent(sysConfigJson.toJSONString(0));
            // todo 修改extra里的invalidIDBuckets
            boolean updated = sysDocRepo.getBaseMapper().updateContentById(sysConfig) == 1;// 乐观锁
            // 存缓存(存keys与初始化序号) & 解析并收集返回Map
            if (updated) {
                HashMap<Object, Object> resMap = new HashMap<>(target.size());
                target.forEach(e1 -> resMap.putAll((JSONObject) e1));
                redisTemplate.opsForHash().putAll(VALID_ID_BUCKETS, resMap);
                log.info("缓存ID桶重建completed");
                return resMap;
            }
            return Collections.emptyMap();
        }
    }

    public S3Operator getFileOperator() {
        if (s3Operator == null) {
            s3Operator = new S3Operator();
            s3Operator.refreshClient();
        }

        return s3Operator;
    }

    public String getEnv() {
        return SpringUtil.getApplicationContext().getEnvironment().getActiveProfiles()[0];
    }
}
