package com.wichelns.devaproject.app.module.ranking.service;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.app.module.user.entity.DeveloperUser;

import java.util.List;

/**
* @Author: 作者
* @Description: 描述
* @Date: 创建于14:44 2024-03-05
**/

public interface RankingService {
    /***
     * 获得开发者排行榜
     * @return List<DeveloperUser>
     */
    List<DeveloperUser> getUserRanking();

    /***
     * 获得团队排行榜
     * @return List<Team>
     */
    List<Team> getTeamRanking();

    /***
     *修改团队排行榜分数
     */
    void updateTeamRanking (Integer id);
    /***
     *修改个人排行榜分数
     */
    void updateUserRanking ( Long id);

}

