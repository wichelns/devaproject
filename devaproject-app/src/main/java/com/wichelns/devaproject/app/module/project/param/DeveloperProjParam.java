package com.wichelns.devaproject.app.module.project.param;

import com.wichelns.devaproject.common.model.BaseParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author zhijia
 * @date 2024/2/21 0:12
 */
@Data
@ApiModel(description = "开发者项目Param")
@EqualsAndHashCode(callSuper = false)
public class DeveloperProjParam extends BaseParam {
	@NotNull(groups = {add.class}, message = "项目ID不能为空")
	@ApiModelProperty(value = "项目ID")
	private Integer projectId;
	@NotNull(groups = team.class, message = "团队ID不能为空")
	@ApiModelProperty(value = "团队ID")
	private Integer teamId;
	@Length(groups = {add.class, updateInfo.class}, max = 100, message = "参选介绍至多100字符")
	@ApiModelProperty(value = "参选介绍")
	private String joinIntro;
	@NotNull(groups = updateInfo.class, message = "开发者项目ID不能为空")
	@ApiModelProperty(value = "开发者项目ID")
	private Long developerProjId;
	@ApiModelProperty(value = "开发者项目类型")
	private Integer developerProjType;
	
	public @interface team {}
	
}
