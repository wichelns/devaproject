package com.wichelns.devaproject.app.module.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author zhijia
 * @date 2024/2/16 1:02
 */
@Data
@NoArgsConstructor
@ApiModel(description = "用户DTO")
public class UserDTO {

	@ApiModelProperty(value = "用户类型")
	private Integer userType;

	@ApiModelProperty(value = "用户Id")
	private Long userId;
	@ApiModelProperty(value = "用户名")
	private String username;
	@ApiModelProperty(value = "头像")
	private String avatar;
	@ApiModelProperty(value = "电话")
	private String phone;
	@ApiModelProperty(value = "密码")
	private String password;
	@ApiModelProperty(value = "状态 1-正常 99-冻结")
	private Integer status;
	@ApiModelProperty(value = "微信/QQ联系号")
	private String contact;
	@ApiModelProperty(value = "个性签名/简介")
	private String profile;
	@ApiModelProperty(value = "接取项目数")
	private Integer recvProjectCount;
	@ApiModelProperty(value = "交付项目数")
	private Integer deliverProjectCount;
	@ApiModelProperty(value = "星星数", notes = "交付项目五星评价")
	private Integer stars;
	@ApiModelProperty(value = "上次登录时间")
	private Date lastLoginTime;
	@ApiModelProperty(value = "上次登录IP")
	private String lastLoginIp;
}
