package com.wichelns.devaproject.app.mq.consumer.redis.simple;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.wichelns.devaproject.app.module.team.dto.MemberDTO;
import com.wichelns.devaproject.app.mq.consumer.handler.TeamProjectHandler;
import com.wichelns.devaproject.common.annotation.Topic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static com.wichelns.devaproject.app.constant.MQConstant.KEY_MEMBER_OUT;
import static com.wichelns.devaproject.app.constant.MQConstant.MQ_TYPE_PUBSUB;

/**
 * 队员退出团队监听
 *
 * @author zhijia
 * @date 2024/2/23 11:53
 */
@Topic(KEY_MEMBER_OUT)
@Slf4j
@Component
@ConditionalOnProperty(value = "mq-type", havingValue = MQ_TYPE_PUBSUB)
public class MemberOutListener implements MessageListener {
	@Resource
	private TeamProjectHandler teamProjectHandler;
	@Resource
	private TaskExecutor taskExecutor;

	@Override
	public void onMessage(Message message, byte[] pattern) {
		String jsonStr = JSONObject.parseObject(new String(message.getBody()), String.class);
		MemberDTO memberDTO = JSONUtil.toBean(jsonStr, MemberDTO.class);

		// 异步处理，降低消息堆积达到阈值导致的pubsub强制下线概率
		taskExecutor.execute(() -> teamProjectHandler.handleMemberOut(memberDTO));
	}

}
