package com.wichelns.devaproject.app.module.team.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wichelns.devaproject.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhijia
 * @date 2024/2/21 20:18
 */
@ApiModel(value = "团队")
@TableName(value = "team", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
public class Team extends BaseEntity {
	// ID、leaderID、头像、团队名称、简介、当前人数、最大人数、信誉墙（接取项目数、完成项目数、得星率）
	@TableId(type = IdType.AUTO)
	@ApiModelProperty(value = "团队ID")
	private Integer teamId;
	@ApiModelProperty(value = "队长ID", notes = "该ID是开发者ID，不是队员ID")
	private Long leaderId;
	@ApiModelProperty(value = "队长名称")
	private String leaderName;
	@ApiModelProperty(value = "头像")
	private String avatar;
	@ApiModelProperty(value = "团名名称")
	private String teamName;
	@ApiModelProperty(value = "简介")
	private String profile;
	@ApiModelProperty(value = "当前人数")
	private Integer curMemberCount;
	@ApiModelProperty(value = "最大人数")
	private Integer maxMemberCount;
	@ApiModelProperty(value = "接取项目数")
	private Integer recvProjectCount;
	@ApiModelProperty(value = "交付项目数")
	private Integer deliverProjectCount;
	@ApiModelProperty(value = "星星数", notes = "交付项目五星评价")
	private Integer stars;

}
