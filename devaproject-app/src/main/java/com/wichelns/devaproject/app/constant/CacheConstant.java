package com.wichelns.devaproject.app.constant;

import com.wichelns.devaproject.app.core.context.SysUserContext;

/**
 * 缓存相关常量
 *
 * @author zhijia
 * @date 2024/2/16 20:58
 */
public interface CacheConstant {
	/**
	 * LOGIN_USER:{@code userType}:{@code userId}
	 */
	String LOGIN_USER_TEMPLATE = "LOGIN_USER:{}:{}";
	String TOKEN_TIMEOUT = "login.tokenTimeout";
	String JWT_SECRET = "login.jwtSecret";
	String SYSTEM_CONFIG_CONTEXT = "SYSTEM_CONFIG";
	String BUSINESS_CONFIG_CONTEXT = "BUSINESS_CONFIG";
	String VALID_ID_BUCKETS = "VALID_ID_BUCKET_KEYS";

	/**
	 * 格式 broadcast:{@code announceId}:readmark:{@code bucketLeadingNum}<br/>
	 * {@code announceId} 是群发通知的ID，{@code bucketLeadingNum} 是区分不同用户ID前缀的，
	 * 详见{@link SysUserContext#nextUserId()}
	 */
	String BROADCAST_READ_MARK_TEMPLATE = "broadcast:{}:readmark:{}";

	// 分布式锁key模板

	/**
	 * 指定项目下，开发者项目的状态变化
	 */
	String DEV_PROJECTS_STATUS_CHANGE = "proj:{}:devprojs:status";
	String PROJ_TEAM_JOIN = "projjoin:team:{}";
}
