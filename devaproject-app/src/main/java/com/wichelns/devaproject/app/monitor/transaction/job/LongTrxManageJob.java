package com.wichelns.devaproject.app.monitor.transaction.job;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.wichelns.devaproject.app.monitor.transaction.dto.TransactionDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 长事务监控job
 *
 * @author zhijia
 * @date 2023/9/7 22:28
 */
@Slf4j
@Configuration
@EnableScheduling
public class LongTrxManageJob {
    public static final Map<String, TransactionDTO> trxMap = new ConcurrentHashMap<>(100);
    /**
     * 事务超时时间，须<b>大于 0</b>
     */
    @Value("${myconfig.db.trx.timeout}")
    private int trxTimeoutSecond;

    @Scheduled(fixedRate=20 * 1000) // 每20秒执行一次
    public void run() {
        long timeoutMillis = trxTimeoutSecond * 1000L;

        long currentTimeMillis = System.currentTimeMillis(); // 暂时允许小的时间误差
        List<String> toRemoveTrxIdList = new LinkedList<>();
        trxMap.values().forEach(trx -> {
            long trxEndTimeMillis = trx.getTrxEndTimeMillis();
            long trxBeginTimeMillis = trx.getTrxBeginTimeMillis();
            long duration = trxEndTimeMillis - trxBeginTimeMillis;

            if (duration > 0L && duration < timeoutMillis) {
                toRemoveTrxIdList.add(trx.getConnectionId());
            } else if (duration >= timeoutMillis // 事务超时结束
                    || duration < 0L && currentTimeMillis - trxBeginTimeMillis >= timeoutMillis) { // 事务进行中，已超时
                // 长事务告警
                doLongTrxAlert(trx);
                toRemoveTrxIdList.add(trx.getConnectionId());
            }
        });

        // 移除正常结束事务与已告警的
        toRemoveTrxIdList.forEach(trxMap::remove);
    }

    private void doLongTrxAlert(TransactionDTO trx) {
        log.warn("事务超时，connectionId={}", trx.getConnectionId());
        log.warn("开始时间:{}", DateUtil.format(new Date(trx.getTrxBeginTimeMillis()), DatePattern.NORM_DATETIME_FORMAT));
        log.warn("结束时间:{}", trx.getTrxEndTimeMillis() == 0L ? 0 : DateUtil.format(new Date(trx.getTrxEndTimeMillis()), DatePattern.NORM_DATETIME_FORMAT));
        log.warn("执行SQL:{}", "\n" + StrUtil.join("\n", trx.getSqlList()));
        log.warn("调用栈:" + "\n" + trx.getStackTraceStr());
        // todo P1告警
    }

}
