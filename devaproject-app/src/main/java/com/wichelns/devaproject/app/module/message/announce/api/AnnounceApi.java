package com.wichelns.devaproject.app.module.message.announce.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wichelns.devaproject.app.module.message.announce.service.AnnounceService;
import com.wichelns.devaproject.app.module.message.announce.vo.AnnounceVO;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/16 0:07
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/announce")
@Api(value = "通知API", tags = "Web - AnnounceApi")
public class AnnounceApi {
	@Resource
	private AnnounceService announceService;

	@GetMapping(value = "/pullAnnounce")
	@ApiOperation(httpMethod = "GET", value = "拉取通知(运营通知与系统通知)")
	public Wrapper<IPage<AnnounceVO>> pullAnnounce(@ApiParam(value = "页码") @RequestParam(value = "pageNo") Integer pageNo,
												   @ApiParam(value = "页大小") @RequestParam(value = "pageSize") Integer pageSize) {
		return WrapMapper.ok(announceService.pullAnnounce(pageNo, pageSize));
	}

	@GetMapping(value = "/pullPublic")
	@ApiOperation(httpMethod = "GET", value = "拉取公告板")
	public Wrapper<IPage<AnnounceVO>> pullPublic(@ApiParam(value = "页码") @RequestParam(value = "pageNo") Integer pageNo,
												 @ApiParam(value = "页大小") @RequestParam(value = "pageSize") Integer pageSize) {
		return WrapMapper.ok(announceService.pullPublic(pageNo, pageSize));
	}

	@PostMapping(value = "/readOne")
	@ApiOperation(httpMethod = "POST", value = "读取一条通知")
	public Wrapper<?> readOne(@ApiParam(value = "通知ID") @RequestParam(value = "announceId") Integer announceId) {
		announceService.readOne(announceId);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/readAll")
	@ApiOperation(httpMethod = "POST", value = "一键已读")
	public Wrapper<?> readAll() {
		announceService.readAll();
		return WrapMapper.ok();
	}

}
