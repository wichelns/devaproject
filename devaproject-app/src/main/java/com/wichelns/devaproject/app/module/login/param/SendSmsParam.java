package com.wichelns.devaproject.app.module.login.param;

import com.wichelns.devaproject.common.model.BaseParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @author yoo
 */
@Data
@ApiModel(description = "发送短信Vo")
@EqualsAndHashCode(callSuper = false)
public class SendSmsParam extends BaseParam {
    @NotNull(message = "手机号不能为空", groups = {sendSms.class})
    @ApiModelProperty(value = "手机号不能为空")
    private String phone;
//    @NotNull(message = "ticket不能为空", groups = {sendSms.class})
//    @ApiModelProperty(value = "登陆邮箱")
//    private String ticket;
//    @NotNull(message = "随机串不能为空", groups = { sendSms.class})
//    @ApiModelProperty(value = "验证码")
//    private String randstr;

    /**
     * 参数校验分组：登陆
     */
    public @interface sendSms {
    }
}

