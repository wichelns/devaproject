package com.wichelns.devaproject.app.module.message.request.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wichelns.devaproject.app.module.message.request.param.ReqParam;
import com.wichelns.devaproject.app.module.message.request.vo.AbstractReqVO;

/**
 * @author zhijia
 * @date 2024/2/29 20:40
 */
public interface RequestService {
	/**
	 * 发送请求
	 */
	void sendReq(ReqParam param);

	/**
	 * 查询请求列表
	 */
	IPage<AbstractReqVO> pageReq(int op, int isSender, int pageNo, int pageSize);

	/**
	 * 确认请求
	 */
	void confirmReq(long reqId);

	/**
	 * 拒绝请求
	 */
	void rejectReq(long reqId);


}
