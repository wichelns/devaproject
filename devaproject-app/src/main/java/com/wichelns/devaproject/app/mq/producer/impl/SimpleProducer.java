package com.wichelns.devaproject.app.mq.producer.impl;

import cn.hutool.json.JSONUtil;
import com.wichelns.devaproject.app.constant.MQConstant;
import com.wichelns.devaproject.app.mq.dto.MessageDTO;
import com.wichelns.devaproject.app.mq.producer.Producer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 基于redis<b>发布订阅</b>模式的mq生产者实现
 *
 * @author zhijia
 * @date 2024/2/11 15:43
 */
@Component
@ConditionalOnProperty(value = "mq-type", havingValue = MQConstant.MQ_TYPE_PUBSUB)
public class SimpleProducer implements Producer {
	@Resource
	private RedisTemplate<String, Object> redisTemplate;

	@Override
	public void produce(MessageDTO messageDto) {
		redisTemplate.convertAndSend(messageDto.getControlHeader().getRoutingKey(), JSONUtil.toJsonStr(messageDto.getContent()));
	}
}
