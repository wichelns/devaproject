package com.wichelns.devaproject.app.module.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhijia
 * @date 2024/2/14 11:20
 */
@Getter
@AllArgsConstructor
public enum SysDocEnum {
	USER_PROTOCOL(10, "用户协议", 1),
	PRIVACY_PROTOCOL(20, "隐私协议", 1),
	REQUIREMENT_USER_MANUAL(30, "需求用户手册", 1),
	DEVELOPER_USER_MANUAL(40, "开发者手册", 1),
	BUSINESS_CONFIG(50, "业务配置", 0),
	SYSTEM_CONFIG(60, "系统配置", 0);

	private final Integer code;
	private final String desc;
	private final Integer isPublic;// 1-公开 0-非公开

}
