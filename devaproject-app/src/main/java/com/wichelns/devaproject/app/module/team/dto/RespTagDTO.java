package com.wichelns.devaproject.app.module.team.dto;

import lombok.Data;

@Data
public class RespTagDTO {
	private Integer tagId;
	private String tagName;
}