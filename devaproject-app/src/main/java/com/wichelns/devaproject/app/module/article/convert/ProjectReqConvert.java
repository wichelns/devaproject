package com.wichelns.devaproject.app.module.article.convert;

import com.wichelns.devaproject.app.module.article.entity.ProjectRequirement;
import com.wichelns.devaproject.app.module.article.param.ProjectReqParam;
import com.wichelns.devaproject.common.model.BaseConvert;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author 淡漠
 * @version 1.0
 */
@Mapper
public interface ProjectReqConvert extends BaseConvert {
    //返回该接口的一个实例类
    ProjectReqConvert INSTANCE = Mappers.getMapper(ProjectReqConvert.class);

    //dto转化为实体类
    @Mapping(source = "tagList", target = "tagList", qualifiedByName = "listToStr")
    ProjectRequirement dto2ProjectReq(ProjectReqParam projectReqParam);

    @Named("listToStr")
    default String listToStr(List<String> tagList) {
        return tagList != null ? tagList.toString() : "[]";
    }

}
