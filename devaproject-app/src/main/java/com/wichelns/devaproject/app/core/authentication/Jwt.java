package com.wichelns.devaproject.app.core.authentication;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.wichelns.devaproject.app.core.context.SysUserContext;
import com.wichelns.devaproject.app.core.context.SystemContext;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;

import java.util.Collections;
import java.util.Map;

import static com.wichelns.devaproject.app.constant.CacheConstant.JWT_SECRET;
import static com.wichelns.devaproject.app.core.authentication.JwtPayloadConstant.CLAIM_USERID;
import static com.wichelns.devaproject.app.core.authentication.JwtPayloadConstant.CLAIM_USERTYPE;

/**
 * Json Web Token服务：签名、验证、解析
 */
@Slf4j
public class Jwt {

    /**
     * 生成token
     *
     * @param userId 用户id
     * @param userType 用户类型
     * @return token
     */
    public static String sign(Long userId, Integer userType) {
        try {
            String secret = SystemContext.holder().opsForSystem().getValue(JWT_SECRET);
            Algorithm algorithm = Algorithm.HMAC256(secret);

            return JWT.create()
                    .withClaim(CLAIM_USERID, userId)
                    .withClaim(CLAIM_USERTYPE, userType)
                    .sign(algorithm);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 校验token是否正确
     *
     * @return 是否正确
     */
    public static boolean verify(String token) {
        try {
            Map<String, Claim> claimMap = parseToken(token);
            Long userId = claimMap.get(CLAIM_USERID).asLong();
            Integer userType = claimMap.get(CLAIM_USERTYPE).asInt();

            String secret = SystemContext.holder().opsForSystem().getValue(JWT_SECRET);

            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withClaim(CLAIM_USERID, userId)
                    .withClaim(CLAIM_USERTYPE, userType)
                    .build();
            verifier.verify(token);
            return SysUserContext.holder().tokenNotExpired(userId, userType);
        } catch (Exception e) {
            return false;
        }
    }

    public static Map<String, Claim> parseToken(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaims();
        } catch (JWTDecodeException e) {
            log.error("msg:{}", e.getMessage());
            return Collections.emptyMap();
        }
    }

    public static Long getLoginUserId() {
        String token = (String) SecurityUtils.getSubject().getPrincipal();
        Long userId = parseToken(token).get(CLAIM_USERID).asLong();
        if (userId == null) {
            throw new BusinessException(ErrorCodeEnum.UAC10010028);
        }
        return userId;
    }

    public static Integer getLoginUserType() {
        String token = (String) SecurityUtils.getSubject().getPrincipal();
        Integer userType = parseToken(token).get(CLAIM_USERTYPE).asInt();
        if (userType == null) {
            throw new BusinessException(ErrorCodeEnum.UAC10010028);
        }
        return userType;
    }

}
