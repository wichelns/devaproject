package com.wichelns.devaproject.app.module.message.request.enums;

import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhijia
 * @date 2024/2/28 12:12
 */
@Getter
@AllArgsConstructor
public enum OperationEnum {
	INVITATION_JOIN_TEAM(1, "入队邀请"),
	REQUEST_JOIN_TEAM(5, "入队申请"),
	REQUEST_MAKE_FRIENDS(10, "好友申请"),
	;

	private final Integer code;
	private final String desc;

	public static OperationEnum getByCode(Integer code) {
		for (OperationEnum operation : OperationEnum.values()) {
			if (operation.getCode().equals(code)) {
				return operation;
			}
		}

		throw new BusinessException("操作非法");
	}

}
