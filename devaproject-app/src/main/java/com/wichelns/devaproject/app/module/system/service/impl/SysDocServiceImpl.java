package com.wichelns.devaproject.app.module.system.service.impl;

import com.wichelns.devaproject.app.dao.system.repo.SysDocRepo;
import com.wichelns.devaproject.app.module.system.entity.SysDoc;
import com.wichelns.devaproject.app.module.system.service.SysDocService;
import com.wichelns.devaproject.common.enums.BaseStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/14 15:30
 */
@Service
public class SysDocServiceImpl implements SysDocService {
	@Resource
	private SysDocRepo sysDocRepo;

	@Override
	public SysDoc getSysDoc(Integer docType) {
		return sysDocRepo.lambdaQuery().eq(SysDoc::getType, docType).eq(SysDoc::getStatus, BaseStatus.Y.getCode()).one();
	}

}
