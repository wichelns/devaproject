package com.wichelns.devaproject.app.core.file.param;

import com.wichelns.devaproject.common.model.BaseParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "上传Param")
@EqualsAndHashCode(callSuper = false)
public class FileUploadParam extends BaseParam {
    @NotNull(message = "文件名不能为空", groups = {add.class, preUrl.class})
    @ApiModelProperty(value = "fileName")
    private String fileName;
    @NotNull(message = "文件不能为空", groups = {add.class})
    @ApiModelProperty(value = "source")
    private String source;

    public @interface preUrl {
    }
}
