package com.wichelns.devaproject.app.dao.system.repo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.system.mapper.SysDocMapper;
import com.wichelns.devaproject.app.module.system.entity.SysDoc;
import org.springframework.stereotype.Repository;

/**
 * @author zhijia
 * @date 2024/2/14 10:59
 */
@Repository
public class SysDocRepo extends ServiceImpl<SysDocMapper, SysDoc> implements IService<SysDoc>  {
}
