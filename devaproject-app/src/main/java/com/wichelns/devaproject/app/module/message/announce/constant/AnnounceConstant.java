package com.wichelns.devaproject.app.module.message.announce.constant;

/**
 * @author zhijia
 * @date 2024/3/4 16:43
 */
public interface AnnounceConstant {
	/**
	 * 默认已读未读有效天数，超过则默认已读
	 */
	int DEFAULT_MAINTAIN_DAYS = 30;

}
