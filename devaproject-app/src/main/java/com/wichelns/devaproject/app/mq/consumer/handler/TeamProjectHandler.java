package com.wichelns.devaproject.app.mq.consumer.handler;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wichelns.devaproject.app.dao.project.repo.PersonalProjectRepo;
import com.wichelns.devaproject.app.dao.project.repo.TeamProjectRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamMemberRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamRepo;
import com.wichelns.devaproject.app.dao.user.repo.CreditRecordRepo;
import com.wichelns.devaproject.app.dao.user.repo.DeveloperUserRepo;
import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.module.project.dto.TeamProjExtraDTO;
import com.wichelns.devaproject.app.module.project.entity.PersonalProject;
import com.wichelns.devaproject.app.module.project.entity.TeamProject;
import com.wichelns.devaproject.app.module.ranking.service.RankingService;
import com.wichelns.devaproject.app.module.team.dto.MemberDTO;
import com.wichelns.devaproject.app.module.team.dto.TeamDTO;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.app.module.team.entity.TeamMember;
import com.wichelns.devaproject.app.module.user.entity.CreditRecord;
import com.wichelns.devaproject.app.module.user.enums.CreditItemEnum;
import com.wichelns.devaproject.common.annotation.ErrorLog;
import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum.*;
import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjTypeEnum.TEAM_PROJECT;
import static com.wichelns.devaproject.app.module.project.enums.DeveloperTypeEnum.DEVELOPER_TEAM;
import static com.wichelns.devaproject.app.module.project.enums.DeveloperTypeEnum.DEVELOPER_USER;
import static com.wichelns.devaproject.app.module.user.enums.CreditItemEnum.RECEIVE_COUNT;
import static com.wichelns.devaproject.app.module.user.enums.CreditItemEnum.STARS_COUNT;

/**
 * @author zhijia
 * @date 2024/2/23 11:58
 */
@Slf4j
@Component
@Transactional
public class TeamProjectHandler {
    @Resource
    private PersonalProjectRepo personalProjectRepo;
    @Resource
    private TeamProjectRepo teamProjectRepo;
    @Resource
    private TeamRepo teamRepo;
    @Resource
    private TeamMemberRepo teamMemberRepo;
    @Resource
    private DeveloperUserRepo developerUserRepo;
    @Resource
    private CreditRecordRepo creditRecordRepo;
    @Resource
    private RankingService rankingService;

    /**
     * 处理团队加入项目候选
     */
    @ErrorLog
    public void handleProjTeamJoin(DeveloperProjDTO developerProjDTO) {
        // 不开事务
        Team team = teamRepo.getById(developerProjDTO.getTeamId());
        List<TeamMember> memberList = teamMemberRepo.lambdaQuery().eq(TeamMember::getTeamId, team.getTeamId()).select(TeamMember::getDevUserId).list();
        List<Long> devUserIdList = memberList.stream().map(TeamMember::getDevUserId).collect(Collectors.toList());

        // 1.更新extra
        TeamProjExtraDTO extraDTO = new TeamProjExtraDTO();
        extraDTO.setAvatar(team.getAvatar());
        extraDTO.setTeamName(team.getTeamName());
        extraDTO.setDevUserIdList(devUserIdList);
        teamProjectRepo.lambdaUpdate().set(TeamProject::getExtra, JSONUtil.toJsonStr(extraDTO))
                .eq(TeamProject::getTeamProjId, developerProjDTO.getTeamProjId()).update();

        // 2.TeamMember的PersonalProject
        List<PersonalProject> personalProjectList = devUserIdList.stream().map(devUserId -> PersonalProject.builder()
                .projectId(developerProjDTO.getProjectId())
                .devUserId(devUserId)
                .teamProjId(developerProjDTO.getTeamProjId())
                .status(APPOINT_AWAITING.getCode()).build()).collect(Collectors.toList());
        personalProjectRepo.saveBatch(personalProjectList);
    }

    private List<CreditRecord> buildPersonCRList(CreditItemEnum creditItem, Integer delta, List<Long> devUserIdList, DeveloperProjDTO developerProjDTO) {
        // 个人
        return devUserIdList.stream().map(devUserId -> CreditRecord.builder()
                .creditItem(creditItem.getCode())
                .delta(delta)
                .projectId(developerProjDTO.getProjectId())
                .sourceId(developerProjDTO.getDevProjId())
                .sourceType(TEAM_PROJECT.getCode())
                .targetId(devUserId)
                .targetType(DEVELOPER_USER.getCode())
                .build()).collect(Collectors.toList());
    }

    private CreditRecord buildTeamCR(CreditItemEnum creditItem, Integer delta, DeveloperProjDTO developerProjDTO) {
        // 团队
        return CreditRecord.builder()
                .creditItem(creditItem.getCode())
                .delta(delta)
                .projectId(developerProjDTO.getProjectId())
                .sourceId(developerProjDTO.getDevProjId())
                .sourceType(TEAM_PROJECT.getCode())
                .targetId(Long.valueOf(developerProjDTO.getTeamId()))
                .targetType(DEVELOPER_TEAM.getCode())
                .build();
    }

    private void handle(DeveloperProjDTO developerProjDTO, CreditItemEnum creditItemEnum, Integer delta, BiConsumer<List<Long>, Integer> persConsumer, BiConsumer<Integer, Integer> teamConsumer) {
        TeamProjExtraDTO teamProjExtraDTO = JSONUtil.toBean(developerProjDTO.getExtra(), TeamProjExtraDTO.class);

        List<CreditRecord> personCRList = buildPersonCRList(creditItemEnum, delta, teamProjExtraDTO.getDevUserIdList(), developerProjDTO);

        List<CreditRecord> need2OpPersonCR = creditRecordRepo.checkBeforeSaveBatch(personCRList);
        if (CollUtil.isNotEmpty(need2OpPersonCR)) {
            List<Long> targetDevUserIdList = need2OpPersonCR.stream().map(CreditRecord::getTargetId).collect(Collectors.toList());
            persConsumer.accept(targetDevUserIdList, delta);
        }

        List<CreditRecord> need2OpTeamCR = creditRecordRepo.checkBeforeSaveBatch(Collections.singletonList(buildTeamCR(creditItemEnum, delta, developerProjDTO)));
        if (CollUtil.isNotEmpty(need2OpTeamCR)) {
            teamConsumer.accept(developerProjDTO.getTeamId(), delta);
        }
    }

    /**
     * 处理确认开发
     */
    @ErrorLog
    public void handleConfirmDev(DeveloperProjDTO developerProjDTO) {
        BiConsumer<List<Long>, Integer> persConsumer = (devUserIdList, delta) -> developerUserRepo.getBaseMapper().incRecvCountById(devUserIdList);
        BiConsumer<Integer, Integer> teamConsumer = (teamId, delta) -> teamRepo.getBaseMapper().incRecvCountById(teamId);
        handle(developerProjDTO, RECEIVE_COUNT, 1, persConsumer, teamConsumer);

    }

    /**
     * 处理确认交付
     */
    @ErrorLog
    public void handleConfirmDelivery(DeveloperProjDTO developerProjDTO) {
        BiConsumer<List<Long>, Integer> persConsumer = (devUserIdList, delta) -> developerUserRepo.getBaseMapper().incDeliverCountById(devUserIdList);
        BiConsumer<Integer, Integer> teamConsumer = (teamId, delta) -> teamRepo.getBaseMapper().incDeliverCountById(teamId);
        handle(developerProjDTO, RECEIVE_COUNT, 1, persConsumer, teamConsumer);
        rankingService.updateTeamRanking(developerProjDTO.getTeamId());
    }

    /**
     * 处理项目交付评价
     */
    @ErrorLog
    public void handleEvaluation(DeveloperProjDTO developerProjDTO) {
        BiConsumer<List<Long>, Integer> persConsumer = (devUserIdList, delta) -> developerUserRepo.getBaseMapper().incStarsById(devUserIdList, delta);
        BiConsumer<Integer, Integer> teamConsumer = (teamId, delta) -> teamRepo.getBaseMapper().incStarsById(teamId, delta);
        handle(developerProjDTO, STARS_COUNT, developerProjDTO.getStars(), persConsumer, teamConsumer);
        rankingService.updateTeamRanking(developerProjDTO.getTeamId());
    }

    /**
     * 处理队长转让
     */
    @ErrorLog
    public void handleLeaderChange(TeamDTO teamDTO) {
        Long newLeaderId = teamDTO.getToLeaderId();
        List<TeamProject> teamProjectList = teamProjectRepo.lambdaQuery().eq(TeamProject::getTeamId, teamDTO.getTeamId())
                .notIn(TeamProject::getStatus, DELIVERED.getCode(), QUITED.getCode(), DISMISSED.getCode())
                .select(TeamProject::getTeamProjId, TeamProject::getProjectId, TeamProject::getExtra, TeamProject::getStatus, TeamProject::getVersion).list();
        if (teamProjectList.isEmpty()) {
            return;
        }

        teamProjectList.forEach(teamProject -> {
            teamProject.setLeaderId(newLeaderId);
            // 解析extra
            TeamProjExtraDTO teamProjExtraDTO = JSONUtil.toBean(teamProject.getExtra(), TeamProjExtraDTO.class);
            List<Long> devUserIdList = teamProjExtraDTO.getDevUserIdList();

            if (devUserIdList.stream().noneMatch(devUserId -> devUserId.equals(newLeaderId))) {// 表明是移进来的
                devUserIdList.add(newLeaderId);
                teamProject.setExtra(JSONUtil.toJsonStr(teamProjExtraDTO));
                // 1.新增PersonalProject
                PersonalProject personalProject = PersonalProject.builder()
                        .projectId(teamProject.getProjectId())
                        .devUserId(newLeaderId)
                        .teamProjId(teamProject.getTeamProjId())
                        .status(APPOINT_AWAITING.getCode()).build();
                personalProjectRepo.save(personalProject);
                // 2.更新信誉墙记录-接取项目数
                if (DEVELOPING.getCode().equals(teamProject.getStatus()) || DELIVERING.getCode().equals(teamProject.getStatus())) {
                    CreditRecord creditRecord = CreditRecord.builder()
                            .creditItem(RECEIVE_COUNT.getCode())
                            .delta(1)
                            .projectId(teamProject.getProjectId())
                            .sourceId(teamProject.getTeamProjId())
                            .sourceType(TEAM_PROJECT.getCode())
                            .targetId(newLeaderId)
                            .targetType(DEVELOPER_USER.getCode())
                            .build();
                    List<CreditRecord> res = creditRecordRepo.checkBeforeSaveBatch(Collections.singletonList(creditRecord));
                    if (CollUtil.isNotEmpty(res)) {
                        developerUserRepo.getBaseMapper().incRecvCountById(Collections.singletonList(newLeaderId));
                    }
                }
            } else {
                teamProject.setExtra(null);// 在此设置是为了避免不必要的update与冲突
            }
            // 3.更新teamproj
            int updated = teamProjectRepo.getBaseMapper().updateOptimistic(teamProject);
            if (updated == 0) {
                throw new BusinessException("并发更新TeamProject的extra，乐观锁方案失败");
            }
        });
    }

    /**
     * 处理关联的TeamProject的extra开发者信息
     */
    @ErrorLog
    public void handleMemberOut(MemberDTO memberDTO) {
        LambdaQueryWrapper<PersonalProject> qwForPP = Wrappers.<PersonalProject>lambdaQuery()
                .eq(PersonalProject::getDevUserId, memberDTO.getDevUserId())
                .isNotNull(PersonalProject::getTeamProjId)
                .select(PersonalProject::getTeamProjId);
        List<Long> teamProjIdList = personalProjectRepo.listObjs(qwForPP, o -> Long.valueOf(o.toString()));

        if (teamProjIdList.isEmpty()) {
            return;
        }

        // 从TeamProject的extra开发人员列表移除该成员
        Long devUserId = memberDTO.getDevUserId();
        LambdaQueryWrapper<TeamProject> qwForTP = Wrappers.<TeamProject>lambdaQuery().in(TeamProject::getTeamProjId, teamProjIdList)
                .eq(TeamProject::getTeamId, memberDTO.getTeamId())
                .notIn(TeamProject::getStatus, DELIVERED.getCode(), QUITED.getCode(), DISMISSED.getCode())
                .select(TeamProject::getTeamProjId, TeamProject::getExtra, TeamProject::getVersion);// 乐观锁

        teamProjectRepo.list(qwForTP).forEach(teamProject -> {

            TeamProjExtraDTO teamProjExtraDTO = JSONUtil.toBean(teamProject.getExtra(), TeamProjExtraDTO.class);
            if (teamProjExtraDTO.getDevUserIdList() != null && teamProjExtraDTO.getDevUserIdList().remove(devUserId)) {
                teamProject.setExtra(JSONUtil.toJsonStr(teamProjExtraDTO));

                int tries = 0;
                while (tries++ < 3) {
                    int updated = teamProjectRepo.getBaseMapper().updateOptimistic(teamProject);
                    if (updated == 1) {
                        break;
                    }
                    // 更新失败，重新获取
                    TeamProject newTeamProj = teamProjectRepo.getById(teamProject.getTeamProjId());
                    teamProjExtraDTO = JSONUtil.toBean(newTeamProj.getExtra(), TeamProjExtraDTO.class);
                    if (teamProjExtraDTO.getDevUserIdList() != null && teamProjExtraDTO.getDevUserIdList().remove(devUserId)) {
                        teamProject.setExtra(JSONUtil.toJsonStr(teamProjExtraDTO));
                        teamProject.setVersion(newTeamProj.getVersion());
                    } else {
                        log.warn("unexpected update:[{}]", newTeamProj);
                        break;
                    }
                }

                if (tries >= 3) {// 兜底方案：悲观锁
                    log.info("兜底方案-悲观锁, teamProjectId={}", teamProject.getTeamProjId());
                    TeamProject newTeamProj = teamProjectRepo.getBaseMapper().lockForUpdateById(teamProject.getTeamProjId());
                    teamProjExtraDTO = JSONUtil.toBean(newTeamProj.getExtra(), TeamProjExtraDTO.class);
                    if (teamProjExtraDTO.getDevUserIdList() != null && teamProjExtraDTO.getDevUserIdList().remove(devUserId)) {
                        teamProjectRepo.lambdaUpdate().set(TeamProject::getExtra, JSONUtil.toJsonStr(teamProjExtraDTO))
                                .set(TeamProject::getVersion, newTeamProj.getVersion() + 1)
                                .eq(TeamProject::getTeamProjId, teamProject.getTeamProjId()).update();
                    }
                }
            }
        });
    }

}
