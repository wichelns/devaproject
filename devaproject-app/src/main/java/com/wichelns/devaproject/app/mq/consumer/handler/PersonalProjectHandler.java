package com.wichelns.devaproject.app.mq.consumer.handler;

import com.wichelns.devaproject.app.dao.user.repo.CreditRecordRepo;
import com.wichelns.devaproject.app.dao.user.repo.DeveloperUserRepo;
import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.module.project.enums.DeveloperTypeEnum;
import com.wichelns.devaproject.app.module.ranking.service.RankingService;
import com.wichelns.devaproject.app.module.user.entity.CreditRecord;
import com.wichelns.devaproject.app.module.user.enums.CreditItemEnum;
import com.wichelns.devaproject.common.annotation.ErrorLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjTypeEnum.PERSONAL_PROJECT;
import static com.wichelns.devaproject.app.module.user.enums.CreditItemEnum.*;

/**
 * @author zhijia
 * @date 2024/2/23 11:58
 */
@Slf4j
@Transactional
@Component
public class PersonalProjectHandler {
	@Resource
	private DeveloperUserRepo developerUserRepo;
	@Resource
	private CreditRecordRepo creditRecordRepo;
	@Resource
	private RankingService rankingService;

	private CreditRecord buildCreditRecord(CreditItemEnum creditItem, Integer delta, DeveloperProjDTO developerProjDTO) {
		return CreditRecord.builder()
				.creditItem(creditItem.getCode())
				.delta(delta)
				.projectId(developerProjDTO.getProjectId())
				.sourceId(developerProjDTO.getDevProjId())
				.sourceType(PERSONAL_PROJECT.getCode())
				.targetId(developerProjDTO.getDevUserId())
				.targetType(DeveloperTypeEnum.DEVELOPER_USER.getCode())
				.build();
	}

	/**
	 * 处理确认开发
	 */
	@ErrorLog
	public void handleConfirmDev(DeveloperProjDTO developerProjDTO) {
		CreditRecord creditRecord = buildCreditRecord(RECEIVE_COUNT, 1, developerProjDTO);

		List<CreditRecord> need2Op = creditRecordRepo.checkBeforeSaveBatch(Collections.singletonList(creditRecord));
		if (need2Op.isEmpty()) {
			return;
		}
		developerUserRepo.getBaseMapper().incRecvCountById(Collections.singletonList(developerProjDTO.getDevUserId()));
	}

	/**
	 * 处理确认交付
	 */
	@ErrorLog
	public void handleConfirmDelivery(DeveloperProjDTO developerProjDTO) {
		CreditRecord creditRecord = buildCreditRecord(DELIVER_COUNT, 1, developerProjDTO);
		List<CreditRecord> need2Op = creditRecordRepo.checkBeforeSaveBatch(Collections.singletonList(creditRecord));
		if (need2Op.isEmpty()) {
			return;
		}
		developerUserRepo.getBaseMapper().incDeliverCountById(Collections.singletonList(developerProjDTO.getDevUserId()));
		rankingService.updateUserRanking(developerProjDTO.getDevUserId());
	}

	/**
	 * 处理项目交付评价
	 */
	@ErrorLog
	public void handleEvaluation(DeveloperProjDTO developerProjDTO) {
		CreditRecord creditRecord = buildCreditRecord(STARS_COUNT, developerProjDTO.getStars(), developerProjDTO);

		List<CreditRecord> need2Op = creditRecordRepo.checkBeforeSaveBatch(Collections.singletonList(creditRecord));
		if (need2Op.isEmpty()) {
			return;
		}
		developerUserRepo.getBaseMapper().incStarsById(Collections.singletonList(developerProjDTO.getDevUserId()), developerProjDTO.getStars());
		rankingService.updateUserRanking(developerProjDTO.getDevUserId());
	}

}
