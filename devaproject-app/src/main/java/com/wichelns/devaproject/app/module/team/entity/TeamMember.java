package com.wichelns.devaproject.app.module.team.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wichelns.devaproject.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhijia
 * @date 2024/2/21 20:18
 */
@ApiModel(value = "团队成语")
@TableName(value = "team_member", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
public class TeamMember extends BaseEntity {
	// ID、开发者ID、团队ID、团队身份（队长、普通成员）、职责标签ID、备注
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "队员ID")
	private Integer memberId;
	@ApiModelProperty(value = "开发者ID")
	private Long devUserId;
	@ApiModelProperty(value = "团队ID")
	private Integer teamId;
	@ApiModelProperty(value = "团队身份")
	private Integer identity;
	@ApiModelProperty(value = "职责标签ID", notes = "格式：组ID-组内标签ID，如1-1, 1-2, 2-1")
	private String respTagId;
	@ApiModelProperty(value = "备注")
	private String remark;

}
