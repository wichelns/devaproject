package com.wichelns.devaproject.app.dao.user.repo;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.user.mapper.CreditRecordMapper;
import com.wichelns.devaproject.app.module.user.entity.CreditRecord;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhijia
 * @date 2024/2/20 23:59
 */
@Repository
public class CreditRecordRepo extends ServiceImpl<CreditRecordMapper, CreditRecord> implements IService<CreditRecord> {
	/**
	 * 过滤掉已经存在的CreditRecord，防止重复记录信誉墙<br/>
	 * 注：还是有概率发生重复，因为使用的不是一致性锁定读的排它锁。
	 *
	 * @param creditRecordList 注意需要相同projectId，且相同{@code CreditItem}的。
	 * @return 待操作的CreditRecord集合
	 */
	public List<CreditRecord> checkBeforeSaveBatch(List<CreditRecord> creditRecordList) {
		List<CreditRecord> already = lambdaQuery()
				.eq(CreditRecord::getProjectId, creditRecordList.get(0).getProjectId())
				.eq(CreditRecord::getCreditItem, creditRecordList.get(0).getCreditItem()).list();

		List<CreditRecord> toSaveList = creditRecordList.stream()
				.filter(creditRecord -> already
						.stream()
						.noneMatch(e -> e.getTargetId().equals(creditRecord.getTargetId())
								&& e.getTargetType().equals(creditRecord.getTargetType()))).collect(Collectors.toList());

		if (CollUtil.isNotEmpty(toSaveList)) {
			saveBatch(toSaveList);
		}
		return toSaveList;
	}

}
