package com.wichelns.devaproject.app.module.project.param;

import com.wichelns.devaproject.common.model.BaseParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

/**
 * @author zhijia
 * @date 2024/2/21 0:12
 */
@Data
@ApiModel(description = "需求用户与开发者项目交互Param")
@EqualsAndHashCode(callSuper = false)
public class ReqManageParam extends BaseParam {
	@NotNull(groups = updateInfo.class, message = "项目需求ID不能为空")
	@ApiModelProperty(value = "项目ID")
	private Integer projectId;
	@NotNull(groups = updateInfo.class, message = "开发者项目ID不能为空")
	@ApiModelProperty(value = "开发者项目ID")
	private Long devProjId;
	@NotNull(groups = updateInfo.class, message = "开发者项目类型不能为空")
	@ApiModelProperty(value = "开发者项目类型")
	private Integer devProjType;
	@NotNull(groups = evaluate.class, message = "评语不能为空")
	@Length(min = 5, max = 100, groups = evaluate.class, message = "结语字数5~100")
	@ApiModelProperty(value = "项目交付结语")
	private String comment;
	@NotNull(groups = evaluate.class, message = "星星数不能为空")
	@Range(min = 1, max = 5, groups = evaluate.class, message = "星星数只能1~5")
	@ApiModelProperty(value = "星星数")
	private Integer stars;
	@NotNull(groups = confirmFlag.class, message = "参数不能为空")
	@Range(min = 0, max = 1, groups = confirmFlag.class)
	@ApiModelProperty(value = "控制信息 1-确认 0-拒绝")
	private Integer confirm;

	public @interface confirmFlag{}
	public @interface evaluate{}

}
