package com.wichelns.devaproject.app.module.article.service;

import com.wichelns.devaproject.app.module.article.param.ProjectReqParam;

/**
 * @author 淡漠
 * @version 1.0
 */
public interface ProjectReqService {
     /**
      * 新增或修改
      */
     void saveOrUpdate(ProjectReqParam projectReqParam);

     /**
      * 发布/撤回已发布
      */
     void togglePublish(Integer projectId);
}
