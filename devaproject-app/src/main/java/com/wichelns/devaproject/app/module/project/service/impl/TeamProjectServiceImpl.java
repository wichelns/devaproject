package com.wichelns.devaproject.app.module.project.service.impl;

import cn.hutool.core.util.StrUtil;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.core.support.RedisUtil;
import com.wichelns.devaproject.app.dao.project.repo.TeamProjectRepo;
import com.wichelns.devaproject.app.module.project.convert.DeveloperProjConvert;
import com.wichelns.devaproject.app.module.project.entity.TeamProject;
import com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum;
import com.wichelns.devaproject.app.module.project.logic.AbstractDevProjLogic;
import com.wichelns.devaproject.app.module.project.param.DeveloperProjParam;
import com.wichelns.devaproject.app.module.project.service.DeveloperProjectService;
import com.wichelns.devaproject.app.mq.dto.MessageDTO;
import com.wichelns.devaproject.app.mq.producer.Producer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicReference;

import static com.wichelns.devaproject.app.constant.CacheConstant.PROJ_TEAM_JOIN;
import static com.wichelns.devaproject.app.constant.MQConstant.KEY_PROJECT_TEAM_JOIN;


/**
 * @author zhijia
 * @date 2024/2/21 0:02
 */
@Slf4j
@Service("team")
public class TeamProjectServiceImpl implements DeveloperProjectService {
	@Resource
	private TeamProjectRepo teamProjectRepo;
	@Resource
	private AbstractDevProjLogic teamProjLogic;
	@Resource
	private Producer producer;
	@Resource
	private RedisUtil redisUtil;

	@Override
	public void joinProject(DeveloperProjParam param) {
		Long devUserId = Jwt.getLoginUserId();
		String key = StrUtil.format(PROJ_TEAM_JOIN, param.getTeamId());

		redisUtil.trxWithinLock(key, () -> {
			// 校验能否参加
			teamProjLogic.checkCanJoin(param.getProjectId(), devUserId, param.getTeamId());

			TeamProject teamProject = TeamProject.builder()
					.projectId(param.getProjectId())
					.teamId(param.getTeamId())
					.leaderId(devUserId)
					.extra("{}")
					.joinIntro(param.getJoinIntro())
					.status(DeveloperProjStatusEnum.APPOINT_AWAITING.getCode()).build();
			teamProjectRepo.save(teamProject);

			// mq异步处理
			MessageDTO messageDto = new MessageDTO(KEY_PROJECT_TEAM_JOIN, DeveloperProjConvert.INSTANCE.domain2DTO(teamProject));
			producer.produce(messageDto);
		});
	}

	@Override
	public void editJoinIntro(DeveloperProjParam param) {
		// 权限校验
		teamProjLogic.checkCanManageDevProj(Jwt.getLoginUserId(), param.getDeveloperProjId());

		teamProjectRepo.lambdaUpdate().set(TeamProject::getJoinIntro, param.getJoinIntro())
				.eq(TeamProject::getTeamProjId, param.getDeveloperProjId()).update();
	}

	@Override
	public void confirmDevAppointment(DeveloperProjParam param) {
		teamProjLogic.confirmDev(Jwt.getLoginUserId(), param.getDeveloperProjId());
	}

	@Override
	public void deliver(DeveloperProjParam param) {
		teamProjLogic.deliver(Jwt.getLoginUserId(), param.getDeveloperProjId());
	}

	@Override
	public void quitProject(Long devProjId) {
		Long devUserId = Jwt.getLoginUserId();
		teamProjLogic.quitProject(devUserId, devProjId);
	}
}
