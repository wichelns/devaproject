package com.wichelns.devaproject.app.module.message.announce.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wichelns.devaproject.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhijia
 * @date 2024/3/4 9:53
 */
@ApiModel(value = "通知")
@TableName(value = "announce", autoResultMap = true)
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
public class Announce extends BaseEntity {
	// ID、标题、内容、跳转链接、通知类型（运营-个人接收、运营-广播、系统通知、公告板）、接收者ID、接收者类型、状态（已读、未读）
	@TableId(type = IdType.AUTO)
	@ApiModelProperty(value = "通知ID")
	private Integer announceId;
	@ApiModelProperty(value = "标题")
	private String title;
	@ApiModelProperty(value = "内容")
	private String content;
	@ApiModelProperty(value = "跳转链接")
	private String refLink;
	@ApiModelProperty(value = "通知类型")
	private Integer announceType;
	@ApiModelProperty(value = "接收者ID")
	private Long receiverId;
	@ApiModelProperty(value = "接收者类型")
	private Integer receiverType;
	@ApiModelProperty(value = "状态 1-已读 0-未读", notes = "只有目标是个人的时候才需要这个")
	private Integer status;
}
