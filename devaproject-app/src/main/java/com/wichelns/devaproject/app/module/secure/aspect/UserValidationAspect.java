package com.wichelns.devaproject.app.module.secure.aspect;

import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.module.user.enums.UserTypeEnum;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class UserValidationAspect {

    @Before("@annotation(com.wichelns.devaproject.app.module.secure.annotation.RequireUserValid)")
    //用于需求用户的校验
    public void validateReqUser(JoinPoint joinPoint) {
        if(!UserTypeEnum.REQUIREMENT_USER.getCode().equals(Jwt.getLoginUserType())){
            //抛出用户类型异常错误
            throw new BusinessException(ErrorCodeEnum.UAC10010029);
        }
    }

    @Before("@annotation(com.wichelns.devaproject.app.module.secure.annotation.DevelopUserValid)")
    //用于需求用户的校验
    public void validateDevUser(JoinPoint joinPoint) {
        if(!UserTypeEnum.DEVELOPER_USER.getCode().equals(Jwt.getLoginUserType())){
            //抛出用户类型异常错误
            throw new BusinessException(ErrorCodeEnum.UAC10010029);
        }
    }
}