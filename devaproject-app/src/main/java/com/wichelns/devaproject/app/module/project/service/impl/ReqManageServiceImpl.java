package com.wichelns.devaproject.app.module.project.service.impl;

import cn.hutool.core.util.StrUtil;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.core.support.RedisUtil;
import com.wichelns.devaproject.app.dao.article.repo.ProjectRequirementRepo;
import com.wichelns.devaproject.app.dao.project.repo.PersonalProjectRepo;
import com.wichelns.devaproject.app.dao.project.repo.TeamProjectRepo;
import com.wichelns.devaproject.app.module.article.entity.ProjectRequirement;
import com.wichelns.devaproject.app.module.article.enums.ProjectReqState;
import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.module.project.entity.PersonalProject;
import com.wichelns.devaproject.app.module.project.entity.TeamProject;
import com.wichelns.devaproject.app.module.project.logic.ReqManageLogic;
import com.wichelns.devaproject.app.module.project.param.ReqManageParam;
import com.wichelns.devaproject.app.module.project.service.ReqManageService;
import com.wichelns.devaproject.app.mq.dto.MessageDTO;
import com.wichelns.devaproject.app.mq.producer.Producer;
import com.wichelns.devaproject.common.enums.BaseStatus;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;

import static com.wichelns.devaproject.app.constant.CacheConstant.DEV_PROJECTS_STATUS_CHANGE;
import static com.wichelns.devaproject.app.constant.MQConstant.*;
import static com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum.*;

/**
 * @author zhijia
 * @date 2024/2/26 12:12
 */
@Service
public class ReqManageServiceImpl implements ReqManageService {
	@Resource
	private ReqManageLogic reqManageLogic;
	@Resource
	private ProjectRequirementRepo projectRequirementRepo;
	@Resource
	private PersonalProjectRepo personalProjectRepo;
	@Resource
	private TeamProjectRepo teamProjectRepo;
	@Resource
	private Producer producer;
	@Resource
	private RedisUtil redisUtil;


	@Override
	public void toggleAppoint(ReqManageParam param) {
		Long reqUserId = Jwt.getLoginUserId();
		Integer projectId = param.getProjectId();
		Long devProjId = param.getDevProjId();
		Integer devProjType = param.getDevProjType();
		// 1.权限校验
		reqManageLogic.checkCanManage(reqUserId, projectId, devProjId, devProjType);

		redisUtil.trxWithinLock(StrUtil.format(DEV_PROJECTS_STATUS_CHANGE, projectId), () -> {
			DeveloperProjDTO devProjDTO4Update = reqManageLogic.getDevProjDTO4Update(devProjId, devProjType);
			// 2.状态校验
			Integer count = projectRequirementRepo.lambdaQuery().eq(ProjectRequirement::getProjectId, projectId)
					.in(ProjectRequirement::getStatus, Arrays.asList(ProjectReqState.PUBLISHED.getCode(), ProjectReqState.DEV_APPOINT_CONFIRMING.getCode()))
					.count();
			if (count != 1) {
				throw new BusinessException(ErrorCodeEnum.PC10040005);
			}
			boolean appointAwaiting = APPOINT_AWAITING.getCode().equals(devProjDTO4Update.getStatus());
			if (!(appointAwaiting || DEV_APPOINT_CONFIRMING.getCode().equals(devProjDTO4Update.getStatus()))) {
				throw new BusinessException(ErrorCodeEnum.PC10040005);
			}

			// 3.更新逻辑
			if (appointAwaiting) {
				reqManageLogic.changeDevProjStatus(devProjId, devProjType, DEV_APPOINT_CONFIRMING);
				projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.DEV_APPOINT_CONFIRMING.getCode())
						.eq(ProjectRequirement::getProjectId, projectId).update();
			} else {
				reqManageLogic.changeDevProjStatus(devProjId, devProjType, APPOINT_AWAITING);
				// 被指认人数==0，置为已发布
				Integer personal = personalProjectRepo.lambdaQuery().eq(PersonalProject::getProjectId, projectId)
						.eq(PersonalProject::getStatus, DEV_APPOINT_CONFIRMING.getCode())
						.isNull(PersonalProject::getTeamProjId).count();
				Integer team = teamProjectRepo.lambdaQuery().eq(TeamProject::getProjectId, projectId)
						.eq(TeamProject::getStatus, DEV_APPOINT_CONFIRMING.getCode()).count();
				if (personal + team == 0) {
					projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.PUBLISHED.getCode())
							.eq(ProjectRequirement::getProjectId, projectId).update();
				}
			}
		});
	}

	@Transactional
	@Override
	public void confirmOrReject(ReqManageParam param) {
		Long reqUserId = Jwt.getLoginUserId();
		Integer projectId = param.getProjectId();
		Long devProjId = param.getDevProjId();
		Integer devProjType = param.getDevProjType();
		// 1.权限校验
		reqManageLogic.checkCanManage(reqUserId, projectId, devProjId, devProjType);

		// 2.状态校验
		DeveloperProjDTO devProjDTO4Update = reqManageLogic.getDevProjDTO4Update(devProjId, devProjType);
		if (!DELIVERING.getCode().equals(devProjDTO4Update.getStatus())) {
			throw new BusinessException(ErrorCodeEnum.PC10040006);
		}
		Integer count = projectRequirementRepo.lambdaQuery().eq(ProjectRequirement::getProjectId, projectId)
				.eq(ProjectRequirement::getStatus, ProjectReqState.DELIVERY_CONFIRMING.getCode()).count();
		if (count != 1) {
			throw new BusinessException(ErrorCodeEnum.PC10040006);
		}
		// 3.更新逻辑
		boolean confirmDelivery = BaseStatus.Y.getCode().equals(param.getConfirm());
		if (confirmDelivery) {
			reqManageLogic.changeDevProjStatus(devProjId, devProjType, DELIVERED);
			projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.COMMENT_AWAITING.getCode())
					.set(ProjectRequirement::getDeliverTime, new Date())
					.eq(ProjectRequirement::getProjectId, projectId).update();

			// 4.mq异步处理信誉墙 todo 发送mq的处理方法，是事务内还是事务外（事务外需要添加重试机制并确保下游消费者的幂等处理）
			devProjDTO4Update.setStatus(DELIVERED.getCode());
			MessageDTO messageDTO = new MessageDTO(KEY_DELIVERY_CONFIRM, devProjDTO4Update);
			producer.produce(messageDTO);
		} else {
			reqManageLogic.changeDevProjStatus(devProjId, devProjType, DEVELOPING);
			projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.DEVELOPING.getCode())
					.eq(ProjectRequirement::getProjectId, projectId).update();
		}
	}

	@Override
	public void dismiss(ReqManageParam param) {
		Long reqUserId = Jwt.getLoginUserId();
		Integer projectId = param.getProjectId();
		Long devProjId = param.getDevProjId();
		Integer devProjType = param.getDevProjType();
		// 1.权限校验
		reqManageLogic.checkCanManage(reqUserId, projectId, devProjId, devProjType);

		redisUtil.trxWithinLock(StrUtil.format(DEV_PROJECTS_STATUS_CHANGE, projectId), () -> {
			DeveloperProjDTO devProjDTO4Update = reqManageLogic.getDevProjDTO4Update(devProjId, devProjType);
			// 2.状态校验
			if (!(DEVELOPING.getCode().equals(devProjDTO4Update.getStatus()))) {
				throw new BusinessException(ErrorCodeEnum.PC10040007);
			}

			// 3.更新逻辑
			reqManageLogic.changeDevProjStatus(devProjId, devProjType, DISMISSED);
			// 被指认人数==0，置为已发布
			Integer personal = personalProjectRepo.lambdaQuery().eq(PersonalProject::getProjectId, projectId)
					.eq(PersonalProject::getStatus, DEV_APPOINT_CONFIRMING.getCode())
					.isNull(PersonalProject::getTeamProjId).count();
			Integer team = teamProjectRepo.lambdaQuery().eq(TeamProject::getProjectId, projectId)
					.eq(TeamProject::getStatus, DEV_APPOINT_CONFIRMING.getCode()).count();
			if (personal + team == 0) {
				projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.PUBLISHED.getCode())
						.eq(ProjectRequirement::getProjectId, projectId).update();
			}
		});
	}

	@Transactional
	@Override
	public void evaluate(ReqManageParam param) {
		Long reqUserId = Jwt.getLoginUserId();
		Integer projectId = param.getProjectId();
		Long devProjId = param.getDevProjId();
		Integer devProjType = param.getDevProjType();
		// 1.权限校验
		reqManageLogic.checkCanManage(reqUserId, projectId, devProjId, devProjType);

		// 2.状态校验
		DeveloperProjDTO devProjDTO4Update = reqManageLogic.getDevProjDTO4Update(devProjId, devProjType);
		if (!DELIVERED.getCode().equals(devProjDTO4Update.getStatus())) {
			throw new BusinessException(ErrorCodeEnum.PC10040008);
		}

		// 3.更新逻辑
		reqManageLogic.evaluateDevProj(devProjId, devProjType, param.getComment(), param.getStars());
		projectRequirementRepo.lambdaUpdate().set(ProjectRequirement::getStatus, ProjectReqState.COMMENT_FINISHED.getCode())
				.eq(ProjectRequirement::getProjectId, projectId).update();

		// 4.mq异步处理信誉墙
		devProjDTO4Update.setStars(param.getStars());
		devProjDTO4Update.setStatus(ProjectReqState.COMMENT_FINISHED.getCode());
		MessageDTO messageDTO = new MessageDTO(KEY_EVALUATION, devProjDTO4Update);
		producer.produce(messageDTO);
	}

}
