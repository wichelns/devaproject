package com.wichelns.devaproject.app.mq.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhijia
 * @date 2024/2/11 16:03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageDTO {
	private ControlHeaderDTO controlHeader;
	private Object content;

	public MessageDTO(String routingKey, Object content) {
		this.controlHeader = new ControlHeaderDTO(routingKey);
		this.content = content;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ControlHeaderDTO {
		private String routingKey;
	}
}
