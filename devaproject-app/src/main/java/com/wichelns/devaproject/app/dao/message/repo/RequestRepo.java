package com.wichelns.devaproject.app.dao.message.repo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.message.mapper.RequestMapper;
import com.wichelns.devaproject.app.module.message.request.entity.Request;
import org.springframework.stereotype.Repository;

/**
 * @author zhijia
 * @date 2024/2/20 23:59
 */
@Repository
public class RequestRepo extends ServiceImpl<RequestMapper, Request> implements IService<Request> {
}
