package com.wichelns.devaproject.app.dao.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wichelns.devaproject.app.module.project.entity.PersonalProject;
import com.wichelns.devaproject.app.module.system.entity.SysDoc;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author zhijia
 * @date 2024/2/14 10:57
 */
@Mapper
public interface PersonalProjectMapper extends BaseMapper<PersonalProject> {
	PersonalProject lockForUpdateById(@Param("devProjId") Long devProjId);
}
