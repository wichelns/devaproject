package com.wichelns.devaproject.app.core.properties;

import lombok.Data;

/**
 * The class Async task properties.
 */
@Data
public class AsyncTaskProperties {

	private int corePoolSize = 1;

	private int maxPoolSize = 100;

	private int queueCapacity = 10000;

	private int keepAliveSeconds = 3000;

	private String threadNamePrefix = "devaproject-task-executor-";
}
