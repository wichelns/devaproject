package com.wichelns.devaproject.app.module.message.announce.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhijia
 * @date 2024/3/4 16:46
 */
@Getter
@AllArgsConstructor
public enum AnnounceTypeEnum {
	ADMIN(1, "运营"),
	SYSTEM(5, "系统"),
	PUBLIC(10, "公告板"),
	;

	private final Integer code;
	private final String desc;
}
