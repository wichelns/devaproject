package com.wichelns.devaproject.app.module.team.logic;

import cn.hutool.extra.spring.SpringUtil;
import com.wichelns.devaproject.app.dao.team.repo.TeamMemberRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamRepo;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.app.module.team.entity.TeamMember;
import com.wichelns.devaproject.app.module.team.enums.MemberIdentityEnum;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/29 21:15
 */
@Component
public class TeamLogic {
	@Resource
	private TeamRepo teamRepo;
	@Resource
	private TeamMemberRepo teamMemberRepo;

	public static boolean isLeader(Integer teamId, Long devUserId) {
		return SpringUtil.getBean(TeamLogic.class).teamMemberRepo.lambdaQuery().eq(TeamMember::getTeamId, teamId)
				.eq(TeamMember::getDevUserId, devUserId)
				.eq(TeamMember::getIdentity, MemberIdentityEnum.LEADER.getCode()).count() == 1;
	}

	public static boolean isMember(Integer teamId, Long devUserId) {
		return SpringUtil.getBean(TeamLogic.class).teamMemberRepo.lambdaQuery().eq(TeamMember::getTeamId, teamId)
				.eq(TeamMember::getDevUserId, devUserId).count() == 1;
	}

	public static boolean teamExists(Integer teamId) {
		return SpringUtil.getBean(TeamLogic.class).teamRepo.lambdaQuery().eq(Team::getTeamId, teamId).count() == 1;
	}

}
