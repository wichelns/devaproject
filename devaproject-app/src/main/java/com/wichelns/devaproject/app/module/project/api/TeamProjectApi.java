package com.wichelns.devaproject.app.module.project.api;

import com.wichelns.devaproject.app.module.project.param.DeveloperProjParam;
import com.wichelns.devaproject.app.module.project.service.DeveloperProjectService;
import com.wichelns.devaproject.app.module.secure.annotation.DevelopUserValid;
import com.wichelns.devaproject.common.model.BaseParam;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/16 0:07
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/project/team")
@Api(value = "团队项目API", tags = "Web - TeamProjectApi")
public class TeamProjectApi {

	@Resource(name = "team")
	private DeveloperProjectService teamProjectService;

	@PostMapping(value = "/join")
	@ApiOperation(httpMethod = "POST", value = "个人参选项目 DONE")
	@DevelopUserValid
	public Wrapper<?> joinProject(@RequestBody @Validated({BaseParam.add.class, DeveloperProjParam.team.class}) DeveloperProjParam param) {
		teamProjectService.joinProject(param);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/quit")
	@ApiOperation(httpMethod = "POST", value = "退出项目 DONE")
	@DevelopUserValid
	public Wrapper<?> quitProject(@RequestBody @Validated(BaseParam.updateInfo.class) DeveloperProjParam param) {
		teamProjectService.quitProject(param.getDeveloperProjId());
		return WrapMapper.ok();
	}

	@PostMapping(value = "/editJoinIntro")
	@ApiOperation(httpMethod = "POST", value = "修改参选介绍 DONE")
	@DevelopUserValid
	public Wrapper<?> editJoinIntro(@RequestBody @Validated(BaseParam.updateInfo.class) DeveloperProjParam param) {
		teamProjectService.editJoinIntro(param);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/confirmDev")
	@ApiOperation(httpMethod = "POST", value = "答应开发指认 DONE")
	@DevelopUserValid
	public Wrapper<?> confirmDevAppointment(@RequestBody @Validated(BaseParam.updateInfo.class) DeveloperProjParam param) {
		teamProjectService.confirmDevAppointment(param);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/deliver")
	@ApiOperation(httpMethod = "POST", value = "项目交付申请")
	@DevelopUserValid
	public Wrapper<?> deliver(@RequestBody @Validated(BaseParam.updateInfo.class) DeveloperProjParam param) {
		teamProjectService.deliver(param);
		return WrapMapper.ok();
	}

}
