package com.wichelns.devaproject.app.module.message.request.strategy.query;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wichelns.devaproject.app.module.message.request.enums.OperationEnum;
import com.wichelns.devaproject.app.module.message.request.param.ReqParam;
import com.wichelns.devaproject.app.module.message.request.strategy.ReqQueryStrategy;
import com.wichelns.devaproject.app.module.message.request.vo.AbstractReqVO;
import com.wichelns.devaproject.common.enums.BaseStatus;
import org.springframework.stereotype.Component;

/**
 * @author zhijia
 * @date 2024/2/28 23:41
 */
@Component
public class SRequestMakeFriendsStrategy implements ReqQueryStrategy {
	private static final OperationEnum OPERATION = OperationEnum.REQUEST_MAKE_FRIENDS;
	private static final int IS_SENDER = BaseStatus.Y.getCode();

	@Override
	public boolean support(OperationEnum operationEnum, int isSender) {
		return OPERATION.equals(operationEnum) && IS_SENDER == isSender;
	}

	@Override
	public IPage<AbstractReqVO> pageRequest(ReqParam param, Integer pageNo, Integer pageSize) {
		return null;
	}

}
