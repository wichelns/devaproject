package com.wichelns.devaproject.app.module.login.service;


import com.wichelns.devaproject.app.module.login.param.LoginParam;
import com.wichelns.devaproject.app.module.login.vo.LoginVO;

public interface LoginService {

    /**
     * <b>临时接口！不是号码主人也可以用号码进行注册！</b>后续须引进短信服务进行身份认证或者接入微信登录
     */
    LoginVO loginOrSignUp(LoginParam param);


    /**
     * 退出登陆
     */
    void logout(String token);
}
