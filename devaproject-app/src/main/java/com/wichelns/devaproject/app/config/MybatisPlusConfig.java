package com.wichelns.devaproject.app.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusConfig {

   /**
    * 分页插件
    */
   @Bean
   public MybatisPlusInterceptor paginationInterceptor() {
       MybatisPlusInterceptor mpInterceptor = new MybatisPlusInterceptor();

       PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor(DbType.MYSQL);
       // 开启 count 的 join 优化,只针对部分 left join
       // paginationInnerInterceptor.setOptimizeJoin(true);
       mpInterceptor.addInnerInterceptor(paginationInnerInterceptor);
       return mpInterceptor;
   }

}
