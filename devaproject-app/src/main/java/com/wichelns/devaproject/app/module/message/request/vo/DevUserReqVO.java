package com.wichelns.devaproject.app.module.message.request.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author zhijia
 * @date 2024/2/29 16:29
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DevUserReqVO extends AbstractReqVO {
	/*
	* team
	* */
	@ApiModelProperty(value = "团队ID")
	private Integer teamId;
	@ApiModelProperty(value = "团名名称")
	private String teamName;

	/*
	* devUser
	* */
	@ApiModelProperty(value = "开发用户Id")
	private Long devUserId;
	@ApiModelProperty(value = "开发用户名")
	private String username;
	@ApiModelProperty(value = "头像")
	private String avatar;
	@ApiModelProperty(value = "个性签名/简介")
	private String profile;
	@ApiModelProperty(value = "接取项目数")
	private Integer recvProjectCount;
	@ApiModelProperty(value = "交付项目数")
	private Integer deliverProjectCount;
	@ApiModelProperty(value = "星星数", notes = "交付项目五星评价")
	private Integer stars;

	/*
	* request
	* */
	@ApiModelProperty(value = "邀请时间")
	private Date inviteTime;
	@ApiModelProperty(value = "申请时间")
	private Date reqTime;

}
