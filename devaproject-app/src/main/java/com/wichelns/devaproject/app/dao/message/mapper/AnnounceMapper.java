package com.wichelns.devaproject.app.dao.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wichelns.devaproject.app.module.message.announce.entity.Announce;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author zhijia
 * @date 2024/2/20 23:58
 */
@Mapper
public interface AnnounceMapper extends BaseMapper<Announce> {
	/**
	 * 系统(个人)/运营的(个人或符合群发对象类型)
	 */
	IPage<Announce> pageAnnounce4Pull(@Param("userId") Long userId, @Param("userType") Integer userType, IPage<Announce> page);
}
