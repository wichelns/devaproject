package com.wichelns.devaproject.app.module.article.exception;

import java.util.Map;

/**
 * @author 淡漠
 * @version 1.0
 */

//自定义参数不符合规范异常
public class ParameterException extends RuntimeException {
    private final Map<String,Object> hashMap;
    public ParameterException(Map<String,Object> hashMap){
        this.hashMap=hashMap;
    }
    public Map<String,Object> getHashMap(){
        return hashMap;
    }
}
