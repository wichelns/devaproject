package com.wichelns.devaproject.app.module.login.service.impl;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.auth0.jwt.interfaces.Claim;
import com.wichelns.devaproject.app.constant.CacheConstant;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.core.context.SysUserContext;
import com.wichelns.devaproject.app.core.context.SystemContext;
import com.wichelns.devaproject.app.dao.user.repo.DeveloperUserRepo;
import com.wichelns.devaproject.app.dao.user.repo.RequirementUserRepo;
import com.wichelns.devaproject.app.module.login.param.LoginParam;
import com.wichelns.devaproject.app.module.login.service.LoginService;
import com.wichelns.devaproject.app.module.login.vo.LoginVO;
import com.wichelns.devaproject.app.module.user.convert.UserConvert;
import com.wichelns.devaproject.app.module.user.dto.UserDTO;
import com.wichelns.devaproject.app.module.user.entity.DeveloperUser;
import com.wichelns.devaproject.app.module.user.entity.RequirementUser;
import com.wichelns.devaproject.app.module.user.enums.UserStatusEnum;
import com.wichelns.devaproject.app.module.user.enums.UserTypeEnum;
import com.wichelns.devaproject.app.module.user.logic.UserLogic;
import com.wichelns.devaproject.app.module.user.vo.UserVO;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import com.wichelns.devaproject.common.toolkit.HttpContextUtil;
import com.wichelns.devaproject.common.toolkit.IpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.wichelns.devaproject.app.core.authentication.JwtPayloadConstant.CLAIM_USERID;
import static com.wichelns.devaproject.app.core.authentication.JwtPayloadConstant.CLAIM_USERTYPE;

@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

    @Resource
    private RequirementUserRepo reqUserRepo;
    @Resource
    private DeveloperUserRepo devUserRepo;
    @Resource
    private UserLogic userLogic;
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public LoginVO loginOrSignUp(LoginParam param) {
        // 安全校验
        String phone = param.getPhone();
        String attemptPwd = param.getPassword();
        Integer userType = param.getUserType();
        String contact = param.getContact();
        if (!Validator.isMobile(phone)) {
            throw new BusinessException(ErrorCodeEnum.GL99990100);
        }

        UserDTO userDTO;
        if (UserTypeEnum.REQUIREMENT_USER.getCode().equals(userType)) {
            RequirementUser reqUser = reqUserRepo.lambdaQuery().eq(RequirementUser::getPhone, phone).one();
            userDTO = UserConvert.INSTANCE.domain2DTO(reqUser);
        } else {
            DeveloperUser devUser = devUserRepo.lambdaQuery().eq(DeveloperUser::getPhone, phone).one();
            userDTO = UserConvert.INSTANCE.domain2DTO(devUser);
        }
        if (userDTO == null) {// 注册
            userDTO = signUp(phone, userType, contact, attemptPwd);
        }
        // 登录获取token
        String token = login(userDTO, attemptPwd);
        UserVO userVO = UserConvert.INSTANCE.dto2VO(userDTO);

        return new LoginVO(token, userVO);
    }


    @Override
    public void logout(String token) {
        Map<String, Claim> claimMap = Jwt.parseToken(token);
        Long userId = claimMap.get(CLAIM_USERID).asLong();
        Integer userType = claimMap.get(CLAIM_USERTYPE).asInt();
        Boolean deleted = redisTemplate.delete(StrUtil.format(CacheConstant.LOGIN_USER_TEMPLATE, userType, userId));
        log.info("用户[{}]:[{}]登出:{}", userType, userId, deleted);
    }

    private String login (UserDTO realDTO, String attemptPwd) {
        if (ObjectUtil.notEqual(realDTO.getPassword(), attemptPwd)) {
            throw new BusinessException(ErrorCodeEnum.UAC10010019);
        }
        //更新用户信息
        if (UserTypeEnum.REQUIREMENT_USER.getCode().equals(realDTO.getUserType())) {
            reqUserRepo.lambdaUpdate().set(RequirementUser::getLastLoginTime, new Date())
                    .set(RequirementUser::getLastLoginIp, IpUtil.getRemoteAddr(HttpContextUtil.getHttpServletRequest()))
                    .eq(RequirementUser::getReqUserId, realDTO.getUserId()).update();
        } else {
            devUserRepo.lambdaUpdate().set(DeveloperUser::getLastLoginTime, new Date())
                    .set(DeveloperUser::getLastLoginIp, IpUtil.getRemoteAddr(HttpContextUtil.getHttpServletRequest()))
                    .eq(DeveloperUser::getDevUserId, realDTO.getUserId()).update();
        }

        //生成Token
        return genAndSaveToken(realDTO.getUserId(), realDTO.getUserType());
        //todo 单点登录
    }

    private UserDTO signUp (String phone, Integer userType, String contact, String attemptPwd) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserType(userType);
        userDTO.setUserId(SysUserContext.holder().nextUserId());
        userDTO.setUsername(userLogic.getDefaultName(phone));
        userDTO.setAvatar(userLogic.getDefaultAvatar());
        userDTO.setPhone(phone);
        userDTO.setPassword(attemptPwd);// 密码由前端使用md5加密传输
        userDTO.setStatus(UserStatusEnum.NORMAL.getCode());
        userDTO.setContact(contact);
        userDTO.setProfile("");
        userDTO.setRecvProjectCount(0);
        userDTO.setDeliverProjectCount(0);
        userDTO.setStars(0);
        userDTO.setLastLoginTime(new Date());
        userDTO.setLastLoginIp(IpUtil.getRemoteAddr(HttpContextUtil.getHttpServletRequest()));

        if (UserTypeEnum.REQUIREMENT_USER.getCode().equals(userType)) {
            reqUserRepo.save(UserConvert.INSTANCE.dto2ReqUser(userDTO));
        } else {
            devUserRepo.save(UserConvert.INSTANCE.dto2DevUser(userDTO));
        }
        log.info("用户成功注册:userId[{}],用户类型[{}],手机号[{}]", userDTO.getUserId(), userDTO.getUserType(), userDTO.getPhone());

        // mq消息

        return userDTO;
    }

    private String genAndSaveToken(Long userId, Integer userType) {
        String token = Jwt.sign(userId, userType);
        long tokenTimeout = Long.parseLong(SystemContext.holder().opsForSystem().getValue(CacheConstant.TOKEN_TIMEOUT, "10080"));
        redisTemplate.boundValueOps(StrUtil.format(CacheConstant.LOGIN_USER_TEMPLATE, userType, userId))
                .set(userId.toString(), tokenTimeout, TimeUnit.MINUTES);
        return token;
    }
}
