package com.wichelns.devaproject.app.module.article.logic;

import com.wichelns.devaproject.app.module.article.util.TagUtil;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.wichelns.devaproject.common.constant.GlobalConstant.*;

/**
 * @author 淡漠
 * @version 1.0
 */
@Component
public class TagLogic {
	@Resource
	private RedisTemplate<String, Object> redisTemplate;

	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy:MM:dd");

	private HashOperations<String, String, Integer> hashOps;

	@PostConstruct
	public void init() {
		hashOps = redisTemplate.opsForHash();
	}

	// 接收tag列表
	public void increaseHeat(List<String> tagList) {
		tagList.forEach(this::updateTagHeatForIncrease);
	}

	// 接收tag列表字符串
	public void increaseHeat(String tagList) {
		// tagList已经进行检验有数据
		Arrays.asList(tagList.substring(1, tagList.length() - 1).split(",")).forEach(this::updateTagHeatForIncrease);
	}

	// 获取当前日期字符串
	public String getCurrentDateAsString() {
		return TAG + LocalDate.now().format(formatter);
	}

	// 新增项目tag热度加50
	public void updateTagHeatForIncrease(String tag) {
		String key = getCurrentDateAsString();

		int heat = INCREASE_TAG_HEAT;
		if (hashOps.hasKey(key, tag)) {
			heat += Objects.requireNonNull(hashOps.get(key, tag));
		}

		hashOps.put(key, tag, heat);

		Long expire = redisTemplate.getExpire(key, TimeUnit.SECONDS);
		if (expire != null && expire > 0) {
			return;
		}

		LocalDate week = LocalDate.now().plusDays(8);
		// 获取八天后的日期
		Date expirationDate = Date.from(week.atStartOfDay(ZoneId.systemDefault()).toInstant());
		// 设置哈希表键的过期时间为八天后的日期
		redisTemplate.expireAt(key, expirationDate);
	}

	// 浏览tag热度加10
	public void updateTagHeatForBrowse(String tag) {
		// 处理tag
		tag = TagUtil.checkTag(tag);

		String key = getCurrentDateAsString();
		int heat = BROWSE_TAG_HEAT;
		if (hashOps.hasKey(key, tag)) {
			heat += Objects.requireNonNull(hashOps.get(key, tag));
		}

		hashOps.put(key, tag, heat);
	}

	// 直接返回
	public Map<String, Integer> getTagHeat() {
		// 使用 LinkedHashMap 保持顺序
		Map<String, Integer> resultMap = new LinkedHashMap<>();

		ZSetOperations<String, Object> zSetOps = redisTemplate.opsForZSet();
		// 降序获取
		Set<ZSetOperations.TypedTuple<Object>> typedTuples = zSetOps.reverseRangeWithScores(TAG_HEAT, 0, -1);

		assert typedTuples != null;
		for (ZSetOperations.TypedTuple<Object> typedTuple : typedTuples) {
			resultMap.put(Objects.requireNonNull(typedTuple.getValue()).toString(),
					Objects.requireNonNull(typedTuple.getScore()).intValue());
		}
		return resultMap;
	}

	// 查询返回一周内热度前十
	public Map<String, Integer> getTagHeatForWeek() {
		Map<String, Integer> tagCountMap = new LinkedHashMap<>();
		for (int i = 1; i < 8; i++) {
			String key = TAG + LocalDate.now().minusDays(i).format(formatter); // 构建键名
			Map<String, Integer> dailyTagCount = hashOps.entries(key);
			if (dailyTagCount.isEmpty()) {
				continue;
			}
			dailyTagCount.forEach((tag, count) -> tagCountMap.merge(tag, count, Integer::sum));
			// 在查询时进行删除任务
			if (i == 7) {
				redisTemplate.delete(key);
			}
		}

		// 对tagHeatMap按照热度值进行降序排序，并选取热度最高的十个元素组成新的LinkedHashMap
		return tagCountMap.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.limit(10)
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						Map.Entry::getValue,
						(e1, e2) -> e1,
						LinkedHashMap::new
				));

	}

	// 点击热门tag
	public void clickTag(String tag) {
		// tag增加热度
		updateTagHeatForBrowse(tag);
	}

}
