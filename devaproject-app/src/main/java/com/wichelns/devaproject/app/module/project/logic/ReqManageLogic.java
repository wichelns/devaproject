package com.wichelns.devaproject.app.module.project.logic;

import cn.hutool.core.util.ObjectUtil;
import com.wichelns.devaproject.app.dao.article.repo.ProjectRequirementRepo;
import com.wichelns.devaproject.app.module.article.entity.ProjectRequirement;
import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.module.project.enums.DeveloperProjStatusEnum;
import com.wichelns.devaproject.app.module.project.enums.DeveloperProjTypeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static com.wichelns.devaproject.common.enums.ErrorCodeEnum.PC10040001;

/**
 * @author zhijia
 * @date 2024/2/26 12:23
 */
@Component
public class ReqManageLogic {
	@Resource
	private ProjectRequirementRepo projReqRepo;
	@Resource
	private AbstractDevProjLogic personalProjLogic;
	@Resource
	private AbstractDevProjLogic teamProjLogic;

	/**
	 * 校验需求用户管理权限：当且仅当项目是自己的，且开发者项目对应的是自己的项目时正常；否则抛出业务异常。
	 */
	public void checkCanManage(Long reqUserId, Integer projectId, Long devProjId, Integer devProjType) {
		Integer count = projReqRepo.lambdaQuery().eq(ProjectRequirement::getProjectId, projectId)
				.eq(ProjectRequirement::getReqUserId, reqUserId).count();
		if (count != 1) {
			throw new BusinessException(PC10040001);
		}
		DeveloperProjDTO devProjDTO4Update = getDevProjDTO4Update(devProjId, devProjType);
		if (ObjectUtil.notEqual(projectId, devProjDTO4Update.getProjectId())) {
			throw new BusinessException(PC10040001);
		}
	}

	public DeveloperProjDTO getDevProjDTO4Update(Long devProjId, Integer devProjType) {
		return DeveloperProjTypeEnum.PERSONAL_PROJECT.getCode().equals(devProjType)
				? personalProjLogic.getDTOById4Update(devProjId)
				: teamProjLogic.getDTOById4Update(devProjId);
	}

	public void changeDevProjStatus(Long devProjId, Integer devProjType, DeveloperProjStatusEnum targetStatus) {
		if (DeveloperProjTypeEnum.PERSONAL_PROJECT.getCode().equals(devProjType)) {
			personalProjLogic.changeDevProjStatus(devProjId, targetStatus);
		} else {
			teamProjLogic.changeDevProjStatus(devProjId, targetStatus);
		}
	}

	public void evaluateDevProj(Long devProjId, Integer devProjType, String comment, Integer stars) {
		if (DeveloperProjTypeEnum.PERSONAL_PROJECT.getCode().equals(devProjType)) {
			personalProjLogic.evaluateDevProj(devProjId, comment, stars);
		} else {
			teamProjLogic.evaluateDevProj(devProjId, comment, stars);
		}
	}

}
