package com.wichelns.devaproject.app.module.team.service;

import com.wichelns.devaproject.app.module.team.param.TeamParam;

/**
 * @author zhijia
 * @date 2024/3/1 14:33
 */
public interface TeamService {
	void addOrEdit(TeamParam param);

	/**
	 * 转让队长
	 */
	void changeLeader(int teamId, long targetMemId);

	/**
	 * 解散团队
	 */
	void breakUp(int teamId);
}
