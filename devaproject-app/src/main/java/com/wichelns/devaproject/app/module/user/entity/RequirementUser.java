package com.wichelns.devaproject.app.module.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wichelns.devaproject.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author zhijia
 * @date 2024/2/12 11:19
 */
@ApiModel(value = "需求用户")
@TableName(value = "requirement_user", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
public class RequirementUser extends BaseEntity {
// 	ID、用户名、头像、手机号、密码、状态、微信或QQ联系号、简介、上次登录时间、上次登录IP
	@TableId(type = IdType.NONE)
	@ApiModelProperty(value = "需求用户Id")
	private Long reqUserId;
	@ApiModelProperty(value = "需求用户名")
	private String username;
	@ApiModelProperty(value = "头像")
	private String avatar;
	@ApiModelProperty(value = "电话")
	private String phone;
	@ApiModelProperty(value = "密码")
	private String password;
	@ApiModelProperty(value = "状态 1-正常 99-冻结")
	private Integer status;
	@ApiModelProperty(value = "微信/QQ联系号")
	private String contact;
	@ApiModelProperty(value = "个性签名/简介")
	private String profile;
	@ApiModelProperty(value = "上次登录时间")
	private Date lastLoginTime;
	@ApiModelProperty(value = "上次登录IP")
	private String lastLoginIp;
}
