package com.wichelns.devaproject.app.dao.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wichelns.devaproject.app.module.user.entity.RequirementUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhijia
 * @date 2024/2/14 11:01
 */
@Mapper
public interface RequirementUserMapper extends BaseMapper<RequirementUser> {
}
