package com.wichelns.devaproject.app.module.article.enums;

import com.wichelns.devaproject.app.core.fsm.BaseState;
import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 项目需求状态
 *
 * @author zhijia
 * @date 2024/2/17 14:10
 */
@Getter
@AllArgsConstructor
public enum ProjectReqState implements BaseState {
	DRAFT(10, "草稿"),
	PUBLISHED(20, "已发布"),
	DEV_APPOINT_CONFIRMING(30, "开发指认待确认"),
	DEVELOPING(40, "开发中"),
	DELIVERY_CONFIRMING(50, "交付待确认中"),
	COMMENT_AWAITING(60, "待评价"),
	COMMENT_FINISHED(70, "已评价"),
	;

	private final Integer code;
	private final String desc;

	public static ProjectReqState getByCode(Integer code) {
		for (ProjectReqState reqState : ProjectReqState.values()) {
			if (reqState.getCode().equals(code)) {
				return reqState;
			}
		}

		throw new BusinessException("状态非法");
	}

}
