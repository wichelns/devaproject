package com.wichelns.devaproject.app.mq.consumer.redis.simple;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.wichelns.devaproject.app.module.project.dto.DeveloperProjDTO;
import com.wichelns.devaproject.app.mq.consumer.handler.TeamProjectHandler;
import com.wichelns.devaproject.common.annotation.Topic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static com.wichelns.devaproject.app.constant.MQConstant.KEY_PROJECT_TEAM_JOIN;
import static com.wichelns.devaproject.app.constant.MQConstant.MQ_TYPE_PUBSUB;

/**
 * 团队参选项目监听
 *
 * @author zhijia
 * @date 2024/2/23 11:53
 */
@Topic(KEY_PROJECT_TEAM_JOIN)
@Slf4j
@Component
@ConditionalOnProperty(value = "mq-type", havingValue = MQ_TYPE_PUBSUB)
public class TeamJoinListener implements MessageListener {
	@Resource
	private TeamProjectHandler teamProjectHandler;
	@Resource
	private TaskExecutor taskExecutor;

	@Override
	public void onMessage(Message message, byte[] pattern) {
		String jsonStr = JSONObject.parseObject(new String(message.getBody()), String.class);
		DeveloperProjDTO developerProjDTO = JSONUtil.toBean(jsonStr, DeveloperProjDTO.class);

		// 异步处理，降低消息堆积达到阈值导致的pubsub强制下线概率
		taskExecutor.execute(() -> teamProjectHandler.handleProjTeamJoin(developerProjDTO));
	}

}
