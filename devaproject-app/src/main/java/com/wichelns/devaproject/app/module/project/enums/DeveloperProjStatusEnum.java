package com.wichelns.devaproject.app.module.project.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 开发者项目状态
 * 
 * @author zhijia
 * @date 2024/2/14 11:20
 */
@Getter
@AllArgsConstructor
public enum DeveloperProjStatusEnum {
	APPOINT_AWAITING(10, "候选中"),
	DEV_APPOINT_CONFIRMING(20, "开发指认待确认"),
	DEVELOPING(30, "开发中"),
	DELIVERING(40, "交付中"),
	DELIVERED(50, "已交付"),
	QUITED(60, "已放弃"),
	DISMISSED(70, "已被解雇"),
	;

	private final Integer code;
	private final String desc;

}
