package com.wichelns.devaproject.app.module.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * @author zhijia
 * @date 2024/2/16 0:29
 */
@Data
@Builder
@ApiModel(description = "用户VO")
public class UserVO {
	@ApiModelProperty(value = "用户类型")
	private Integer userType;
	/*
	* 共有属性
	* */
	@ApiModelProperty(value = "用户Id")
	private Long userId;
	@ApiModelProperty(value = "用户名")
	private String username;
	@ApiModelProperty(value = "头像")
	private String avatar;
	@ApiModelProperty(value = "电话")
	private String phone;
	@ApiModelProperty(value = "状态 1-正常 99-冻结")
	private Integer status;
	@ApiModelProperty(value = "微信/QQ联系号")
	private String contact;
	@ApiModelProperty(value = "个性签名/简介")
	private String profile;
	/*
	* 开发用户
	* */
	@ApiModelProperty(value = "接取项目数")
	private Integer recvProjectCount;
	@ApiModelProperty(value = "交付项目数")
	private Integer deliverProjectCount;
	@ApiModelProperty(value = "星星数", notes = "交付项目五星评价")
	private Integer stars;

	@Tolerate
	public UserVO(){}

}
