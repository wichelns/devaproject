package com.wichelns.devaproject.app.module.ranking.service.impl;


import cn.hutool.core.util.NumberUtil;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.wichelns.devaproject.app.dao.team.repo.TeamMemberRepo;
import com.wichelns.devaproject.app.dao.team.repo.TeamRepo;
import com.wichelns.devaproject.app.dao.user.repo.DeveloperUserRepo;
import com.wichelns.devaproject.app.module.ranking.service.RankingService;
import com.wichelns.devaproject.app.module.team.entity.Team;
import com.wichelns.devaproject.app.module.team.entity.TeamMember;
import com.wichelns.devaproject.app.module.user.entity.DeveloperUser;


import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

import static com.wichelns.devaproject.common.constant.GlobalConstant.RANG_TEAM;
import static com.wichelns.devaproject.common.constant.GlobalConstant.RANG_USER;

/**
 * @Author: xph
 * @Description: 描述
 * @Date: 创建于14:44 2024-03-05
 **/
@Service
public class RankingServiceImpl implements RankingService {
    @Resource
    private TeamRepo teamRepo;
    @Resource
    private DeveloperUserRepo developerUserRepo;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private TeamMemberRepo teamMemberRepo;

    /***
     * 获取开发者排行榜
     * @return List<DeveloperUser>
     */
    @Override
    public List<DeveloperUser> getUserRanking() {
        List<DeveloperUser> developerUsers = new ArrayList<>();
        //如果redis中没有数据则加载数据库中数据进入redis
        if (!Boolean.TRUE.equals(redisTemplate.hasKey(RANG_USER))) {
            List<DeveloperUser> list = developerUserRepo.lambdaQuery().list();
            Set<ZSetOperations.TypedTuple<Object>> redisUR = new HashSet<>();
            list.forEach(developerUser -> {
                if (developerUser.getStars() != 0) {
                    redisUR.add(new DefaultTypedTuple<>(developerUser.getDevUserId(), Double.parseDouble(developerUser.getDeliverProjectCount().toString()) + NumberUtil.div(Double.parseDouble(developerUser.getStars().toString()), Double.parseDouble(developerUser.getDeliverProjectCount().toString()) * 50, 3)));
                } else {
                    redisUR.add(new DefaultTypedTuple<>(developerUser.getDevUserId(), Double.valueOf(developerUser.getDeliverProjectCount())));
                }

            });
            redisTemplate.opsForZSet().add(RANG_USER, redisUR);
        }
        Set<ZSetOperations.TypedTuple<Object>> typedTuples = redisTemplate.opsForZSet().reverseRangeWithScores(RANG_USER, 0, 100);
        assert typedTuples != null;
        typedTuples.forEach(typedTuple -> developerUsers.add(developerUserRepo.lambdaQuery().eq(DeveloperUser::getDevUserId, typedTuple.getValue()).one()));
        return developerUsers;
    }

    /***
     * 获取团队排行榜
     * @return List<Team>
     */
    public List<Team> getTeamRanking() {
        List<Team> teams = new ArrayList<>();
        //如果redis中没有数据则加载数据库中数据进入redis
        if (!Boolean.TRUE.equals(redisTemplate.hasKey(RANG_TEAM))) {
            List<Team> list = teamRepo.lambdaQuery().list();
            Set<ZSetOperations.TypedTuple<Object>> redisTR = new HashSet<>();
            list.forEach(team -> {
                if (team.getStars() != 0) {
                    redisTR.add(new DefaultTypedTuple<>(team.getTeamId(), Double.parseDouble(team.getDeliverProjectCount().toString()) + NumberUtil.div(Double.parseDouble(team.getStars().toString()), (Double.parseDouble(team.getDeliverProjectCount().toString()) * 50), 3)));
                } else {
                    redisTR.add(new DefaultTypedTuple<>(team.getTeamId(), Double.valueOf(team.getDeliverProjectCount())));
                }
            });
            redisTemplate.opsForZSet().add(RANG_TEAM, redisTR);
        }
        Set<ZSetOperations.TypedTuple<Object>> typedTuples = redisTemplate.opsForZSet().reverseRangeWithScores(RANG_TEAM, 0, 100);
        assert typedTuples != null;
        typedTuples.forEach(typedTuple ->
                teams.add(teamRepo.lambdaQuery().eq(Team::getTeamId, typedTuple.getValue()).one()));
        return teams;
    }

    /**
     * 修改redis里面的分数信息
     */
    public void updateUserRanking(Long id) {
        DeveloperUser developerUser = developerUserRepo.lambdaQuery().eq(DeveloperUser::getDevUserId, id).one();
        if (developerUser.getStars() != 0) {
            redisTemplate.opsForZSet().add(RANG_USER, id, Double.parseDouble(developerUser.getDeliverProjectCount().toString()) + NumberUtil.div(Double.parseDouble(developerUser.getStars().toString()), (Double.parseDouble(developerUser.getDeliverProjectCount().toString()) * 50), 3));
        } else {
            redisTemplate.opsForZSet().add(RANG_USER, id, Double.valueOf(developerUser.getDeliverProjectCount()));
        }
    }

    public void updateTeamRanking(Integer id) {
        Team team = teamRepo.lambdaQuery().eq(Team::getTeamId, id).one();
        if (team.getStars() != 0) {
            redisTemplate.opsForZSet().add(RANG_TEAM, id, Double.parseDouble(team.getDeliverProjectCount().toString()) + NumberUtil.div(Double.parseDouble(team.getStars().toString()), (Double.parseDouble(team.getDeliverProjectCount().toString()) * 50), 3));
        } else {
            redisTemplate.opsForZSet().add(RANG_TEAM, id, Double.valueOf(team.getDeliverProjectCount()));
        }
        List<TeamMember> teamMembers = teamMemberRepo.lambdaQuery().eq(TeamMember::getTeamId, id).list();
        teamMembers.forEach(teamMember -> updateUserRanking(teamMember.getDevUserId()));
    }
}

