package com.wichelns.devaproject.app.module.ranking.api;

import com.wichelns.devaproject.app.module.ranking.service.RankingService;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author zhuyu
 * @update 2024/3/5 14:23
 */

@RestController
@Slf4j
@RequestMapping(value = "/api/ranking")
@Api(value = "排行榜API", tags = "Web - RankingApi")
public class RankingApi {
    @Resource
    private RankingService rankingService;

    @ApiOperation(value = "用户排行榜")
    @GetMapping(value = "/userRanking")
    public Wrapper<?> userRanking(){
        return WrapMapper.ok(rankingService.getUserRanking());
    }

    @ApiOperation(value = "团队排行榜")
    @GetMapping(value = "/teamRanking")
    public Wrapper<?> teamRanking(){
        return WrapMapper.ok(rankingService.getTeamRanking());
    }
}
