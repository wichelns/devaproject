package com.wichelns.devaproject.app.core.support;

import cn.hutool.core.util.IdUtil;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;

/**
 * @author zhijia
 * @date 2023/10/17 0:11
 */
@Component
public class DefaultRedisLock implements RedisLock {
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    private static final long LOCK_EXPIRE_TIME = 15000L; // 锁过期时间
    private static final long LOCK_TRY_INTERVAL = 150L; // 获取锁的间隔时间
    private static final int LOCK_TRY_TIMES = 5; // 获取锁的重试次数
    private static final String ID_PREFIX = IdUtil.fastUUID() + ":"; // 确保上锁解锁为同一线程
    private static final String LOCK_PREFIX = "lock:"; // 避免影响业务数据
    private static final DefaultRedisScript<Long> LOCK_SCRIPT; // 可重入锁lua脚本
    private static final DefaultRedisScript<Long> UNLOCK_SCRIPT;


    static {
        LOCK_SCRIPT = new DefaultRedisScript<>();
        LOCK_SCRIPT.setLocation(new ClassPathResource("lock.lua"));
        LOCK_SCRIPT.setResultType(Long.class);

        UNLOCK_SCRIPT = new DefaultRedisScript<>();
        UNLOCK_SCRIPT.setLocation(new ClassPathResource("unlock.lua"));
        UNLOCK_SCRIPT.setResultType(Long.class);
    }

    /**
     * 推荐使用<br/>
     * 可重试获取锁，无需考虑key,value
     */
    public boolean lock(String key) {
        key = LOCK_PREFIX + key;
        String value = ID_PREFIX + Thread.currentThread().getId();
        return lock(key, value);
    }

    /**
     * 可重试获取锁，需要考虑key,value
     */
    public boolean lock(String key, String value) {
        try {
            for (int i = 0; i < LOCK_TRY_TIMES; i++) {
                if (baseLock(key, value)) {
                    return true;
                }
                Thread.sleep(LOCK_TRY_INTERVAL);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return false;
        }
        return false;
    }

    /**
     * 推荐使用<br/>
     * 仅尝试获取锁一次，无需考虑key,value
     */
    public boolean singleLock(String key) {
        key = LOCK_PREFIX + key;
        String value = ID_PREFIX + Thread.currentThread().getId();
        return baseLock(key, value);
    }


    // public boolean baseLock(String key, String value) {
    //     return Boolean.TRUE.equals(redisTemplate.opsForValue().setIfAbsent(key, value, LOCK_EXPIRE_TIME, TimeUnit.MILLISECONDS));
    // }

    public boolean baseLock(String key, String value) {
        Long success = 1L;
        Long res = redisTemplate.execute(LOCK_SCRIPT, Collections.singletonList(key), value, LOCK_EXPIRE_TIME);
        return success.equals(res);
    }

    /**
     * 推荐使用<br/>
     * 解锁，无需考虑key,value
     */
    public void unlock(String key) {
        key = LOCK_PREFIX + key;
        String value = ID_PREFIX + Thread.currentThread().getId();
        baseUnlock(key, value);
    }


    public void baseUnlock(String key, String value) {
        // 调用lua脚本
        redisTemplate.execute(UNLOCK_SCRIPT, Collections.singletonList(key), value);
    }


}
