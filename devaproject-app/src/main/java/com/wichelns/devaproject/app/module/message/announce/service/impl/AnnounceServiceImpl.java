package com.wichelns.devaproject.app.module.message.announce.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wichelns.devaproject.app.core.authentication.Jwt;
import com.wichelns.devaproject.app.core.context.SysUserContext;
import com.wichelns.devaproject.app.dao.message.repo.AnnounceRepo;
import com.wichelns.devaproject.app.module.message.announce.constant.AnnounceConstant;
import com.wichelns.devaproject.app.module.message.announce.entity.Announce;
import com.wichelns.devaproject.app.module.message.announce.enums.AnnounceTypeEnum;
import com.wichelns.devaproject.app.module.message.announce.service.AnnounceService;
import com.wichelns.devaproject.app.module.message.announce.vo.AnnounceVO;
import com.wichelns.devaproject.common.enums.BaseStatus;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.Date;
import java.util.List;

import static com.wichelns.devaproject.app.constant.CacheConstant.BROADCAST_READ_MARK_TEMPLATE;

/**
 * @author zhijia
 * @date 2024/3/4 10:43
 */
@Service
public class AnnounceServiceImpl implements AnnounceService {
	@Resource
	private AnnounceRepo announceRepo;
	@Resource
	private RedisTemplate<String, String> redisTemplate;

	@Override
	public IPage<AnnounceVO> pullAnnounce(int pageNo, int pageSize) {
		long loginUserId = Jwt.getLoginUserId();
		Integer userType = Jwt.getLoginUserType();
		// 系统(个人)/运营的(个人或群发对象类型符合)
		IPage<Announce> page = announceRepo.getBaseMapper().pageAnnounce4Pull(loginUserId, userType, new Page<>(pageNo, pageSize));

		long idLeadingNum = SysUserContext.parseIdLeadingNum(loginUserId);
		long bitmapOffset = loginUserId - SysUserContext.parseIdLowerBound(loginUserId);

		return page.convert(e -> {
			AnnounceVO announceVO = BeanUtil.copyProperties(e, AnnounceVO.class);
			// 运营群发
			if (AnnounceTypeEnum.ADMIN.getCode().equals(announceVO.getAnnounceType())
					&& announceVO.getReceiverId() == null) {
				// 距离发布时间过去30天 直接返回已读
				if (DateUtil.date().isAfter(DateUtil.offsetDay(announceVO.getCreatedTime(), AnnounceConstant.DEFAULT_MAINTAIN_DAYS))) {
					announceVO.setStatus(BaseStatus.Y.getCode());
				} else {
					// 获取状态
					String readMarkKey = StrUtil.format(BROADCAST_READ_MARK_TEMPLATE, announceVO.getAnnounceId(), idLeadingNum);
					boolean read = Boolean.TRUE.equals(redisTemplate.opsForValue().getBit(readMarkKey, bitmapOffset));
					announceVO.setStatus(read ? BaseStatus.Y.getCode() : BaseStatus.N.getCode());
				}
			}
			return announceVO;
		});
	}

	@Override
	public IPage<AnnounceVO> pullPublic(int pageNo, int pageSize) {
		return announceRepo.lambdaQuery()
				.isNull(Announce::getReceiverId) // 使用索引
				.eq(Announce::getAnnounceType, AnnounceTypeEnum.PUBLIC.getCode())
				.page(new Page<>(pageNo, pageSize))
				.convert(e -> BeanUtil.copyProperties(e, AnnounceVO.class));
	}

	@Override
	public void readOne(int announceId) {
		long loginUserId = Jwt.getLoginUserId();
		Integer userType = Jwt.getLoginUserType();
		Announce announce = announceRepo.getById(announceId);
		if (AnnounceTypeEnum.ADMIN.getCode().equals(announce.getAnnounceType())
				&& announce.getReceiverId() == null // 运营群发
				&& (userType.equals(announce.getReceiverType()) || announce.getReceiverType() == null) // 接收者类型
				&& DateUtil.date().isBefore(DateUtil.offsetDay(announce.getCreatedTime(), AnnounceConstant.DEFAULT_MAINTAIN_DAYS))) {

			long idLeadingNum = SysUserContext.parseIdLeadingNum(loginUserId);
			long bitmapOffset = loginUserId - SysUserContext.parseIdLowerBound(loginUserId);
			String readMarkKey = StrUtil.format(BROADCAST_READ_MARK_TEMPLATE, announceId, idLeadingNum);

			redisTemplate.opsForValue().setBit(readMarkKey, bitmapOffset, true);// todo TTL
		} else if (announce.getReceiverId() != null) {
			// 发给个人的
			announceRepo.lambdaUpdate().set(Announce::getStatus, BaseStatus.Y.getCode())
					.eq(Announce::getReceiverId, loginUserId)
					.eq(Announce::getAnnounceId, announceId).update();
		}
	}

	@Override
	public void readAll() {
		long loginUserId = Jwt.getLoginUserId();
		int offset = -AnnounceConstant.DEFAULT_MAINTAIN_DAYS;
		DateTime offsetDay = DateUtil.offsetDay(new Date(), offset);

		announceRepo.lambdaUpdate().set(Announce::getStatus, BaseStatus.Y.getCode())
				.eq(Announce::getReceiverId, loginUserId)
				.eq(Announce::getStatus, BaseStatus.N.getCode()).update();
		// 运营群发，接收用户类型，未超期限 todo 临界时间限制
		List<Announce> announceList = announceRepo.lambdaQuery()
				.eq(Announce::getAnnounceType, AnnounceTypeEnum.ADMIN.getCode())
				.isNull(Announce::getReceiverId)
				.and(qw -> qw.eq(Announce::getReceiverType, Jwt.getLoginUserType())
						.or().isNull(Announce::getReceiverType))
				.lt(Announce::getCreatedTime, offsetDay).list();

		long idLeadingNum = SysUserContext.parseIdLeadingNum(loginUserId);
		long bitmapOffset = loginUserId - SysUserContext.parseIdLowerBound(loginUserId);
		announceList.forEach(announce -> {
			String readMarkKey = StrUtil.format(BROADCAST_READ_MARK_TEMPLATE, announce.getAnnounceId(), idLeadingNum);
			redisTemplate.opsForValue().setBit(readMarkKey, bitmapOffset, true);// todo TTL
		});
	}

}
