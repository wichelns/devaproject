package com.wichelns.devaproject.app.dao.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wichelns.devaproject.app.module.message.request.entity.Request;
import com.wichelns.devaproject.app.module.user.entity.CreditRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhijia
 * @date 2024/2/20 23:58
 */
@Mapper
public interface RequestMapper extends BaseMapper<Request> {
}
