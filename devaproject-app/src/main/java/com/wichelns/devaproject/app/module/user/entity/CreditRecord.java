package com.wichelns.devaproject.app.module.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wichelns.devaproject.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;

/**
 * @author zhijia
 * @date 2024/2/20 23:16
 */
@ApiModel(value = "信誉记录")
@TableName(value = "credit_record", autoResultMap = true)
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
public class CreditRecord extends BaseEntity {
	// ID、变动项、变动量、来源ID、来源类型（个人项目、团队项目）、目标ID、目标类型（个人、团队）
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "记录ID")
	private Long recordId;
	@ApiModelProperty(value = "信誉项")
	private Integer creditItem;
	@ApiModelProperty(value = "增量")
	private Integer delta;
	@ApiModelProperty(value = "项目需求ID")
	private Integer projectId;
	@ApiModelProperty(value = "来源ID")
	private Long sourceId;
	@ApiModelProperty(value = "来源类型 10-个人项目 20-团队项目")
	private Integer sourceType;
	@ApiModelProperty(value = "目标ID")
	private Long targetId;
	@ApiModelProperty(value = "目标类型 10-个人 20-团队")
	private Integer targetType;

	@Tolerate
	public CreditRecord() {}

}
