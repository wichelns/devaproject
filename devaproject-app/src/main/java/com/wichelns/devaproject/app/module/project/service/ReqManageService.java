package com.wichelns.devaproject.app.module.project.service;

import com.wichelns.devaproject.app.module.project.param.ReqManageParam;

/**
 * 需求用户与开发者项目交互
 *
 * @author zhijia
 * @date 2024/2/26 12:08
 */
public interface ReqManageService {
	/**
	 * 指认或取消指认
	 */
	void toggleAppoint(ReqManageParam param);

	/**
	 * 确认或拒绝项目交付申请
	 */
	void confirmOrReject(ReqManageParam param);

	/**
	 * 解雇开发
	 */
	void dismiss(ReqManageParam param);

	/**
	 * 项目交付评价
	 */
	void evaluate(ReqManageParam param);

}
