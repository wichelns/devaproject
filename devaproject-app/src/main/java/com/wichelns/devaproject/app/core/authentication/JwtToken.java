package com.wichelns.devaproject.app.core.authentication;

import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * JSON Web Token
 */
@Data
public class JwtToken implements AuthenticationToken {

    private static final long serialVersionUID = 1282057025599826155L;

    private String token;

    private String expiresAt;

    public JwtToken(String token) {
        this.token = token;
    }

    public JwtToken(String token, String expiresAt) {
        this.token = token;
        this.expiresAt = expiresAt;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

}
