package com.wichelns.devaproject.app.module.team.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wichelns.devaproject.common.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhijia
 * @date 2024/3/3 13:40
 */
@ApiModel(value = "职责标签")
@TableName(value = "resp_tag", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
public class RespTagLib extends BaseEntity {
	@TableId(type = IdType.AUTO)
	@ApiModelProperty(value = "ID")
	private Integer id;
	@ApiModelProperty(value = "开发者用户ID")
	private Long devUserId;
	@ApiModelProperty(value = "组ID")
	private Integer groupId;
	@ApiModelProperty(value = "组名")
	private String groupName;
	@ApiModelProperty(value = "职责标签列表 格式(JSON数组)：[{'tagId':1,'tagName':'xxx'},...]")
	private String respTagList;
}
