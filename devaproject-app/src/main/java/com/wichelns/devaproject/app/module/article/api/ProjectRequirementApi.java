package com.wichelns.devaproject.app.module.article.api;

import com.wichelns.devaproject.app.module.article.logic.TagLogic;
import com.wichelns.devaproject.app.module.article.param.ProjectReqParam;
import com.wichelns.devaproject.app.module.article.param.ProjectReqRequestParams;
import com.wichelns.devaproject.app.module.article.service.ProjectReqEsService;
import com.wichelns.devaproject.app.module.article.service.ProjectReqService;
import com.wichelns.devaproject.app.module.secure.annotation.RequireUserValid;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author zhijia
 * &#064;date  2024/2/16 0:07
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/article/projectReq")
@Api(value = "项目需求API", tags = "Web - ProjectRequirementApi")
public class ProjectRequirementApi {
	@Resource
	private ProjectReqService projectReqService;
    @Resource
    private ProjectReqEsService projectReqEsService;
	@Resource
	private TagLogic tagLogic;

	@ApiOperation(httpMethod = "POST",value = "发布项目需求")
	@PostMapping("/saveOrUpdate")
	@RequireUserValid
	public Wrapper<Object> saveOrUpdate(@RequestBody ProjectReqParam projectReqParam){
        projectReqService.saveOrUpdate(projectReqParam);
		return WrapMapper.ok();
	}

	@ApiOperation(httpMethod = "POST",value = "发布/撤回项目需求")
	@PostMapping("/togglePublish")
	@RequireUserValid
	public Wrapper<Object> togglePublish(@RequestParam(value = "projectId") Integer projectId){
        projectReqService.togglePublish(projectId);
		return WrapMapper.ok();
	}

    @GetMapping("/search")
	@ApiOperation(httpMethod = "GET",value = "es查询项目需求列表")
    public  Wrapper<?> search(@RequestBody ProjectReqRequestParams projectReqRequestParams){
        return WrapMapper.ok(projectReqEsService.search(projectReqRequestParams));
    }

	@GetMapping("/complete")
	@ApiOperation(httpMethod = "GET",value = "es补充查询项目需求列表")
	public  Wrapper<?> search(@RequestBody String prefix){
		return WrapMapper.ok(projectReqEsService.getSuggestion(prefix));
	}


	//获取热门标签
	@ApiOperation(httpMethod = "GET",value = "获取热门标签")
	@GetMapping("/getTag")
	public Wrapper<Object> getTag(){
		return WrapMapper.ok(tagLogic.getTagHeat());
	}


	//浏览热门tag
	@ApiOperation(httpMethod = "GET",value = "点击热门标签")
	@GetMapping("/clickTag")
	public Wrapper<Object> clickTag(@RequestParam String tag) {
		tagLogic.clickTag(tag);
		return WrapMapper.ok();
	}


}

