package com.wichelns.devaproject.app.module.message.request.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wichelns.devaproject.app.module.message.request.param.ReqParam;
import com.wichelns.devaproject.app.module.message.request.service.RequestService;
import com.wichelns.devaproject.app.module.message.request.vo.AbstractReqVO;
import com.wichelns.devaproject.app.module.secure.annotation.DevelopUserValid;
import com.wichelns.devaproject.common.model.BaseParam;
import com.wichelns.devaproject.common.wrapper.WrapMapper;
import com.wichelns.devaproject.common.wrapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author zhijia
 * @date 2024/2/29 20:08
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/request")
@Api(value = "请求API", tags = "Web - RequestApi")
public class RequestApi {

	@Resource
	private RequestService requestService;

	@PostMapping(value = "/sendReq")
	@ApiOperation(httpMethod = "POST", value = "发送请求")
	@DevelopUserValid
	public Wrapper<?> sendReq(@RequestBody @Validated(BaseParam.add.class) ReqParam param) {
		requestService.sendReq(param);
		return WrapMapper.ok();
	}

	@GetMapping(value = "/query")
	@ApiOperation(httpMethod = "GET", value = "查询请求列表")
	@DevelopUserValid
	public Wrapper<IPage<AbstractReqVO>> queryReq(@ApiParam(value = "操作类型 1-入队邀请 5-入队申请 10-好友申请") @RequestParam(value = "operation") Integer op,
												  @ApiParam(value = "是否为发送类型请求 1-是 0-否") @RequestParam(value = "sender") Integer sender,
												  @ApiParam(value = "页码") @RequestParam(value = "pageNo") Integer pageNo,
												  @ApiParam(value = "页大小") @RequestParam(value = "pageSize") Integer pageSize) {
		return WrapMapper.ok(requestService.pageReq(op, sender, pageNo, pageSize));
	}

	@PostMapping(value = "/confirm")
	@ApiOperation(httpMethod = "POST", value = "确认请求")
	@DevelopUserValid
	public Wrapper<?> confirm(@ApiParam(value = "请求ID") @RequestParam(value = "reqId") Long reqId) {
		requestService.confirmReq(reqId);
		return WrapMapper.ok();
	}

	@PostMapping(value = "/reject")
	@ApiOperation(httpMethod = "POST", value = "拒绝请求")
	@DevelopUserValid
	public Wrapper<?> reject(@ApiParam(value = "请求ID") @RequestParam(value = "reqId") Long reqId) {
		requestService.rejectReq(reqId);
		return WrapMapper.ok();
	}

}
