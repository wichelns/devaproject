package com.wichelns.devaproject.app.dao.message.repo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wichelns.devaproject.app.dao.message.mapper.AnnounceMapper;
import com.wichelns.devaproject.app.module.message.announce.entity.Announce;
import org.springframework.stereotype.Repository;

/**
 * @author zhijia
 * @date 2024/2/20 23:59
 */
@Repository
public class AnnounceRepo extends ServiceImpl<AnnounceMapper, Announce> implements IService<Announce> {
}
