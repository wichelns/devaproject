local val = redis.call('get', KEYS[1])
if (val == false) then -- 获取锁。踩坑记录：键不存在时返回值是false，而不是nil
  redis.call('set', KEYS[1], ARGV[1], 'PX', ARGV[2])
  return 1
else
  if (val == ARGV[1]) then -- 重入锁
    redis.call('pexpire', KEYS[1], ARGV[2]) -- 刷新锁过期时间
    return 1
  else
    return 0
  end
end

-- if (redis.call('set', KEYS[1], ARGV[1], 'PX', ARGV[2], 'NX')) then
--     return 1
-- else
--     if (redis.call('get', KEYS[1]) == ARGV[1]) then
--         return 1
--     else
--         return 0
--     end
-- end