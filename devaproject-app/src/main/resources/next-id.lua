local curr = tonumber(redis.call('GET', KEYS[1]))
local increment = tonumber(ARGV[1])
local initId = tonumber(ARGV[2])
local upperbound = tonumber(ARGV[3])

if (not curr) then--[[初始化]]
    redis.call('SET', KEYS[1], initId)
    curr = initId
end

if (curr + increment < upperbound) then
    return redis.call('INCRBY', KEYS[1], increment)
else
    return -1
end
