package com.wichelns.devaproject.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 管理端启动类
 *
 * @author zhijia
 * @date 2024/2/9 17:57
 */
@SpringBootApplication
public class DevaprojectAdmin {
	public static void main(String[] args) {
		SpringApplication.run(DevaprojectAdmin.class, args);
	}
}
