package com.wichelns.devaproject.admin.anounce.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhijia
 * @date 2024/2/16 0:07
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/admin/announce")
@Api(value = "通知API", tags = "管理端通知管理api")
public class AnnounceApi {

	@PostMapping(value = "/post")
	@ApiOperation(httpMethod = "POST", value = "发布通知")
	public String postAnnounce() {
		return "health";
	}

}
