package com.wichelns.devaproject.common.toolkit.wx;

import lombok.Data;

/**
 * @author yoo
 */
@Data
public class Signature {
    private String appId;
    private long timestamp;
    private String nonce;
    private String signature;
    private String pack;
}
