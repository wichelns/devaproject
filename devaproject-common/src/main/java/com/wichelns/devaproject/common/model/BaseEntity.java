package com.wichelns.devaproject.common.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author yoo
 */
@Getter
@Setter
public class BaseEntity {
    private Integer deleted;
    private Integer version;
    private Date createdTime;
    private Date updatedTime;
}
