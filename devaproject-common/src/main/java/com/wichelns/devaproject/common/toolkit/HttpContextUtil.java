package com.wichelns.devaproject.common.toolkit;

import cn.hutool.core.io.IoUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.wichelns.devaproject.common.enums.ErrorCodeEnum;
import com.wichelns.devaproject.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;


/**
 * Yoo
 */
@Slf4j
public class HttpContextUtil {

    public static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }

    public static UserAgent getUserAgent() {
        HttpServletRequest request = getHttpServletRequest();
        String userAgentStr = request.getHeader("User-Agent");
        return UserAgentUtil.parse(userAgentStr);
    }

    public static String getBody(HttpServletRequest request) {
        try (InputStream is = request.getInputStream()) {
            return IoUtil.read(is, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            log.error("HttpServletRequest Fail", ex);
            throw new BusinessException(ErrorCodeEnum.GL_SYS_ERROR);
        }
    }
}
