package com.wichelns.devaproject.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yoo
 */

@AllArgsConstructor
public enum BaseStatus {
    /**
     * Y
     */
    Y(1, "Yes"),
    /**
     * N
     */
    N(0, "No"),
    ;

    @Getter
    private final Integer code;
    @Getter
    private final String desc;
}
