package com.wichelns.devaproject.common.toolkit;

import cn.hutool.core.util.StrUtil;

import java.util.Arrays;
import java.util.List;

/**
 * @author zhijia
 * @date 2024/3/2 20:07
 */
public class DevaprojectUtil {
	public static String[] arrStrToStrArr(String arrStr) {
		if (StrUtil.isBlank(arrStr)) {
			return new String[0];
		}
		return arrStr.substring(1, arrStr.length() - 1).split(", *");
	}

	public static List<String> arrStrToList(String arrStr) {
		return Arrays.asList(arrStrToStrArr(arrStr));
	}

}
