package com.wichelns.devaproject.common.serializer;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.wichelns.devaproject.common.annotation.JoinPath;

import java.io.IOException;
import java.util.List;
import java.util.StringJoiner;

/**
 * 处理图片前缀的json序列化器 拼接类型 "aa.jpg","bb.jpg"
 *
 * @author author
 * @createDate 2020/12/18 9:04
 */
public class JoinPathSerializer extends JsonSerializer<String> implements ContextualSerializer {
    private boolean addPrefix;

    @Override
    public void serialize(String value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (addPrefix && StrUtil.isNotBlank(value)) {
            List<String> list = StrUtil.split(value, ',');
            StringJoiner stringJoiner = new StringJoiner(",");
            for (String item : list) {
                if (StrUtil.isBlank(item)) {
                    continue;
                }
                if (!StrUtil.contains(item, "http")) {
                    //如果检测到有自定义注解进行前缀处理
                    stringJoiner.add("http://img.dolly.yixo.com.cn" + item);
                } else {
                    stringJoiner.add(item);
                }
            }
            jsonGenerator.writeString(stringJoiner.toString());
            return;
        }
        jsonGenerator.writeString(value);
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider serializerProvider, BeanProperty beanProperty) throws JsonMappingException {
        JoinPath annotation = beanProperty.getAnnotation(JoinPath.class);
        if (annotation != null) {
            this.addPrefix = true;
        }
        return this;
    }
}
