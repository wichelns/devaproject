package com.wichelns.devaproject.common.toolkit;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.wichelns.devaproject.common.annotation.Path;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 路径拼接处理工具
 */
public class PathUtil {

    private static final Map<String, List<String>> FIELD_CACHE = new ConcurrentHashMap<>();

    /**
     * 处理带path注解的字段，拼接上指定域名
     *
     * @param obj
     */
    public static void handlePathField(Object obj) {
        List<String> pathFieldNames = getPathFieldNames(obj.getClass());
        if (CollUtil.isNotEmpty(pathFieldNames)) {
            for (String pathFieldName : pathFieldNames) {
                Object fieldValue = ReflectUtil.getFieldValue(obj, pathFieldName);
                if (fieldValue != null && !StrUtil.contains(fieldValue.toString(), "http")) {
                    ReflectUtil.setFieldValue(obj, pathFieldName, "http://img.dolly.yixo.com.cn" + fieldValue);
                }
            }
        }
    }

    private static List<String> getPathFieldNames(Class<?> clazz) {
        List<String> fieldNameList = new ArrayList<>();
        return Optional.ofNullable(FIELD_CACHE.get(clazz.getName())).orElseGet(() -> {
            Field[] fields = ReflectUtil.getFields(clazz);
            for (Field field : fields) {
                Path annotationsByType = field.getAnnotation(Path.class);
                if (annotationsByType != null) {
                    fieldNameList.add(field.getName());
                }
            }
            FIELD_CACHE.put(clazz.getName(), fieldNameList);
            return fieldNameList;
        });
    }
}
