package com.wichelns.devaproject.common.constant;

/**
 * The class Global constant.
 */
public class GlobalConstant {

	public static final String UNKNOWN = "unknown";
	public static final String IP_KEY = "IP";
	public static final String ENV = "env";
	public static final String X_FORWARDED_FOR = "X-Forwarded-For";
	public static final String X_REAL_IP = "X-Real-IP";
	public static final String PROXY_CLIENT_IP = "Proxy-Client-IP";
	public static final String WL_PROXY_CLIENT_IP = "WL-Proxy-Client-IP";
	public static final String HTTP_CLIENT_IP = "HTTP_CLIENT_IP";
	public static final String HTTP_X_FORWARDED_FOR = "HTTP_X_FORWARDED_FOR";

	/**
	 * 用户代理
	 */
	public static final String USER_AGENT = "User-Agent";

	// http response 相关
	public static final String RESPONSE_JSON = "RESPONSE_JSON";
	public static final String RESPONSE_JSONP = "RESPONSE_JSONP";
	public static final String RESPONSE_RAW = "RESPONSE_RAW";
	public static final String UTF_8 = "UTF-8";
	public static final int OK_STATUS_CODE = 200;
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_TYPE_JSON = "application/json";
	public static final String SET_COOKIES_HEADER = "Set-Cookie";
	public static final String SUCCESS = "success";

	public static final String LOCALHOST_IP = "127.0.0.1";
	public static final String LOCALHOST_IP_16 = "0:0:0:0:0:0:0:1";
	public static final int MAX_IP_LENGTH = 15;

	public static final String DEV_PROFILE = "dev";
	public static final String TEST_PROFILE = "test";
	public static final String PROD_PROFILE = "prod";

	public static final String ROOT_PREFIX = "devaproject";
	public static final String TOKEN = "Authorization";
	public static final String TAG = "tag:";
	public static final Integer INCREASE_TAG_HEAT = 50;
	public static final Integer BROWSE_TAG_HEAT = 10;
	public static final String TAG_HEAT = "tag:hot";

	public final static String RANG_USER = "rang:user";

	public final static String RANG_TEAM = "rang:team";
}
