package com.wichelns.devaproject.common.toolkit;

/**
 * 执行位操作
 */
public class BitUtil {
    /**
     * 把字节指定的位数设为1，不管原来该位是什么。
     *
     * @param data
     * @param pos  从1开始
     */
    public static int setByteBitOne(int data, int pos) {
        data |= (1 << (pos - 1));
        return data;
    }

    /**
     * 把字节指定的位数设为0，不管原来该位是什么。
     *
     * @param data
     * @param pos
     */
    public static int setByteBitZero(int data, int pos) {
        data &= (1 << (pos - 1));
        return data;
    }

    /**
     * 返回指定字节第几个位的数字，只可能是０或者１
     *
     * @param data
     * @param pos  从1开始的位数
     * @return
     */
    public static int getByteBit(int data, int pos) {
        int bitData = 0;

        byte compare = (byte) Math.pow(2.0, pos);
        if ((data & compare) == compare) {
            bitData = 1;
        }
        return bitData;
    }

    /**
     * 该位是不是为1
     * @param data
     * @param pos
     * @return
     */
    public static boolean isOne(int data, int pos) {
        data &= (1L << (pos - 1));
        return data > 0;
    }

    public BitUtil() {
    }

}
