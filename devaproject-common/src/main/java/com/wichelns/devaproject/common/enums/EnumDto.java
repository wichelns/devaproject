package com.wichelns.devaproject.common.enums;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EnumDto {
    private Integer code;
    private String desc;
}
