package com.wichelns.devaproject.common.toolkit;


import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * JSON
 */
public class JsonUtil {

    public static boolean isJson(String str) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.readTree(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static <T> T toBean(String str, Class<T> beanClz) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(str, beanClz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
