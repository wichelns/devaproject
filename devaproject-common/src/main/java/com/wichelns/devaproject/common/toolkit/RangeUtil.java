package com.wichelns.devaproject.common.toolkit;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class RangeUtil {

    public static <T, R> boolean hasIntersection(List<T> source, Function<T, R> getStartFunc, Function<T, R> getEndFunc, Comparator<PositionUnit<R>> comparator) {
        List<PositionUnit<R>> positionUnitList = new ArrayList<>();
        Integer positionUnitId = 0;
        // 加载 positionUnitList 元素
        for (T t : source) {
            PositionUnit<R> startUnit = null;
            PositionUnit<R> endUnit = null;
            if (getStartFunc.apply(t) != null) {
                startUnit = new PositionUnit<>(positionUnitId, getStartFunc.apply(t), false, false);
                positionUnitList.add(startUnit);
            }
            if (getEndFunc.apply(t) != null) {
                endUnit = new PositionUnit<>(positionUnitId, getEndFunc.apply(t), false, false);
                positionUnitList.add(endUnit);
            }
            if (startUnit != null && endUnit != null && comparator.compare(startUnit, endUnit) > 0) {
                throw new RuntimeException("当前排序规则下，区间起点>终点");
            }
            if (startUnit != null) {
                startUnit.hasEnd = endUnit != null;
            }
            if (endUnit != null) {
                endUnit.hasStart = startUnit != null;
            }
            positionUnitId++;
        }
        if (positionUnitList.isEmpty() || positionUnitList.size() == 1) {
            return false;
        }
        positionUnitList.sort(comparator);
        // 交集之边界重叠检查，[x, x] 这种是允许的
        for (int i = 0; i < positionUnitList.size(); i++) {
            if (i != positionUnitList.size() - 1
                    && positionUnitList.get(i).getPosition().equals(positionUnitList.get(i + 1).getPosition())
                    && !positionUnitList.get(i).equals(positionUnitList.get(i + 1))) {
                return true;
            }
        }
        // 交集但非边界重叠检查
        for (int i = 0; i < positionUnitList.size(); i++) {
            if (positionUnitList.get(i).hasStart && i != 0
                    && !(positionUnitList.get(i - 1).equals(positionUnitList.get(i)))) {
                return true;
            }
            if (positionUnitList.get(i).hasEnd && i != positionUnitList.size() - 1
                    && !(positionUnitList.get(i + 1).equals(positionUnitList.get(i)))) {
                return true;
            }
        }

        return false;
    }

    /**
     * 区间交集判断：同一 PositionHelper 至多可以拆分成两个 PositionUnit，这两者共享一个id，
     * [ start | end ]Position 对应存放在 PositionUnit 的 position 里，根据 has[ Start | end ] 判断有无起点或终点。<br/>
     * <b>判断关键</b>：按 position 顺序排序所有的 PositionUnit，对于每一个
     * 对象，若 has[ Start | end ] == true 则相应的前者或后者必须满足 equals() == true。
     * <b>注意</b>：R 需要重写 equals() 方法
     */
    public static final class PositionUnit<R> {
        Integer positionUnitId;
        R position;
        boolean hasStart;
        boolean hasEnd;

        PositionUnit() {
        }

        public PositionUnit(Integer positionUnitId, R position, boolean hasStart, boolean hasEnd) {
            this.positionUnitId = positionUnitId;
            this.position = position;
            this.hasStart = hasStart;
            this.hasEnd = hasEnd;
        }

        public R getPosition() {
            return position;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof PositionUnit)) {
                return false;
            }
            PositionUnit another = (PositionUnit) obj;
            if (another.positionUnitId == null || this.positionUnitId == null) {
                return false;
            }
            return this.positionUnitId.equals(another.positionUnitId);
        }
    }
}
