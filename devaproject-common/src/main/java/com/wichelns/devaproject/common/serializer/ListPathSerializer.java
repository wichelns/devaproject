package com.wichelns.devaproject.common.serializer;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.wichelns.devaproject.common.annotation.ListPath;

import java.io.IOException;
import java.util.List;

/**
 * 处理图片前缀的json序列化器 列表类型 如:["/aaa.jpg","/bbb.jpg"]
 *
 * @author author
 * @createDate 2020/12/18 9:04
 */
public class ListPathSerializer extends JsonSerializer<List<String>> implements ContextualSerializer {
    private boolean addPrefix;

    @Override
    public void serialize(List<String> valueList, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (addPrefix && CollUtil.isNotEmpty(valueList)) {
            String[] strList = new String[valueList.size()];
            int i = 0;
            for (String value : valueList) {
                if (StrUtil.isBlank(value)) {
                    continue;
                }
                if (!StrUtil.contains(value, "http")) {
                    //如果检测到有自定义注解进行前缀处理
                    strList[i++] = "http://img.dolly.yixo.com.cn" + value;
                } else {
                    strList[i++] = value;
                }
            }
            jsonGenerator.writeArray(strList, 0, i);
            return;
        }
        jsonGenerator.writeArray(valueList.toArray(new String[0]), 0, valueList.size());
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider serializerProvider, BeanProperty beanProperty) throws JsonMappingException {
        ListPath annotation = beanProperty.getAnnotation(ListPath.class);
        if (annotation != null) {
            this.addPrefix = true;
        }
        return this;
    }
}
