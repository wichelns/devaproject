package com.wichelns.devaproject.common.toolkit.wx;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * 微信签名工具
 *
 * @author yoo
 */
@Slf4j
public class Signatures {
    private String appId;
    private String key;
    private String mchId;

    public static Signatures assemble(String appId, String key, String mchId) {
        Signatures signatures = new Signatures();
        signatures.appId = appId;
        signatures.key = key;
        signatures.mchId = mchId;
        return signatures;
    }

    /**
     * 创建供APP调用的签名
     *
     * @param prepayId
     * @return
     */
    public Signature createAppSignature(String prepayId) {
        String pack = "Sign=WXPay";
        long timestamp = System.currentTimeMillis() / 1000;
        String nonce = RandomUtil.randomString(16);
        String sortString = String.format("appId=%s&nonceStr=%s&package=%s&partnerid=%s&&prepayid=%s&timeStamp=%s",
                appId, nonce, pack, mchId, prepayId, timestamp);
        return createSignature(pack, timestamp, nonce, appId, key, sortString);
    }

    /**
     * 创建供公众号或者小程序调用的签名
     *
     * @param prepayId
     * @return
     */
    public Signature createJsSignature(String prepayId, String nonce, long timestamp) {
        String pack = "prepay_id=" + prepayId;
        String sortString = String.format("appId=%s&nonceStr=%s&package=%s&signType=MD5&timeStamp=%s",
                appId, nonce, pack, timestamp);
        log.info("sortString:{}", sortString);
        return createSignature(pack, timestamp, nonce, appId, key, sortString);
    }

    /**
     * 创建供公众号或者小程序调用的签名
     *
     * @return
     */
    public Signature createSignature(Object param) {
        ObjectMapper objectMapper = new ObjectMapper();
        TreeMap<String, String> sortMap = objectMapper.convertValue(param, new TypeReference<TreeMap<String, String>>() {
        });
        long timestamp = System.currentTimeMillis() / 1000;
        String sortStr = sortMap.entrySet().stream()
                .filter(set -> ObjectUtil.notEqual("sign", set.getKey()))
                .filter(set -> StrUtil.isNotBlank(set.getValue()))
                .map(set -> set.getKey() + "=" + set.getValue())
                .collect(Collectors.joining("&"));
        return createSignature(null, timestamp, sortMap.get("nonce_str"), appId, key, sortStr);
    }

    private Signature createSignature(String pack, long timestamp, String nonce, String appId, String key, String sortString) {
        String signature = DigestUtil.md5Hex(sortString + "&key=" + key).toUpperCase();
        Signature sign = new Signature();
        sign.setAppId(appId);
        sign.setNonce(nonce);
        sign.setTimestamp(timestamp);
        sign.setSignature(signature);
        sign.setPack(pack);
        return sign;
    }

}
