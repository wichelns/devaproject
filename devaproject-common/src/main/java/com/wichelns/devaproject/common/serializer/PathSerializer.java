package com.wichelns.devaproject.common.serializer;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.wichelns.devaproject.common.annotation.Path;

import java.io.IOException;

/**
 * 处理图片前缀的json序列化器
 *
 * @author author
 * @createDate 2020/12/18 9:04
 */
public class PathSerializer extends JsonSerializer<String> implements ContextualSerializer {
    private boolean addPrefix;

    @Override
    public void serialize(String value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (addPrefix && StrUtil.isNotBlank(value) && !StrUtil.contains(value, "http")) {
            //如果检测到有自定义注解进行前缀处理
            jsonGenerator.writeString("http://img.dolly.yixo.com.cn" + value);
        } else {
            jsonGenerator.writeString(value);
        }
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider serializerProvider, BeanProperty beanProperty) throws JsonMappingException {
        Path annotation = beanProperty.getAnnotation(Path.class);
        if (annotation != null) {
            this.addPrefix = true;
        }
        return this;
    }
}
