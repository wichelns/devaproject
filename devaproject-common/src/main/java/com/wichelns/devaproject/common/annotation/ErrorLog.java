package com.wichelns.devaproject.common.annotation;

import java.lang.annotation.*;


/**
 * 错误日志切面注解；默认优先级比{@code @Transactional}高
 *
 * @author zhijia
 * @date 2024/2/27 20:29
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ErrorLog {
	int ORDER = 90;
}
