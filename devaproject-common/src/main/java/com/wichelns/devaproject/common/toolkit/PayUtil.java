package com.wichelns.devaproject.common.toolkit;

import cn.hutool.crypto.SecureUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class PayUtil {

    /**
     * 获取签名串
     * @param map
     * @return  urlParam.append(key).append("=").append( paraMap.get(key) == null ? "" : paraMap.get(key) );
     */
    public static String getStrSort(Map<String,Object> map){
        ArrayList<String> list = new ArrayList<String>();
        for(Map.Entry<String,Object> entry:map.entrySet()){
            if(null != entry.getValue() && !"".equals(entry.getValue())){
                list.add(entry.getKey() + "=" + entry.getValue() + "&");
            }
        }
        int size = list.size();
        String [] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < size; i ++) {
            sb.append(arrayToSort[i]);
        }
        return sb.toString();
    }

    public static String getSign(Map<String,Object> map, String key){
        String result = getStrSort(map);
        result += "key=" + key;
        return SecureUtil.md5(result).toUpperCase();
    }

    public static String getSign(String signStr, String key){
        signStr += "key=" + key;
        return SecureUtil.md5(signStr).toUpperCase();
    }

    public static String getSign(String signStr){
        return SecureUtil.md5(signStr).toUpperCase();
    }
}
