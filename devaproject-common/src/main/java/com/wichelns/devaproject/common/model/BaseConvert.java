package com.wichelns.devaproject.common.model;


import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HtmlUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.mapstruct.Named;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public interface BaseConvert {

    @Named("joinToList")
    default List<String> joinToList(String joinString) {
        if (StrUtil.isBlank(joinString)) {
            return Collections.emptyList();
        }
        return StrUtil.split(joinString, ",");
    }

    @Named("joinToList4Int")
    default List<Integer> joinToList4Int(String joinString) {
        if (StrUtil.isBlank(joinString)) {
            return Collections.emptyList();
        }
        return StrUtil.split(joinString, ",").stream().map(Integer::parseInt).collect(Collectors.toList());
    }

    @Named("unescape")
    default String unescape(String escapeHtml) {
        return HtmlUtil.unescape(escapeHtml);
    }

    @Named("strToJson")
    default JSONObject strToJson(String jsonStr) {
        return JSONUtil.parseObj(jsonStr);
    }

    @Named("strToArrJson")
    default JSONArray strToArrJson(String jsonStr) {
        return JSONUtil.parseArray(jsonStr);
    }
}
